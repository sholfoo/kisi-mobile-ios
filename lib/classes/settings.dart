import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Settings {
  static setLocale(Locale locale) async {
    var sp = await SharedPreferences.getInstance();
    sp.setString('app_language', locale.languageCode);
    sp.setString('app_country', locale.countryCode);
  }

  static Future<Locale> getLocale() async {
    var sp = await SharedPreferences.getInstance();
    return Locale(
      sp.getString('app_language') ?? 'id',
      sp.getString('app_country') ?? 'ID',
    );
  }
}
