import 'dart:math';

map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) result.add(handler(i, list[i]));
  return result;
}

var rng = new Random();
String randomString() {
  return rng.nextInt(10000).toString();
}
