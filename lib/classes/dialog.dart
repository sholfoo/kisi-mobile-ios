import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

Future<bool> error(BuildContext context,
    {@required String message, Function onOk}) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => AlertDialog(
      title: Text('Error'),
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            if (onOk != null)
              onOk();
            else
              Navigator.pop(context, true);
          },
          child: Text('OK'),
        )
      ],
    ),
  );
}

Future<bool> progressDialog(
  BuildContext context, {
  String message = 'Mohon menunggu, sedang memproses data...',
}) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => WillPopScope(
      child: AlertDialog(
        content: Row(
          children: <Widget>[
            CircularProgressIndicator(),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 16),
                child: Text(message),
              ),
            )
          ],
        ),
      ),
      onWillPop: () async => false,
    ),
  );
}

errorToast(BuildContext context, {@required String message, Function onRetry}) {
  Scaffold.of(context).showSnackBar(SnackBar(
    content: Text(message),
    action: SnackBarAction(label: 'Coba Lagi', onPressed: onRetry),
  ));
}

shortToast({@required String message}) {
  Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_SHORT,
    timeInSecForIos: 1,
  );
}

Future<bool> confirm(BuildContext context,
    {@required String message, Function onCancel, Function onOk}) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => AlertDialog(
      title: Text('Konfirmasi'),
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            if (onCancel != null)
              onCancel();
            else
              Navigator.pop(context, false);
          },
          child: Text('CANCEL'),
        ),
        FlatButton(
          onPressed: () {
            if (onOk != null)
              onOk();
            else
              Navigator.pop(context, true);
          },
          child: Text('OK'),
        )
      ],
    ),
  );
}
