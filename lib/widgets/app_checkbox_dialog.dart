import 'package:flutter/material.dart';

class AppCheckboxDialog extends StatelessWidget {
  final String label;
  final bool value;
  final ValueChanged<bool> onChanged;
  final TextStyle labelStyle;
  final Color checkColor;
  final Color activeColor;

  const AppCheckboxDialog({
    Key key,
    this.label,
    this.value,
    this.onChanged,
    this.labelStyle,
    this.checkColor,
    this.activeColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onChanged(!value);
      },
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Theme(
              data: ThemeData(unselectedWidgetColor: checkColor),
              child: Checkbox(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                value: value,
                onChanged: onChanged,
                checkColor: checkColor,
                activeColor: (activeColor == null)
                    ? Theme.of(context).primaryColor
                    : activeColor,
              ),
            ),
            Text(label, style: labelStyle),
          ],
        ),
      ),
    );
  }
}
