import 'package:flutter/material.dart';

class PhoneCodeSelect extends StatelessWidget {
  final String currentValue;
  final List<PopupMenuItem<String>> items;
  final Function(String) onSelected;

  const PhoneCodeSelect({
    this.currentValue,
    this.items,
    this.onSelected,
  });

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      initialValue: '+62',
      child: Center(
        heightFactor: 1,
        widthFactor: 1,
        child: Text(currentValue),
      ),
      onSelected: onSelected != null ? onSelected : null,
      itemBuilder: (BuildContext context) => items,
    );
  }
}