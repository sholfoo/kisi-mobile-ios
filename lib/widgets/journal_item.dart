import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/dtos/news_item_dto.dart';

import '../journal_detail_screen.dart';

class JournalItem extends StatelessWidget {
  final NewsItemDto news;

  JournalItem({
    this.news,
  });

  @override
  Widget build(BuildContext context) {
    var imgSize = ((MediaQuery.of(context).size.width * 0.9) - 20) / 2;

    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => JournalDetailScreen(news)),
          );
        },
        child: Container(
          padding: EdgeInsets.all(5),
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: FadeInImage.assetNetwork(
                  image: news.image,
                  placeholder: 'assets/image_placeholder.png',
                  width: imgSize,
                  height: imgSize,
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8),
                child: Text(
                  news.title,
                  textAlign: TextAlign.center,
                  maxLines: 2,
                  style: TextStyle(color: Colors.grey.shade700),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(
                  news.subject,
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                  maxLines: 1,
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(
                  DateFormat('dd MMM yyyy').format(news.publishDate),
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 12,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
