import 'package:flutter/material.dart';
import 'package:kisi_flutter/views/register_screen/form_utils.dart';

class TextCencored extends StatelessWidget {
  final String title;
  final String text;
  final bool multiLine;

  TextCencored({
    this.title,
    this.text,
    this.multiLine = false,
  });

  String _cencor(String str) {
    if (str.length <= 4) return str;

    var visible = str.substring(0, 4);
    return visible.padRight(str.length, '*');
  }

  @override
  Widget build(BuildContext context) {
    return InputDecorator(
      child: Text(
        _cencor(text),
        style: TextStyle(),
        maxLines: multiLine ? null : 1,
        overflow: TextOverflow.clip,
      ),
      decoration: defaultInputDecoration(title),
    );
  }
}
