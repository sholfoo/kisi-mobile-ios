import 'package:flutter/material.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';

class SimpleViewState extends BaseScreenState {
  final showImage;

  SimpleViewState({
    ScreenState state,
    Widget Function(BuildContext ctx) child,
    String message = '',
    Function onRetry,
    bool pullToRefresh = true,
    this.showImage = true,
  }) : super(
          state: state,
          child: child,
          message: message,
          onRetry: onRetry,
          pullToRefresh: pullToRefresh,
        );

  @override
  Widget empty() {
    return Container(
      padding: EdgeInsets.all(8),
      width: double.infinity,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Visibility(
              visible: showImage,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 12),
                child: Image.asset(
                  'assets/state/empty.png',
                  color: Colors.grey.shade600,
                  height: 64,
                ),
              ),
            ),
            Text(
              'Tidak ada data, silakan coba kembali',
              textAlign: TextAlign.center,
            ),
            FlatButton(
              onPressed: () => retry(),
              child: Text(
                'REFRESH',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget progress() {
    return Container(
      padding: EdgeInsets.all(8),
      width: double.infinity,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            CircularProgressIndicator(strokeWidth: 2),
            Container(height: 12),
            Text(
              'Mohon menunggu,\nsedang memproses data...',
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget offline() {
    return Container(
      padding: EdgeInsets.all(8),
      width: double.infinity,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              'Anda tidak terhubung ke jaringan',
              textAlign: TextAlign.center,
            ),
            FlatButton(
              onPressed: () => retry(),
              child: Text(
                'REFRESH',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget error() {
    return Container(
      padding: EdgeInsets.all(8),
      width: double.infinity,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Visibility(
              visible: showImage,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 12),
                child: Image.asset(
                  'assets/state/error.png',
                  color: Colors.grey.shade600,
                  height: 64,
                ),
              ),
            ),
            Text(
              message,
              textAlign: TextAlign.center,
            ),
            FlatButton(
              onPressed: () => retry(),
              child: Text(
                'REFRESH',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return super.build(context);
  }
}
