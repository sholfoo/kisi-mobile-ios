import 'package:flutter/material.dart';

class BaseScreenState extends StatelessWidget {
  final ScreenState state;
  final Widget Function(BuildContext ctx) child;
  final String message;
  final Function onRetry;
  final bool pullToRefresh;

  BaseScreenState({
    this.state,
    this.child,
    this.message = '',
    this.onRetry,
    this.pullToRefresh = true,
  });

  Widget offline() {
    return Container();
  }

  Widget progress() {
    return Container();
  }

  Widget empty() {
    return Container();
  }

  Widget error() {
    return Container();
  }

  retry() {
    if (onRetry != null) onRetry();
  }

  @override
  Widget build(BuildContext context) {
    if (state == ScreenState.content) {
      if (pullToRefresh)
        return RefreshIndicator(
          onRefresh: () async {
            retry();
            return Future.value(true);
          },
          child: child(context),
        );
      else
        return child(context);
    }
    if (state == ScreenState.offline) return offline();
    if (state == ScreenState.progress) return progress();
    if (state == ScreenState.empty) return empty();
    if (state == ScreenState.error) return error();

    return Container();
  }
}

enum ScreenState {
  content,
  offline,
  progress,
  empty,
  error,
}
