import 'package:flutter/material.dart';

class AppPageSection extends StatelessWidget {
  final String title;
  final String subtitle;
  final Widget child;
  final Color backgroundColor;
  final GestureTapCallback onMoreTap;
  final EdgeInsets titlePadding;

  AppPageSection({
    this.title,
    this.subtitle,
    this.titlePadding = const EdgeInsets.fromLTRB(16, 16, 16, 0),
    @required this.child,
    this.backgroundColor = Colors.white,
    this.onMoreTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: titlePadding,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        title,
                        style: TextStyle(
                          color: Colors.blue,
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        subtitle,
                        style: TextStyle(
                          color: Colors.grey.shade800,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
                ((onMoreTap != null)
                    ? InkWell(
                        onTap: () {},
                        child: Text(
                          'MORE',
                          style: TextStyle(
                            color: Colors.blue,
                            fontSize: 12,
                          ),
                        ),
                      )
                    : Container()),
              ],
            ),
          ),
          child
        ],
      ),
    );
  }
}

class AppPageSectionSpacer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
    );
  }
}
