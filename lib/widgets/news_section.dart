import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/dtos/news_item_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/section.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';

import '../journal_detail_screen.dart';
import 'journal_item.dart';

class NewsSection extends StatelessWidget {
  final ScreenState state;
  final NewsSectionType type;
  final NewsSectionMode mode;
  final List<NewsItemDto> data;
  final String message;
  final Function onRetry;

  NewsSection({
    this.state = ScreenState.content,
    this.type,
    this.mode,
    this.data,
    this.message,
    this.onRetry,
  });

  _verticalMode(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: data.map<Widget>((item) {
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => JournalDetailScreen(item)),
              );
            },
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 8,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.network(
                        item.image,
                        width: 130,
                        height: 130,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          item.title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Container(height: 8),
                        Text(
                          item.content,
                          maxLines: 4,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Container(height: 8),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              DateFormat('dd MMM yyyy')
                                  .format(item.publishDate),
                              style: TextStyle(
                                color: Colors.grey.shade500,
                                fontSize: 13,
                              ),
                            ),
                            Text(
                              item.subject,
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                color: Colors.grey.shade500,
                                fontSize: 13,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        }).toList(),
      ),
    );
  }

  Widget _horizontalMode(BuildContext context) {
    var perPage = 2;

    var list = List<List<NewsItemDto>>();

    var cPage = 0;
    for (var i = 1; i <= data.length; i++)
      if ((i > 0 && i % perPage == 0) || i == data.length) {
        list.add(data.skip(cPage * perPage).take(perPage).toList());
        cPage++;
      }

    var imgSize = ((MediaQuery.of(context).size.width * 0.9) - 20) / 2;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 11),
      child: CarouselSlider(
        height: imgSize + 90 + 10,
        enlargeCenterPage: false,
        initialPage: 0,
        viewportFraction: 0.9,
        items: list.map((item) {
          return Builder(
            builder: (BuildContext context) {
              return Container(
                width: MediaQuery.of(context).size.width * 0.9,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: item
                      .map<Widget>(
                        (x) => Expanded(child: JournalItem(news: x)),
                      )
                      .toList(),
                ),
              );
            },
          );
        }).toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    var title2 = '';
    if (type == NewsSectionType.market) title2 = loc.main.news_subtitle_market;
    if (type == NewsSectionType.stocks) title2 = loc.main.news_subtitle_stock;

    return AppPageSection(
      title: loc.main.news_title,
      subtitle: title2,
      child: Container(
        child: SimpleViewState(
          state: state,
          message: message,
          onRetry: onRetry,
          showImage: false,
          pullToRefresh: false,
          child: (ctx) {
            return (mode == NewsSectionMode.vertical)
                ? _verticalMode(context)
                : _horizontalMode(context);
          },
        ),
      ),
    );
  }
}

enum NewsSectionType {
  stocks,
  market,
}

enum NewsSectionMode {
  horizontal,
  vertical,
}
