import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/dtos/req_register_dto.dart';
import 'package:kisi_flutter/provider/auth_provider.dart';

Dio _dio;
String token = '';

Dio get client {
  var authProvider = GetIt.I<AuthProvider>();
  token = authProvider.token;

  // Setup dio
  if (_dio == null) {
    _dio = Dio();
//    _dio.options.baseUrl = 'http://dev.kisi.co.id/api/';
    _dio.options.baseUrl = 'https://kisi.co.id/api/';
    _dio.options.followRedirects = false;
  }
  return _dio;
}

Future<Response> login(data) {
  return client.post(
    'login',
    data: data,
  );
}

Future<Response> updateToken() async {
  var fcm = FirebaseMessaging();

  return client.post(
    'update-device-token',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
    data: {
      'device_token': await fcm.getToken(),
    },
  );
}

Future<Response> updatePassword({String oldPassword, String newPassword}) {
  return client.post(
    'update-password',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
    data: {
      'old_password': oldPassword,
      'password': newPassword,
    },
  );
}

Future<Response> forgotPassword({String email}) {
  return client.post(
    'forget-password',
    data: {'email': email},
  );
}

Future<Response> news() {
  return client.get(
    'news',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
  );
}

Future<Response> stockNews() {
  return client.get(
    'stock-news',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
  );
}

Future<Response> marketNews() {
  return client.get(
    'market-news',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
  );
}

Future<Response> notifications() {
  return client.get(
    'list-notification',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
  );
}

Future<Response> researches(String type) {
  return client.get(
    'list-research',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
    queryParameters: {
      'limit': 10, //100000
      'offset': 0,
      'type': type,
    },
  );
}

Future<Response> events() {
  return client.get(
    'list-event',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
  );
}

Future<Response> members() {
  return client.get(
    'user',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
  );
}

Future<Response> countries() {
  return client.get(
    'list-countries',
  );
}

Future<Response> phoneCodes() {
  return client.get(
    'list-phone-codes',
  );
}

Future<Response> business() {
  return client.get(
    'list-line-of-bussiness',
  );
}

Future<Response> banks() {
  return client.get(
    'list-bank',
  );
}

Future<Response> schedules() {
  return client.get(
    'list-schedule',
  );
}

Future<Response> questions() {
  return client.get(
    'list-questionnaire?form_group=risk_profiling_1',
  );
}

Future<Response> validateDukcapil({
  DateTime dateOfBirth,
  String placeOfBirth,
  String idNumber,
}) {
  return client.post(
    'get-data-dukcapil',
    data: {
      'date_of_birth': DateFormat('dd-MM-yyyy').format(dateOfBirth),
      'place_of_birth': placeOfBirth,
      'id_number': idNumber,
      'nationality': 'WNI',
    },
  );
}

Future<Response> validateBca({
  String holderName,
  String cardNumber,
}) {
  return client.post(
    'bca-validation',
    data: {
      'account_holder_name': holderName,
      'account_number': cardNumber,
    },
  );
}

Future<Response> submitRegister(ReqRegisterDto model) async {
  return client.post(
    'opening-account',
    data: FormData.fromMap(await model.toMap()),
  );
}

Future<Response> submitSchedule({
  int userId,
  DateTime schedule,
}) {
  var formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
  return client.post(
    'store-schedule',
    data: {
      'user_id': userId.toString(),
      'datetime': formatter.format(schedule),
    },
  );
}

Future<Response> getSchedule() {
  return client.get(
    'get-schedule',
    options: Options(headers: {'Authorization': 'Bearer $token'}),
  );
}

Future<Response> submitQuestionnaire({
  int userId,
  List<int> questionIds,
  List<int> answerIds,
}) {
  return client.post(
    'submit-questionnaire',
    data: {
      'user_id': userId.toString(),
      'question_ids': questionIds.join(','),
      'answer_ids': answerIds.join(','),
    },
  );
}

String parseError(Object e) {
  var message = "";

  if (e is DioError) {
    switch (e.type) {
      case DioErrorType.DEFAULT:
        message = e.error.toString();
        break;
      case DioErrorType.CONNECT_TIMEOUT:
      case DioErrorType.RECEIVE_TIMEOUT:
      case DioErrorType.SEND_TIMEOUT:
        message = "Waktu koneksi habis";
        break;
      case DioErrorType.CANCEL:
        message = "Permintaan dibatalkan";
        break;
      case DioErrorType.RESPONSE:
        if (e.response.statusCode == 400)
          message =
              "Format permintaan yang Anda kirim tidak sesuai\n${e.response.data}";
        else if (e.response.statusCode == 401)
          message = "Unauthorized";
        else if (e.response.statusCode == 403)
          message =
              "Anda tidak memiliki izin untuk mengakses sumber yang dimaksud";
        else if (e.response.statusCode == 404)
          message = "Sumber yang Anda maksud tidak ditemukan";
        else if (e.response.statusCode == 500)
          message = "Ada kesalahan di server\n${e.response.data}";
        break;
      default:
        message = "Kesalahan tidak diketahui";
    }
  } else
    message = "Ada kesalahan di client\n${e.toString()}";

  return message;
}
