import 'package:flutter/material.dart';
import 'package:kisi_flutter/generated/locale_base.dart';

class AppLocalizations {
  static const LocalizationsDelegate<LocaleBase> delegate =
      _AppLocalizationsDelegate();
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<LocaleBase> {
  const _AppLocalizationsDelegate();

  final idMap = const {
    'en': 'locales/en.json',
    'id': 'locales/id.json',
  };

  @override
  bool isSupported(Locale locale) => ['en', 'id'].contains(locale.languageCode);

  @override
  Future<LocaleBase> load(Locale locale) async {
    var lang = 'en';
    if (isSupported(locale)) lang = locale.languageCode;
    final loc = LocaleBase();
    await loc.load(idMap[lang]);
    return loc;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}
