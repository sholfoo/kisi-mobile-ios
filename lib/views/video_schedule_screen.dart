import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/dtos/get_schedule_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class VideoScheduleScreen extends StatelessWidget {
  final _model = _ViewModel();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _model,
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {
  GetScheduleData data;
  var state = ScreenState.content;
  var message = '';

  _ViewModel() {
    fetch();
  }

  fetch() async {
    state = ScreenState.progress;
    notifyListeners();

    try {
      var response = await api.getSchedule();
      data = GetScheduleDto.fromJson(response.data).data;

      if (data == null)
        state = ScreenState.empty;
      else
        state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);
      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      appBar: AppBar(
        title: Text(loc.main.schedule_title),
      ),
      body: Center(
        child: SimpleViewState(
          state: provider.state,
          message: provider.message,
          onRetry: () => provider.fetch(),
          child: (ctx) {
            var schedule =
                DateTime.parse("${provider.data.date} ${provider.data.time}");
            return Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    loc.main.schedule_title_body,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(
                      loc.main.schedule_text1,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(
                      loc.main.schedule_text2,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text(
                      DateFormat('dd MMM yyyy, HH:mm').format(schedule),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
