import 'package:flutter/material.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:kisi_flutter/views/register_screen/register_step.dart';
import 'package:kisi_flutter/widgets/phone_code_select.dart';
import 'package:kisi_flutter/widgets/text_cencored.dart';
import 'package:provider/provider.dart';
import 'package:string_validator/string_validator.dart';

import 'form_utils.dart';

class Step3Fragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(
      context,
    );

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return RegisterStep(
      title: loc.register.general_form_title,
      subtitle: loc.register.step3_form_subtitle,
      description: loc.register.general_form_description,
      child: Form(
        key: provider.step3FormKey,
        child: Column(
          children: <Widget>[
            (provider.wni())
                ? TextCencored(
                    title: loc.register.step3_identity_address,
                    text: provider.identityAddress.text,
                    multiLine: true,
                  )
                : TextFormField(
                    controller: provider.identityAddress,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.multiline,
                    minLines: 2,
                    maxLines: null,
                    decoration: defaultInputDecoration(
                      loc.register.step3_identity_address,
                      alignLabelWithHint: true,
                    ),
                  ),
            formInputSpacer,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: (provider.wni())
                      ? TextCencored(
                          title: loc.register.general_province,
                          text: provider.identityProvince.text,
                        )
                      : TextFormField(
                          controller: provider.identityProvince,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          decoration: defaultInputDecoration(
                              loc.register.general_province),
                        ),
                ),
                Container(width: 10),
                Expanded(
                  child: (provider.wni())
                      ? TextCencored(
                          title: loc.register.general_city,
                          text: provider.identityCity.text,
                          multiLine: true,
                        )
                      : TextFormField(
                          controller: provider.identityCity,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          decoration:
                              defaultInputDecoration(loc.register.general_city),
                        ),
                ),
              ],
            ),
            formInputSpacer,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: IgnorePointer(
                    ignoring: provider.wni(),
                    child: DropdownButtonFormField<String>(
                      value: provider.identityCountry,
                      items: provider.countryList,
                      onChanged: (val) => provider.identityCountry = val,
                      validator: (val) {
                        if (val == null)
                          return loc.general.general_required_field;
                        return null;
                      },
                      isDense: true,
                      isExpanded: true,
                      decoration:
                          defaultInputDecoration(loc.register.general_country),
                    ),
                  ),
                ),
                Container(width: 10),
                Expanded(
                  child: TextFormField(
                    controller: provider.identityPostal,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    decoration:
                        defaultInputDecoration(loc.register.general_zip),
                  ),
                ),
              ],
            ),
            formSectionSpacer,
            DropdownButtonFormField<String>(
              value: provider.currentAddressState,
              items: provider.currentAddressStateItems,
              onChanged: (val) => provider.currentAddressState = val,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step3_address_state),
            ),
            Visibility(
              visible: provider.currentAddressState !=
                  provider.currentAddressStateItems[0].value,
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.otherAddress,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.multiline,
                    minLines: 2,
                    maxLines: null,
                    decoration: defaultInputDecoration(
                      loc.register.step3_current_address,
                      alignLabelWithHint: true,
                    ),
                  ),
                  formInputSpacer,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          controller: provider.otherProvince,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          decoration: defaultInputDecoration(
                              loc.register.general_province),
                        ),
                      ),
                      Container(width: 10),
                      Expanded(
                        child: TextFormField(
                          controller: provider.otherCity,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          decoration:
                              defaultInputDecoration(loc.register.general_city),
                        ),
                      ),
                    ],
                  ),
                  formInputSpacer,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: DropdownButtonFormField<String>(
                          value: provider.otherCountry,
                          items: provider.countryList,
                          onChanged: (val) => provider.otherCountry = val,
                          validator: (val) {
                            if (val == null)
                              return loc.general.general_required_field;
                            return null;
                          },
                          isDense: true,
                          isExpanded: true,
                          decoration: defaultInputDecoration(
                              loc.register.general_country),
                        ),
                      ),
                      Container(width: 10),
                      Expanded(
                        child: TextFormField(
                          controller: provider.otherPostal,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.number,
                          decoration:
                              defaultInputDecoration(loc.register.general_zip),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            formSectionSpacer,
            DropdownButtonFormField<String>(
              value: provider.homeState,
              items: provider.homeStatusItems,
              onChanged: (val) => provider.homeState = val,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step3_house_state),
            ),
            formInputSpacer,
            (provider.wni())
                ? TextCencored(
                    title: loc.register.step3_maiden_name,
                    text: provider.motherMaidenCtrl.text,
                    multiLine: true,
                  )
                : TextFormField(
                    controller: provider.motherMaidenCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.step3_maiden_name),
                  ),
            formInputSpacer,
            TextFormField(
              controller: provider.emailCtrl,
              keyboardType: TextInputType.emailAddress,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                if (!isEmail(val)) return loc.general.general_email_field;
                return null;
              },
              decoration: defaultInputDecoration(loc.register.step3_email),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.homePhoneNumberCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.phone,
              decoration: defaultInputDecoration(
                loc.register.step3_home_phone,
                prefix: PhoneCodeSelect(
                  currentValue: provider.homePhoneNumberCode,
                  items: provider.phoneCodeList,
                  onSelected: (val) => provider.homePhoneNumberCode = val,
                ),
              ),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.mobilePhoneNumberCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.phone,
              decoration: defaultInputDecoration(
                loc.register.step3_mobile_phone,
                prefix: PhoneCodeSelect(
                  currentValue: provider.mobilePhoneNumberCode,
                  items: provider.phoneCodeList,
                  onSelected: (val) => provider.mobilePhoneNumberCode = val,
                ),
              ),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.facsimileNumberCtrl,
              keyboardType: TextInputType.phone,
              decoration: defaultInputDecoration(
                loc.register.step3_fax,
                prefix: PhoneCodeSelect(
                  currentValue: provider.facsimileNumberCode,
                  items: provider.phoneCodeList,
                  onSelected: (val) => provider.facsimileNumberCode = val,
                ),
              ),
            ),
            formActionSpacer,
            SizedBox(
              width: double.infinity,
              child: MaterialButton(
                elevation: 0,
                highlightElevation: 0,
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () {
                  if (provider.step3FormKey.currentState.validate())
                    provider.nextPage();
                },
                child: Text(loc.register.general_next),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
