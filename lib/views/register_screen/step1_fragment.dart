import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:kisi_flutter/views/register_screen/register_step.dart';
import 'package:provider/provider.dart';

import 'form_utils.dart';

class Step1Fragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(
      context,
      listen: true,
    );

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return RegisterStep(
      title: loc.register.general_form_title,
      subtitle: loc.register.step1_form_subtitle,
      description: loc.register.general_form_description,
      child: Form(
        key: provider.step1FormKey,
        child: Column(
          children: <Widget>[
            DropdownButtonFormField<String>(
              value: provider.nationality,
              items: provider.nationalityItems,
              onChanged: (val) {
                provider.nationality = val;
              },
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step1_nationality),
            ),
            Visibility(
              visible: provider.wni(),
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.identityNumberCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      if (val.length < 16)
                        return loc.general.general_length_field
                            .replaceAll('%s', '16');
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 16,
                    decoration: defaultInputDecoration(
                        loc.register.step1_identity_number),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.placeOfBirthCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step1_place_of_birth),
                  ),
                  formInputSpacer,
                  DateTimeField(
                    initialValue: provider.dateOfBirth,
                    validator: (val) {
                      if (val == null)
                        return loc.general.general_required_field;
                      return null;
                    },
                    resetIcon: null,
                    format: DateFormat('dd MMMM yyyy'),
                    readOnly: true,
                    decoration: defaultInputDecoration(
                        loc.register.step1_date_of_birth),
                    onShowPicker: (context, currentValue) {
                      return showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDatePickerMode: DatePickerMode.year,
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100),
                      );
                    },
                    onChanged: (val) {
                      provider.dateOfBirth = val;
                    },
                  ),
                ],
              ),
            ),
            formActionSpacer,
            SizedBox(
              width: double.infinity,
              child: MaterialButton(
                elevation: 0,
                highlightElevation: 0,
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () async {
                  if (provider.step1FormKey.currentState.validate()) {
                    if (provider.wni()) {
                      var result = await provider.validateDukcapil(context);

                      if (result) provider.nextPage();
                    } else
                      provider.nextPage();
                  }
                },
                child: Text(loc.register.general_next),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
