import 'package:flutter/material.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/login_screen.dart';

import 'form_utils.dart';

class RegisterCompleteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          'assets/logo-kisi-long.png',
          height: 28,
        ),
        centerTitle: false,
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                loc.register.complete_title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  loc.register.complete_text_line1,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  loc.register.complete_text_line2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              formActionSpacer,
              SizedBox(
                width: double.infinity,
                child: MaterialButton(
                  elevation: 0,
                  highlightElevation: 0,
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  onPressed: () {
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (ctx) => LoginScreen(),
                      ),
                      (_) => false,
                    );
                  },
                  child: Text(loc.register.general_next),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
