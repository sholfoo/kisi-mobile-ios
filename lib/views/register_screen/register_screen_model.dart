import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/dtos/bank_dto.dart';
import 'package:kisi_flutter/dtos/bca_validation_dto.dart';
import 'package:kisi_flutter/dtos/business_dto.dart';
import 'package:kisi_flutter/dtos/country_dto.dart';
import 'package:kisi_flutter/dtos/dukcapil_dto.dart';
import 'package:kisi_flutter/dtos/phone_code_dto.dart';
import 'package:kisi_flutter/dtos/questionary_dto.dart';
import 'package:kisi_flutter/dtos/req_register_dto.dart';
import 'package:kisi_flutter/dtos/submit_register_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/register_complete_screen.dart';
import 'package:kisi_flutter/views/register_screen/step1_fragment.dart';
import 'package:kisi_flutter/views/register_screen/step2_fragment.dart';
import 'package:kisi_flutter/views/register_screen/step3_fragment.dart';
import 'package:kisi_flutter/views/register_screen/step4_fragment.dart';
import 'package:kisi_flutter/views/register_screen/step5_fragment.dart';
import 'package:kisi_flutter/views/register_screen/step6_fragment.dart';
import 'package:kisi_flutter/views/register_screen/step7_fragment.dart';

import 'package:kisi_flutter/services/api.dart' as api;
import 'package:kisi_flutter/views/register_screen/step8_fragment.dart';
import 'package:kisi_flutter/views/register_screen/step9_fragment.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:string_validator/string_validator.dart';

class RegisterScreenModel with ChangeNotifier {
  var _currentPage = 0;

  var step1FormKey = GlobalKey<FormState>();
  var step2FormKey = GlobalKey<FormState>();
  var step3FormKey = GlobalKey<FormState>();
  var step4FormKey = GlobalKey<FormState>();
  var step5FormKey = GlobalKey<FormState>();
  var step6FormKey = GlobalKey<FormState>();
  var step7FormKey = GlobalKey<FormState>();

  SubmitRegisterDto registerResult;
  var registerSubmitted = false;
  var scheduleSubmitted = false;
  var questionarySubmitted = false;
  var isShowQuestionare = false;

  // Step 1
  String _nationality = 'WNI';
  String get nationality => _nationality;
  set nationality(String nationality) {
    _nationality = nationality;
    notifyListeners();
  }

  bool wni() {
    return _nationality == 'WNI';
  }

  TextEditingController identityNumberCtrl = TextEditingController();
  TextEditingController placeOfBirthCtrl = TextEditingController();

  DateTime _dateOfBirth;
  DateTime get dateOfBirth => _dateOfBirth;
  set dateOfBirth(DateTime dateOfBirth) {
    _dateOfBirth = dateOfBirth;
    notifyListeners();
  }

  // Step 2
  TextEditingController clientFullNameCtrl = TextEditingController();

  String _gender = 'Male';
  String get gender => _gender;
  set gender(String gender) {
    _gender = gender;
    notifyListeners();
  }

  String _countryOfOrigin;
  String get countryOfOrigin => _countryOfOrigin;
  set countryOfOrigin(String countryOfOrigin) {
    _countryOfOrigin = countryOfOrigin;
    notifyListeners();
  }

  String _usTaxpayer = 'No';
  String get usTaxpayer => _usTaxpayer;
  set usTaxpayer(String usTaxpayer) {
    _usTaxpayer = usTaxpayer;
    notifyListeners();
  }

  String _usGreenCardHolder = 'No';
  String get usGreenCardHolder => _usGreenCardHolder;
  set usGreenCardHolder(String usGreenCardHolder) {
    _usGreenCardHolder = usGreenCardHolder;
    notifyListeners();
  }

  String _bornInUs = 'No';
  String get bornInUs => _bornInUs;
  set bornInUs(String bornInUs) {
    _bornInUs = bornInUs;
    notifyListeners();
  }

  String _hasUsPhoneNumber = 'No';
  String get hasUsPhoneNumber => _hasUsPhoneNumber;
  set hasUsPhoneNumber(String hasUsPhoneNumber) {
    _hasUsPhoneNumber = hasUsPhoneNumber;
    notifyListeners();
  }

  String _hasUsMailingAddress = 'No';
  String get hasUsMailingAddress => _hasUsMailingAddress;
  set hasUsMailingAddress(String hasUsMailingAddress) {
    _hasUsMailingAddress = hasUsMailingAddress;
    notifyListeners();
  }

  TextEditingController usTinNumber = TextEditingController();

  String _identityType = 'identity card';
  String get identityType => _identityType;
  set identityType(String identityType) {
    _identityType = identityType;
    notifyListeners();
  }

  String _expiryStatus = 'Lifetime';
  String get expiryStatus => _expiryStatus;
  set expiryStatus(String expiryStatus) {
    _expiryStatus = expiryStatus;
    notifyListeners();
  }

  DateTime _expiryDate = DateTime(9998, 12, 31);
  DateTime get expiryDate => _expiryDate;
  set expiryDate(DateTime expiryDate) {
    _expiryDate = expiryDate;
    notifyListeners();
  }

  String _hasNpwp = 'No';
  String get hasNpwp => _hasNpwp;
  set hasNpwp(String hasNpwp) {
    _hasNpwp = hasNpwp;
    notifyListeners();
  }

  TextEditingController npwpCtrl = TextEditingController();

  String _educationBackground = 'Elementary';
  String get educationBackground => _educationBackground;
  set educationBackground(String educationBackground) {
    _educationBackground = educationBackground;
    notifyListeners();
  }

  String _maritalStatus = 'Single';
  String get maritalStatus => _maritalStatus;
  set maritalStatus(String maritalStatus) {
    _maritalStatus = maritalStatus;
    notifyListeners();
  }

  TextEditingController spouseCtrl = TextEditingController();

  String _religion = 'Islam';
  String get religion => _religion;
  set religion(String religion) {
    _religion = religion;
    notifyListeners();
  }

  // Step 3
  TextEditingController identityAddress = TextEditingController();
  TextEditingController identityProvince = TextEditingController();
  TextEditingController identityCity = TextEditingController();

  String _identityCountry = 'Indonesia';
  String get identityCountry => _identityCountry;
  set identityCountry(String identityCountry) {
    _identityCountry = identityCountry;
    notifyListeners();
  }

  TextEditingController identityPostal = TextEditingController();

  String _currentAddressState = 'The same address as stated in ID card';
  String get currentAddressState => _currentAddressState;
  set currentAddressState(String currentAddressState) {
    _currentAddressState = currentAddressState;
    notifyListeners();
  }

  TextEditingController otherAddress = TextEditingController();
  TextEditingController otherProvince = TextEditingController();
  TextEditingController otherCity = TextEditingController();

  String _otherCountry = 'Indonesia';
  String get otherCountry => _otherCountry;
  set otherCountry(String otherCountry) {
    _otherCountry = otherCountry;
    notifyListeners();
  }

  TextEditingController otherPostal = TextEditingController();
  TextEditingController motherMaidenCtrl = TextEditingController();

  String _homeState = 'Self Owned';
  String get homeState => _homeState;
  set homeState(String homeState) {
    _homeState = homeState;
    notifyListeners();
  }

  String _homePhoneNumberCode = '+62';
  String get homePhoneNumberCode => _homePhoneNumberCode;
  set homePhoneNumberCode(String homePhoneNumberCode) {
    _homePhoneNumberCode = homePhoneNumberCode;
    notifyListeners();
  }

  TextEditingController homePhoneNumberCtrl = TextEditingController();

  String _mobilePhoneNumberCode = '+62';
  String get mobilePhoneNumberCode => _mobilePhoneNumberCode;
  set mobilePhoneNumberCode(String mobilePhoneNumberCode) {
    _mobilePhoneNumberCode = mobilePhoneNumberCode;
    notifyListeners();
  }

  TextEditingController mobilePhoneNumberCtrl = TextEditingController();
  TextEditingController emailCtrl = TextEditingController();

  String _facsimileNumberCode = '+62';
  String get facsimileNumberCode => _facsimileNumberCode;
  set facsimileNumberCode(String facsimileNumberCode) {
    _facsimileNumberCode = facsimileNumberCode;
    notifyListeners();
  }

  TextEditingController facsimileNumberCtrl = TextEditingController();

  // Step 4
  String _occupation = 'Employee';
  String get occupation => _occupation;
  set occupation(String occupation) {
    _occupation = occupation;
    notifyListeners();
  }

  TextEditingController otherOccupationCtrl = TextEditingController();
  TextEditingController companyNameCtrl = TextEditingController();

  String _lineOfBusiness;
  String get lineOfBusiness => _lineOfBusiness;
  set lineOfBusiness(String lineOfBusiness) {
    _lineOfBusiness = lineOfBusiness;
    notifyListeners();
  }

  TextEditingController positionCtrl = TextEditingController();

  String _officePhoneNumberCode = '+62';
  String get officePhoneNumberCode => _officePhoneNumberCode;
  set officePhoneNumberCode(String officePhoneNumberCode) {
    _officePhoneNumberCode = officePhoneNumberCode;
    notifyListeners();
  }

  TextEditingController officePhoneNumberCtrl = TextEditingController();

  String _workFacsimileNumberCode = '+62';
  String get workFacsimileNumberCode => _workFacsimileNumberCode;
  set workFacsimileNumberCode(String workFacsimileNumberCode) {
    _workFacsimileNumberCode = workFacsimileNumberCode;
    notifyListeners();
  }

  TextEditingController workFacsimileNumberCtrl = TextEditingController();
  TextEditingController workEmailCtrl = TextEditingController();
  TextEditingController workLengthCtrl = TextEditingController();

  TextEditingController companyAddress = TextEditingController();
  TextEditingController companyProvince = TextEditingController();
  TextEditingController companyCity = TextEditingController();

  String _companyCountry = 'Indonesia';
  String get companyCountry => _companyCountry;
  set companyCountry(String companyCountry) {
    _companyCountry = companyCountry;
    notifyListeners();
  }

  TextEditingController companyPostal = TextEditingController();

  String _annualIncome = 'Below 10 Million';
  String get annualIncome => _annualIncome;
  set annualIncome(String annualIncome) {
    _annualIncome = annualIncome;
    notifyListeners();
  }

  String _primaryIncome = 'Salary';
  String get primaryIncome => _primaryIncome;
  set primaryIncome(String primaryIncome) {
    _primaryIncome = primaryIncome;
    notifyListeners();
  }

  TextEditingController otherPrimaryIncomeCtrl = TextEditingController();

  String _investmentFund = 'Salary';
  String get investmentFund => _investmentFund;
  set investmentFund(String investmentFund) {
    _investmentFund = investmentFund;
    notifyListeners();
  }

  TextEditingController otherInvestmentFundCtrl = TextEditingController();

  // Step 5
  TextEditingController heirNameCtrl = TextEditingController();
  TextEditingController heirRelationCtrl = TextEditingController();

  String _heirPhoneNumberCode = '+62';
  String get heirPhoneNumberCode => _heirPhoneNumberCode;
  set heirPhoneNumberCode(String heirPhoneNumberCode) {
    _heirPhoneNumberCode = heirPhoneNumberCode;
    notifyListeners();
  }

  TextEditingController heirPhoneNumberCtrl = TextEditingController();

  String _heirMobilePhoneNumberCode = '+62';
  String get heirMobilePhoneNumberCode => _heirMobilePhoneNumberCode;
  set heirMobilePhoneNumberCode(String heirMobilePhoneNumberCode) {
    _heirMobilePhoneNumberCode = heirMobilePhoneNumberCode;
    notifyListeners();
  }

  TextEditingController heirMobilePhoneNumberCtrl = TextEditingController();

  TextEditingController emergencyNameCtrl = TextEditingController();
  TextEditingController emergencyRelationCtrl = TextEditingController();

  String _emergencyPhoneNumberCode = '+62';
  String get emergencyPhoneNumberCode => _emergencyPhoneNumberCode;
  set emergencyPhoneNumberCode(String emergencyPhoneNumberCode) {
    _emergencyPhoneNumberCode = emergencyPhoneNumberCode;
    notifyListeners();
  }

  TextEditingController emergencyPhoneNumberCtrl = TextEditingController();

  String _emergencyMobilePhoneNumberCode = '+62';
  String get emergencyMobilePhoneNumberCode => _emergencyMobilePhoneNumberCode;
  set emergencyMobilePhoneNumberCode(String emergencyMobilePhoneNumberCode) {
    _emergencyMobilePhoneNumberCode = emergencyMobilePhoneNumberCode;
    notifyListeners();
  }

  TextEditingController emergencyMobilePhoneNumberCtrl =
      TextEditingController();

  TextEditingController emergencyAddress = TextEditingController();
  TextEditingController emergencyProvince = TextEditingController();
  TextEditingController emergencyCity = TextEditingController();

  String _emergencyCountry = 'Indonesia';
  String get emergencyCountry => _emergencyCountry;
  set emergencyCountry(String emergencyCountry) {
    _emergencyCountry = emergencyCountry;
    notifyListeners();
  }

  TextEditingController emergencyPostal = TextEditingController();

  String _correspondentAddress = 'ID Address';
  String get correspondentAddress => _correspondentAddress;
  set correspondentAddress(String correspondentAddress) {
    _correspondentAddress = correspondentAddress;
    notifyListeners();
  }

  TextEditingController correspondentAddressSpecifyCtrl =
      TextEditingController();

  String _confirmationMode = 'Email';
  String get confirmationMode => _confirmationMode;
  set confirmationMode(String confirmationMode) {
    _confirmationMode = confirmationMode;
    notifyListeners();
  }

  String _bankName;
  String get bankName => _bankName;
  set bankName(String bankName) {
    _bankName = bankName;
    notifyListeners();
  }

  TextEditingController bankBranchCtrl = TextEditingController();
  TextEditingController bankHolderNameCtrl = TextEditingController();
  TextEditingController bankTypeCtrl = TextEditingController();
  TextEditingController bankAccountCtrl = TextEditingController();

  String _riskTolerance = 'Risk Averse';
  String get riskTolerance => _riskTolerance;
  set riskTolerance(String riskTolerance) {
    _riskTolerance = riskTolerance;
    notifyListeners();
  }

  String _timeHorizon = 'Short Term';
  String get timeHorizon => _timeHorizon;
  set timeHorizon(String timeHorizon) {
    _timeHorizon = timeHorizon;
    notifyListeners();
  }

  String _investObjective = 'Price Appreciation';
  String get investObjective => _investObjective;
  set investObjective(String investObjective) {
    _investObjective = investObjective;
    notifyListeners();
  }

  String _addInfo1 = 'No';
  String get addInfo1 => _addInfo1;
  set addInfo1(String addInfo1) {
    _addInfo1 = addInfo1;
    notifyListeners();
  }

  TextEditingController addInfo1NameCtrl = TextEditingController();
  TextEditingController addInfo1CompanyCtrl = TextEditingController();
  TextEditingController addInfo1PositionCtrl = TextEditingController();

  String _addInfo2 = 'No';
  String get addInfo2 => _addInfo2;
  set addInfo2(String addInfo2) {
    _addInfo2 = addInfo2;
    notifyListeners();
  }

  TextEditingController addInfo2NameCtrl = TextEditingController();
  TextEditingController addInfo2CompanyCtrl = TextEditingController();
  TextEditingController addInfo2PositionCtrl = TextEditingController();

  String _addInfo3 = 'No';
  String get addInfo3 => _addInfo3;
  set addInfo3(String addInfo3) {
    _addInfo3 = addInfo3;
    notifyListeners();
  }

  TextEditingController addInfo3ShareCompanyCtrl = TextEditingController();

  String _addInfo4 = 'No';
  String get addInfo4 => _addInfo4;
  set addInfo4(String addInfo4) {
    _addInfo4 = addInfo4;
    notifyListeners();
  }

  TextEditingController addInfo4CompanyNameCtrl = TextEditingController();

  String _addInfo5 = 'No';
  String get addInfo5 => _addInfo5;
  set addInfo5(String addInfo5) {
    _addInfo5 = addInfo5;
    notifyListeners();
  }

  TextEditingController addInfo5PositionCtrl = TextEditingController();

  String _addInfo6 = 'No';
  String get addInfo6 => _addInfo6;
  set addInfo6(String addInfo6) {
    _addInfo6 = addInfo6;
    notifyListeners();
  }

  TextEditingController addInfo6NameCtrl = TextEditingController();
  TextEditingController addInfo6RelationCtrl = TextEditingController();
  TextEditingController addInfo6PositionCtrl = TextEditingController();

  // Step 6
  bool get mustFillStep6 =>
      occupation == 'House Wife' || occupation == 'Student';

  TextEditingController grdNameCtrl = TextEditingController();

  String _grdRelationship = 'Spouse';
  String get grdRelationship => _grdRelationship;
  set grdRelationship(String grdRelationship) {
    _grdRelationship = grdRelationship;
    notifyListeners();
  }

  TextEditingController grdRelationshipDetailCtrl = TextEditingController();

  String _grdPhoneNumberCode = '+62';
  String get grdPhoneNumberCode => _grdPhoneNumberCode;
  set grdPhoneNumberCode(String grdPhoneNumberCode) {
    _grdPhoneNumberCode = grdPhoneNumberCode;
    notifyListeners();
  }

  TextEditingController grdPhoneNumberCtrl = TextEditingController();

  String _grdMobilePhoneNumberCode = '+62';
  String get grdMobilePhoneNumberCode => _grdMobilePhoneNumberCode;
  set grdMobilePhoneNumberCode(String grdMobilePhoneNumberCode) {
    _grdMobilePhoneNumberCode = grdMobilePhoneNumberCode;
    notifyListeners();
  }

  TextEditingController grdMobilePhoneNumberCtrl = TextEditingController();

  String _grdIdentityType = 'identity card';
  String get grdIdentityType => _grdIdentityType;
  set grdIdentityType(String grdIdentityType) {
    _grdIdentityType = grdIdentityType;
    notifyListeners();
  }

  TextEditingController grdIdentityNumberCtrl = TextEditingController();

  String _grdExpiryStatus = 'Lifetime';
  String get grdExpiryStatus => _grdExpiryStatus;
  set grdExpiryStatus(String grdExpiryStatus) {
    _grdExpiryStatus = grdExpiryStatus;
    notifyListeners();
  }

  DateTime _grdExpiryDate = DateTime(9998, 12, 31);
  DateTime get grdExpiryDate => _grdExpiryDate;
  set grdExpiryDate(DateTime grdExpiryDate) {
    _grdExpiryDate = grdExpiryDate;
    notifyListeners();
  }

  String _grdOccupation = 'Employee';
  String get grdOccupation => _grdOccupation;
  set grdOccupation(String grdOccupation) {
    _grdOccupation = grdOccupation;
    notifyListeners();
  }

  TextEditingController grdOtherOccupationCtrl = TextEditingController();
  TextEditingController grdCompanyNameCtrl = TextEditingController();

  String _grdLineOfBusiness;
  String get grdLineOfBusiness => _grdLineOfBusiness;
  set grdLineOfBusiness(String grdLineOfBusiness) {
    _grdLineOfBusiness = grdLineOfBusiness;
    notifyListeners();
  }

  TextEditingController grdPositionCtrl = TextEditingController();

  TextEditingController grdCompanyAddress = TextEditingController();
  TextEditingController grdCompanyProvince = TextEditingController();
  TextEditingController grdCompanyCity = TextEditingController();

  String _grdCompanyCountry = 'Indonesia';
  String get grdCompanyCountry => _grdCompanyCountry;
  set grdCompanyCountry(String grdCompanyCountry) {
    _grdCompanyCountry = grdCompanyCountry;
    notifyListeners();
  }

  TextEditingController grdCompanyPostal = TextEditingController();

  String _grdAnnualIncome = 'Below 10 Million';
  String get grdAnnualIncome => _grdAnnualIncome;
  set grdAnnualIncome(String grdAnnualIncome) {
    _grdAnnualIncome = grdAnnualIncome;
    notifyListeners();
  }

  String _grdPrimaryIncome = 'Salary';
  String get grdPrimaryIncome => _grdPrimaryIncome;
  set grdPrimaryIncome(String grdPrimaryIncome) {
    _grdPrimaryIncome = grdPrimaryIncome;
    notifyListeners();
  }

  TextEditingController grdOtherPrimaryIncomeCtrl = TextEditingController();

  // Step 7
  File _fileKtp;
  File get fileKtp => _fileKtp;
  set fileKtp(File fileKtp) {
    _fileKtp = fileKtp;
    notifyListeners();
  }

  File _fileSwafotoKtp;
  File get fileSwafotoKtp => _fileSwafotoKtp;
  set fileSwafotoKtp(File fileSwafotoKtp) {
    _fileSwafotoKtp = fileSwafotoKtp;
    notifyListeners();
  }

  File _fileNpwp;
  File get fileNpwp => _fileNpwp;
  set fileNpwp(File fileNpwp) {
    _fileNpwp = fileNpwp;
    notifyListeners();
  }

  File _fileSignature;
  File get fileSignature => _fileSignature;
  set fileSignature(File fileSignature) {
    _fileSignature = fileSignature;
    notifyListeners();
  }

  // Step 8
  DateTime _schedule;
  DateTime get schedule => _schedule;
  set schedule(DateTime schedule) {
    _schedule = schedule;
    notifyListeners();
  }

  // ===
  // Dropdown Items
  // ===
  List<DropdownMenuItem<String>> nationalityItems = [];
  List<DropdownMenuItem<String>> genderItems = [];
  List<DropdownMenuItem<String>> identityTypeItems = [];
  List<DropdownMenuItem<String>> expiryStatusItem = [];
  List<DropdownMenuItem<String>> yesNoItems = [];
  List<DropdownMenuItem<String>> yesNoSpecifyItems = [];
  List<DropdownMenuItem<String>> educationBackgroundItems = [];
  List<DropdownMenuItem<String>> maritalStatusItems = [];
  List<DropdownMenuItem<String>> religionItems = [];
  List<DropdownMenuItem<String>> currentAddressStateItems = [];
  List<DropdownMenuItem<String>> homeStatusItems = [];
  List<DropdownMenuItem<String>> occupationItems = [];
  List<DropdownMenuItem<String>> annualIncomeItems = [];
  List<DropdownMenuItem<String>> primaryIncomeItems = [];
  List<DropdownMenuItem<String>> investFundItems = [];
  List<DropdownMenuItem<String>> correspondentAddressItems = [];
  List<DropdownMenuItem<String>> confirmationModeItems = [];
  List<DropdownMenuItem<String>> riskToleranceItems = [];
  List<DropdownMenuItem<String>> timeHorizonItems = [];
  List<DropdownMenuItem<String>> investObjectiveItems = [];
  List<DropdownMenuItem<String>> relationshipItems = [];

  List<DropdownMenuItem<String>> countryList = [];
  List<PopupMenuItem<String>> phoneCodeList = [];
  List<DropdownMenuItem<String>> businessList = [];
  List<DropdownMenuItem<String>> bankList = [];

  List<QuestionaryDataDto> questionList = [];
  List<String> questionAnswerList = [];

  updateQuestionAnswer(int idx, String value) {
    questionAnswerList[idx] = value;
    notifyListeners();
  }

  var state = ScreenState.content;
  var message = '';

  RegisterScreenModel() {
    setup();
  }

  setup() async {
    state = ScreenState.progress;
    notifyListeners();

    try {
      await Future.wait([
        fetchCountry(),
        fetchPhoneCodes(),
        fetchBusiness(),
        fetchBanks(),
        fetchQuestions()
      ]);

      state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);
      notifyListeners();
    }
  }

  int get currentPage => _currentPage;
  set currentPage(int value) {
    _currentPage = value;
    notifyListeners();
  }

  prevPage() {
    currentPage--;
  }

  nextPage() {
    currentPage++;
  }

  var pages = [
    Step1Fragment(),
    Step2Fragment(),
    Step3Fragment(),
    Step4Fragment(),
    Step5Fragment(),
    Step6Fragment(),
    Step7Fragment(),
    Step8Fragment(),
    Step9Fragment(),
  ];

  Widget get activePage => pages[_currentPage];

  fetchSelectionItems(BuildContext context) {
    // Load selection items
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    nationalityItems = [
      DropdownMenuItem(
        value: 'WNI',
        child: Text(loc.register.item_nationality_wni),
      ),
      DropdownMenuItem(
        value: 'WNA',
        child: Text(loc.register.item_nationality_wna),
      ),
    ];

    genderItems = [
      DropdownMenuItem(
        value: 'Male',
        child: Text(loc.register.item_gender_male),
      ),
      DropdownMenuItem(
        value: 'Female',
        child: Text(loc.register.item_gender_female),
      ),
    ];

    identityTypeItems = [
      DropdownMenuItem(
        value: 'identity card',
        child: Text(loc.register.item_id_type_ktp),
      ),
      DropdownMenuItem(
        value: 'passport',
        child: Text(loc.register.item_id_type_passport),
      ),
      DropdownMenuItem(
        value: 'kims/kitas',
        child: Text(loc.register.item_id_type_kims),
      )
    ];

    expiryStatusItem = [
      DropdownMenuItem(
        value: 'Lifetime',
        child: Text(loc.register.item_id_exp_lifetime),
      ),
      DropdownMenuItem(
        value: 'Has Expired',
        child: Text(loc.register.item_id_exp_has_expired),
      ),
    ];

    yesNoItems = [
      DropdownMenuItem(
        value: 'Yes',
        child: Text(loc.register.item_yes),
      ),
      DropdownMenuItem(
        value: 'No',
        child: Text(loc.register.item_no),
      ),
    ];

    yesNoSpecifyItems = [
      DropdownMenuItem(
        value: 'Yes',
        child: Text(loc.register.item_yes_specify),
      ),
      DropdownMenuItem(
        value: 'No',
        child: Text(loc.register.item_no),
      ),
    ];

    educationBackgroundItems = [
      DropdownMenuItem(
        value: 'Elementary',
        child: Text(loc.register.item_edu_elementary),
      ),
      DropdownMenuItem(
        value: 'Junior High School',
        child: Text(loc.register.item_edu_junior),
      ),
      DropdownMenuItem(
        value: 'Senior High School',
        child: Text(loc.register.item_edu_senior),
      ),
      DropdownMenuItem(
        value: 'Diploma',
        child: Text(loc.register.item_edu_diploma),
      ),
      DropdownMenuItem(
        value: 'Bachelor Degree',
        child: Text(loc.register.item_edu_bachelor),
      ),
      DropdownMenuItem(
        value: 'Master Degree',
        child: Text(loc.register.item_edu_master),
      ),
      DropdownMenuItem(
        value: 'Doctor Degree',
        child: Text(loc.register.item_edu_doctor),
      ),
    ];

    maritalStatusItems = [
      DropdownMenuItem(
        value: 'Single',
        child: Text(loc.register.item_marital_single),
      ),
      DropdownMenuItem(
        value: 'Married',
        child: Text(loc.register.item_marital_married),
      ),
      DropdownMenuItem(
        value: 'Divorce',
        child: Text(loc.register.item_marital_divorce),
      ),
      DropdownMenuItem(
        value: 'Widow/Widower',
        child: Text(loc.register.item_marital_widow),
      )
    ];

    religionItems = [
      DropdownMenuItem(
        value: 'Islam',
        child: Text(loc.register.item_religion_islam),
      ),
      DropdownMenuItem(
        value: 'Catholic',
        child: Text(loc.register.item_religion_catholic),
      ),
      DropdownMenuItem(
        value: 'Christian',
        child: Text(loc.register.item_religion_christian),
      ),
      DropdownMenuItem(
        value: 'Hinduism',
        child: Text(loc.register.item_religion_hinduism),
      ),
      DropdownMenuItem(
        value: 'Buddhism',
        child: Text(loc.register.item_religion_buddhism),
      ),
      DropdownMenuItem(
        value: 'Khong Hu Cu',
        child: Text(loc.register.item_religion_khonhuchu),
      )
    ];

    currentAddressStateItems = [
      DropdownMenuItem(
        value: 'The same address as stated in ID card',
        child: Text(loc.register.item_curr_address_same),
      ),
      DropdownMenuItem(
        value: 'Different from address stated in ID Card',
        child: Text(loc.register.item_curr_address_diff),
      ),
    ];

    homeStatusItems = [
      DropdownMenuItem(
        value: 'Self Owned',
        child: Text(loc.register.item_house_self),
      ),
      DropdownMenuItem(
        value: 'Parents Owned',
        child: Text(loc.register.item_house_parent),
      ),
      DropdownMenuItem(
        value: 'Official Resident',
        child: Text(loc.register.item_house_official),
      ),
      DropdownMenuItem(
        value: 'Rented House',
        child: Text(loc.register.item_house_rent),
      ),
    ];

    occupationItems = [
      DropdownMenuItem(
        value: 'Employee',
        child: Text(loc.register.item_occ_employee),
      ),
      DropdownMenuItem(
        value: 'Civil Servant',
        child: Text(loc.register.item_occ_servant),
      ),
      DropdownMenuItem(
        value: 'Self Employed',
        child: Text(loc.register.item_occ_self),
      ),
      DropdownMenuItem(
        value: 'Armed Force/Police',
        child: Text(loc.register.item_occ_army),
      ),
      DropdownMenuItem(
        value: 'Retiree',
        child: Text(loc.register.item_occ_retiree),
      ),
      DropdownMenuItem(
        value: 'House Wife',
        child: Text(loc.register.item_occ_house_wife),
      ),
      DropdownMenuItem(
        value: 'Student',
        child: Text(loc.register.item_occ_student),
      ),
      DropdownMenuItem(
        value: 'Teacher',
        child: Text(loc.register.item_occ_teacher),
      ),
      DropdownMenuItem(
        value: 'Others',
        child: Text(loc.register.item_occ_other),
      ),
    ];

    annualIncomeItems = [
      DropdownMenuItem(
        value: 'Below 10 Million',
        child: Text(loc.register.item_income_10),
      ),
      DropdownMenuItem(
        value: '10-50 million',
        child: Text(loc.register.item_income_50),
      ),
      DropdownMenuItem(
        value: '50-100 million',
        child: Text(loc.register.item_income_100),
      ),
      DropdownMenuItem(
        value: '100-500 million',
        child: Text(loc.register.item_income_500),
      ),
      DropdownMenuItem(
        value: '500-1000 million',
        child: Text(loc.register.item_income_1000),
      ),
      DropdownMenuItem(
        value: 'Above 1000 million',
        child: Text(loc.register.item_income_99999),
      ),
    ];

    primaryIncomeItems = [
      DropdownMenuItem(
        value: 'Bonus',
        child: Text(loc.register.item_income_src_bonus),
      ),
      DropdownMenuItem(
        value: 'Salary',
        child: Text(loc.register.item_income_src_salary),
      ),
      DropdownMenuItem(
        value: 'Investment',
        child: Text(loc.register.item_income_src_invest),
      ),
      DropdownMenuItem(
        value: 'Business Profit',
        child: Text(loc.register.item_income_src_business),
      ),
      DropdownMenuItem(
        value: 'Spouse/Parent',
        child: Text(loc.register.item_income_src_spouse),
      ),
      DropdownMenuItem(
        value: 'Deposit',
        child: Text(loc.register.item_income_src_saving),
      ),
      DropdownMenuItem(
        value: 'Grant',
        child: Text(loc.register.item_income_src_grant),
      ),
      DropdownMenuItem(
        value: 'Loan',
        child: Text(loc.register.item_income_src_loan),
      ),
      DropdownMenuItem(
        value: 'Rent',
        child: Text(loc.register.item_income_src_rent),
      ),
      DropdownMenuItem(
        value: 'Others',
        child: Text(loc.register.item_income_src_other),
      ),
    ];

    investFundItems = [
      DropdownMenuItem(
        value: 'Bonus',
        child: Text(loc.register.item_invest_fund_bonus),
      ),
      DropdownMenuItem(
        value: 'Salary',
        child: Text(loc.register.item_invest_fund_salary),
      ),
      DropdownMenuItem(
        value: 'Investment',
        child: Text(loc.register.item_invest_fund_invest),
      ),
      DropdownMenuItem(
        value: 'Business Profit',
        child: Text(loc.register.item_invest_fund_business),
      ),
      DropdownMenuItem(
        value: 'Others',
        child: Text(loc.register.item_invest_fund_other),
      ),
    ];

    correspondentAddressItems = [
      DropdownMenuItem(
        value: 'ID Address',
        child: Text(loc.register.item_correspondent_addr_id),
      ),
      DropdownMenuItem(
        value: 'Residence Address',
        child: Text(loc.register.item_correspondent_addr_current),
      ),
      DropdownMenuItem(
        value: 'Office Address',
        child: Text(loc.register.item_correspondent_addr_office),
      ),
      DropdownMenuItem(
        value: 'Others Address',
        child: Text(loc.register.item_correspondent_addr_other),
      ),
    ];

    confirmationModeItems = [
      DropdownMenuItem(
        value: 'Email',
        child: Text(loc.register.item_confirm_email),
      ),
      DropdownMenuItem(
        value: 'Fax',
        child: Text(loc.register.item_confirm_fax),
      ),
    ];

    riskToleranceItems = [
      DropdownMenuItem(
        value: 'Risk Averse',
        child: Text(loc.register.item_risk_averse),
      ),
      DropdownMenuItem(
        value: 'Risk Neutral',
        child: Text(loc.register.item_risk_neutral),
      ),
      DropdownMenuItem(
        value: 'Risk Taker',
        child: Text(loc.register.item_risk_taker),
      ),
    ];

    timeHorizonItems = [
      DropdownMenuItem(
        value: 'Short Term',
        child: Text(loc.register.item_time_hrz_short),
      ),
      DropdownMenuItem(
        value: 'Medium Term',
        child: Text(loc.register.item_time_hrz_medium),
      ),
      DropdownMenuItem(
        value: 'Long Term',
        child: Text(loc.register.item_time_hrz_long),
      ),
    ];

    investObjectiveItems = [
      DropdownMenuItem(
        value: 'Price Appreciation',
        child: Text(loc.register.item_invest_obj_price),
      ),
      DropdownMenuItem(
        value: 'Income',
        child: Text(loc.register.item_invest_obj_income),
      ),
      DropdownMenuItem(
        value: 'Investment',
        child: Text(loc.register.item_invest_obj_invest),
      ),
      DropdownMenuItem(
        value: 'Speculation',
        child: Text(loc.register.item_invest_obj_speculation),
      ),
    ];

    relationshipItems = [
      DropdownMenuItem(
        value: 'Spouse',
        child: Text(loc.register.item_relation_spouse),
      ),
      DropdownMenuItem(
        value: 'Parent',
        child: Text(loc.register.item_relation_parent),
      ),
      DropdownMenuItem(
        value: 'Guardian',
        child: Text(loc.register.item_relation_guardian),
      ),
    ];

    // Notify listener, trigger ui rebuild
    notifyListeners();
  }

  Future<bool> fetchCountry() async {
    var response = await api.countries();

    var data = (response.data as List)
        .map<CountryDto>((x) => CountryDto.fromJson(x))
        .toList();

    countryList.clear();
    countryList.addAll(
      data.map<DropdownMenuItem<String>>(
        (x) => DropdownMenuItem(
          value: x.nicename,
          child: Text(
            x.nicename,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ),
      ),
    );

    return Future.value(true);
  }

  Future<bool> fetchPhoneCodes() async {
    var response = await api.phoneCodes();

    var data = (response.data as List)
        .map<PhoneCodeDto>((x) => PhoneCodeDto.fromJson(x))
        .toList();

    phoneCodeList.clear();
    phoneCodeList.addAll(
      data.map<PopupMenuItem<String>>(
        (x) => PopupMenuItem(
          value: x.phonecode,
          child: Text(
            '${x.nicename} (${x.phonecode})',
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
          ),
        ),
      ),
    );

    return Future.value(true);
  }

  Future<bool> fetchBusiness() async {
    var response = await api.business();

    var data = (response.data as List)
        .map<BusinessDto>((x) => BusinessDto.fromJson(x))
        .toList();

    businessList.clear();
    businessList.addAll(
      data.map<DropdownMenuItem<String>>(
        (x) => DropdownMenuItem(
          value: x.businessName,
          child: Text(
            x.businessName,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ),
      ),
    );

    return Future.value(true);
  }

  Future<bool> fetchBanks() async {
    var response = await api.banks();

    var data = (response.data as List)
        .map<BankDto>((x) => BankDto.fromJson(x))
        .toList();

    bankList.clear();
    bankList.addAll(
      data.map<DropdownMenuItem<String>>(
        (x) => DropdownMenuItem(
          value: x.bankName,
          child: Text(
            x.bankName,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
        ),
      ),
    );

    return Future.value(true);
  }

  Future<bool> fetchQuestions() async {
    var response = await api.questions();

    var result = QuestionaryDto.fromJson(response.data);

    if(result.showQuestionnaire){
      questionList.clear();
      questionList.addAll(result.data);

      questionAnswerList.clear();
      questionAnswerList.addAll(result.data.map<String>((x) => null));

      isShowQuestionare = true;
    } else {
      isShowQuestionare = false;
    }

    return Future.value(true);
  }

  Future<bool> validateDukcapil(BuildContext ctx) async {
    try {
      progressDialog(ctx);
      var response = await api.validateDukcapil(
        idNumber: identityNumberCtrl.text,
        dateOfBirth: dateOfBirth,
        placeOfBirth: placeOfBirthCtrl.text,
      );
      Navigator.pop(ctx);

      var result = DukcapilDto.fromJson(response.data);
      if (result.success) {
        clientFullNameCtrl.text = result.data.clientFullName;
        identityAddress.text = result.data.identityAddress;
        identityProvince.text = result.data.identityProvince;
        identityCity.text = result.data.identityCity;
        if (result.data.gender.toLowerCase() == 'laki-laki')
          gender = 'Male';
        else
          gender = 'Female';
        countryOfOrigin = 'Indonesia';
        motherMaidenCtrl.text = result.data.mothersMaidenName;
        return true;
      }

      error(ctx, message: result.message);
      return false;
    } catch (e) {
      Navigator.pop(ctx);
      error(ctx, message: api.parseError(e));
      return false;
    }
  }

  Future<bool> validateBca(BuildContext ctx) async {
    if (bankName.toLowerCase() != "PT. Bank Central Asia".toLowerCase())
      return true;

    try {
      progressDialog(ctx);
      var response = await api.validateBca(
        holderName: bankHolderNameCtrl.text,
        cardNumber: bankAccountCtrl.text,
      );
      Navigator.pop(ctx);

      var result = BcaValidationDto.fromJson(response.data);
      if (result.success) return true;

      error(ctx, message: result.message);
      return false;
    } catch (e) {
      Navigator.pop(ctx);
      error(ctx, message: api.parseError(e));
      return false;
    }
  }

  ReqRegisterDto buildRegisterModel() {
    var tmp = ReqRegisterDto();
    // Step 1 & 2
    tmp.nationality = nationality;
    tmp.nationalityOrigin = countryOfOrigin;
    tmp.clientFullName = clientFullNameCtrl.text;
    tmp.gender = gender;
    tmp.placeOfBirth = placeOfBirthCtrl.text;
    tmp.dateOfBirth = dateOfBirth;
    tmp.idType = identityType;
    tmp.idNumber = identityNumberCtrl.text;
    tmp.idExpiredDate = expiryDate;
    tmp.npwp = npwpCtrl.text;
    tmp.usTaxPayer = usTaxpayer;
    tmp.usGreenCardHolder = usGreenCardHolder;
    tmp.bornInUs = bornInUs;
    tmp.usPhoneNumber = hasUsPhoneNumber;
    tmp.usCorrespondentAddress = hasUsMailingAddress;
    tmp.usTinNumber = usTinNumber.text;
    tmp.educationalBackground = educationBackground;
    tmp.maritalStatus = maritalStatus;
    tmp.spouseName = spouseCtrl.text;
    tmp.religion = religion;
    // Tahap 3
    tmp.identityAddress = identityAddress.text;
    tmp.identityProvince = identityProvince.text;
    tmp.identityCity = identityCity.text;
    tmp.identityCountry = identityCountry;
    tmp.identityPostal = identityPostal.text;
    tmp.currentAddress = currentAddressState;
    tmp.othersStreet = otherAddress.text;
    tmp.othersProvince = otherProvince.text;
    tmp.othersCity = otherCity.text;
    tmp.othersCountry = otherCountry;
    tmp.othersPostal = otherPostal.text;
    tmp.homeStatus = homeState;
    tmp.mothersMaidenName = motherMaidenCtrl.text;
    tmp.emailAddress = emailCtrl.text;
    tmp.homePhoneNumber = homePhoneNumberCode + homePhoneNumberCtrl.text;
    tmp.mobilePhoneNumber = mobilePhoneNumberCode + mobilePhoneNumberCtrl.text;
    tmp.facsimileNumber = facsimileNumberCode + facsimileNumberCtrl.text;
    // Tahap 4
    tmp.occupation = occupation;
    tmp.othersOccupation = otherOccupationCtrl.text;
    tmp.companyName = companyNameCtrl.text;
    tmp.lineOfBusiness = lineOfBusiness;
    tmp.position = positionCtrl.text;
    tmp.officePhoneNumber = officePhoneNumberCode + officePhoneNumberCtrl.text;
    tmp.workFacsimileNumber =
        workFacsimileNumberCode + workFacsimileNumberCtrl.text;
    tmp.workEmailAddress = workEmailCtrl.text;
    tmp.lengthOfWork = toInt(workLengthCtrl.text);
    tmp.companyAddress = companyAddress.text;
    tmp.companyProvince = companyProvince.text;
    tmp.companyCity = companyCity.text;
    tmp.companyCountry = companyCountry;
    tmp.companyPostal = companyPostal.text;
    tmp.annualIncome = annualIncome;
    tmp.sourceOfPrimaryIncome = primaryIncome;
    tmp.othersSourceOfPrimaryIncome = otherPrimaryIncomeCtrl.text;
    tmp.sourceOfInvestmentFund = investmentFund;
    tmp.othersSourceOfInvestmentFund = otherInvestmentFundCtrl.text;
    // Tahap 5
    tmp.heirName = heirNameCtrl.text;
    tmp.heirRelationship = heirRelationCtrl.text;
    tmp.heirPhone = heirPhoneNumberCode + heirPhoneNumberCtrl.text;
    tmp.heirMobileNumber =
        heirMobilePhoneNumberCode + heirMobilePhoneNumberCtrl.text;
    tmp.emergencyName = emergencyNameCtrl.text;
    tmp.emergencyRelation = emergencyRelationCtrl.text;
    tmp.emergencyPhone =
        emergencyPhoneNumberCode + emergencyPhoneNumberCtrl.text;
    tmp.emergencyMobilePhone =
        emergencyMobilePhoneNumberCode + emergencyMobilePhoneNumberCtrl.text;
    tmp.emergencyAddress = emergencyAddress.text;
    tmp.emergencyProvince = emergencyProvince.text;
    tmp.emergencyCity = emergencyCity.text;
    tmp.emergencyCountry = emergencyCountry;
    tmp.emergencyZip = emergencyPostal.text;
    tmp.correspondentAddress = correspondentAddress;
    tmp.correspondentAddressSpecify = correspondentAddressSpecifyCtrl.text;
    tmp.confirmationService = confirmationMode;
    tmp.bankName = bankName;
    tmp.bankBranch = bankBranchCtrl.text;
    tmp.accountHolderName = bankHolderNameCtrl.text;
    tmp.typeOfAccount = bankTypeCtrl.text;
    tmp.accountNumber = bankAccountCtrl.text;
    tmp.riskTolerance = riskTolerance;
    tmp.timeHorizon = timeHorizon;
    tmp.investmentObjective = investObjective;
    tmp.addInfo_1 = addInfo1;
    tmp.addInfo_1Name = addInfo1NameCtrl.text;
    tmp.addInfo_1Company = addInfo1CompanyCtrl.text;
    tmp.addInfo_1Position = addInfo1PositionCtrl.text;
    tmp.addInfo_2 = addInfo2;
    tmp.addInfo_2Name = addInfo2NameCtrl.text;
    tmp.addInfo_2Company = addInfo2CompanyCtrl.text;
    tmp.addInfo_2Position = addInfo2PositionCtrl.text;
    tmp.addInfo_3 = addInfo3;
    tmp.addInfo_3ShareCompany = addInfo3ShareCompanyCtrl.text;
    tmp.addInfo_4 = addInfo4;
    tmp.addInfo_4CompanyName = addInfo4CompanyNameCtrl.text;
    tmp.addInfo_5 = addInfo5;
    tmp.addInfo_5Position = addInfo5PositionCtrl.text;
    tmp.addInfo_6 = addInfo6;
    tmp.addInfo_6Name = addInfo6NameCtrl.text;
    tmp.addInfo_6Relation = addInfo6RelationCtrl.text;
    tmp.addInfo_6Position = addInfo6PositionCtrl.text;
    // Tahap 6
    tmp.guardiansName = grdNameCtrl.text;
    tmp.guardiansRelationship = grdRelationship;
    tmp.guardiansRelationshipOthers = grdRelationshipDetailCtrl.text;
    tmp.mobileNumberGuardian =
        grdMobilePhoneNumberCode + grdMobilePhoneNumberCtrl.text;
    tmp.idTypeGuardian = grdIdentityType;
    tmp.idNumberGuardian = grdIdentityNumberCtrl.text;
    tmp.idExpiredDateGuardian = grdExpiryDate;
    tmp.occGuardian = grdOccupation;
    tmp.othersOccGuardian = grdOtherOccupationCtrl.text;
    tmp.companyNameGuardian = grdCompanyNameCtrl.text;
    tmp.lineOfBusinessGuardian = grdLineOfBusiness;
    tmp.positionGuardian = grdPositionCtrl.text;
    tmp.companyAddressGuardian = grdCompanyAddress.text;
    tmp.companyProvinceGuardian = grdCompanyProvince.text;
    tmp.companyCityGuardian = grdCompanyCity.text;
    tmp.companyCountryGuardian = grdCompanyCountry;
    tmp.companyZipGuardian = grdCompanyPostal.text;
    tmp.annualIncomeGuardian = grdAnnualIncome;
    tmp.sourceOfPrimaryIncomeGuardian = grdPrimaryIncome;
    tmp.othersSourceOfPrimaryIncomeGuardian = grdOtherPrimaryIncomeCtrl.text;
    // Step 7
    tmp.fileKtp = fileKtp;
    tmp.fileNpwp = fileNpwp;
    tmp.fileSwafotoKtp = fileSwafotoKtp;
    tmp.fileSignature = fileSignature;

    return tmp;
  }
}
