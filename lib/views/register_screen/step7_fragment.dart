import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:kisi_flutter/views/register_screen/register_step.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:signature/signature.dart';

import 'form_utils.dart';

class Step7Fragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {}

class _Content extends StatelessWidget {
  final _signPad = Signature(
    height: 250,
    backgroundColor: Colors.white,
  );

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(context);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return RegisterStep(
      title: loc.register.general_form_title,
      subtitle: loc.register.step7_form_subtitle,
      description: loc.register.step7_form_description,
      child: Form(
        key: provider.step7FormKey,
        child: Column(
          children: <Widget>[
            ImageFormField(
              initialValue: provider.fileKtp,
              onChanged: (val) => provider.fileKtp = val,
              validator: (val) {
                if (val == null) return loc.general.general_required_field;
                return null;
              },
              labelText: loc.register.step7_ktp,
            ),
            formInputSpacer,
            ImageFormField(
              initialValue: provider.fileSwafotoKtp,
              onChanged: (val) => provider.fileSwafotoKtp = val,
              validator: (val) {
                if (val == null) return loc.general.general_required_field;
                return null;
              },
              labelText: loc.register.step7_selfie_ktp,
            ),
            formInputSpacer,
            ImageFormField(
              initialValue: provider.fileNpwp,
              onChanged: (val) => provider.fileNpwp = val,
              validator: (val) {
                if (provider.hasNpwp == 'Yes' && val == null)
                  return loc.general.general_required_field;
                return null;
              },
              labelText: loc.register.step7_npwp,
            ),
            formInputSpacer,
            FormField(
              validator: (val) {
                if (_signPad.isEmpty)
                  return loc.register.step7_signature_validation;
                return null;
              },
              builder: (state) => Stack(
                children: <Widget>[
                  Container(
                    child: InputDecorator(
                      decoration: InputDecoration(
                        errorText: state.errorText,
                        contentPadding: EdgeInsets.all(12),
                        border: OutlineInputBorder(),
                        labelText: loc.register.step7_signature,
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: GestureDetector(
                          onVerticalDragUpdate: (_) {},
                          child: _signPad,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 16,
                    top: 16,
                    child: InkWell(
                      onTap: () {
                        _signPad.clear();
                      },
                      child: Padding(
                        padding: EdgeInsets.all(4),
                        child: Icon(Icons.clear),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            formActionSpacer,
            SizedBox(
              width: double.infinity,
              child: MaterialButton(
                elevation: 0,
                highlightElevation: 0,
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () async {
                  if (provider.step7FormKey.currentState.validate()) {
                    // Save signature to file
                    var path = await getTemporaryDirectory();
                    var fileSignature = File(path.path + '/signature.png');
                    fileSignature.writeAsBytes(await _signPad.exportBytes());

                    provider.fileSignature = fileSignature;

                    // Open next page
                    provider.nextPage();
                  }
                },
                child: Text(loc.register.general_next),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ImageFormField extends FormField<File> {
  ImageFormField({
    FormFieldSetter<File> onSaved,
    FormFieldValidator<File> validator,
    Function(File) onChanged,
    String labelText = '',
    File initialValue,
    bool autovalidate = false,
  }) : super(
          onSaved: onSaved,
          validator: validator,
          initialValue: initialValue,
          autovalidate: autovalidate,
          builder: (FormFieldState<File> state) {
            final loc = Localizations.of<LocaleBase>(state.context, LocaleBase);

            return Container(
              child: InputDecorator(
                isFocused: false,
                isEmpty: false,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      state.value == null
                          ? Container()
                          : Image.file(
                              state.value,
                              height: 125,
                            ),
                      OutlineButton(
                        onPressed: () async {
                          var file = await ImagePicker.pickImage(
                            source: ImageSource.camera,
                            maxHeight: 512,
                            maxWidth: 512,
                          );
                          if (file != null) {
                            state.didChange(file);
                            if (onChanged != null) onChanged(file);
                          }
                        },
                        child: Text(loc.general.take_picture),
                      )
                    ],
                  ),
                ),
                decoration: InputDecoration(
                  errorText: state.errorText,
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 12,
                    horizontal: 12,
                  ),
                  border: OutlineInputBorder(),
                  labelText: labelText,
                ),
              ),
            );
          },
        );
}
