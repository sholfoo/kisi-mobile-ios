import 'package:flutter/material.dart';
import 'package:kisi_flutter/widgets/section.dart';

class RegisterStep extends StatelessWidget {
  final String title;
  final String subtitle;
  final String description;
  final Widget child;

  RegisterStep({
    this.title,
    this.subtitle,
    this.description,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 12,
                      ),
                    ),
                    Text(
                      subtitle,
                      style: TextStyle(
                        color: Colors.grey.shade800,
                        fontSize: 16,
                      ),
                    ),
                    Container(height: 16),
                    Text(
                      description,
                      textAlign: TextAlign.justify,
                    )
                  ],
                ),
              ),
              AppPageSectionSpacer(),
              child,
            ],
          ),
        ),
      ),
    );
  }
}
