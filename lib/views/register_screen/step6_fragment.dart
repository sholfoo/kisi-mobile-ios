import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:kisi_flutter/views/register_screen/register_step.dart';
import 'package:kisi_flutter/widgets/phone_code_select.dart';
import 'package:provider/provider.dart';

import 'form_utils.dart';

class Step6Fragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(context);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    var requiredValidator = provider.mustFillStep6
        ? (val) {
            if (val == null) return loc.general.general_required_field;
            if (val is String && val.trim().isEmpty)
              return loc.general.general_required_field;
            return null;
          }
        : null;

    return RegisterStep(
      title: loc.register.general_form_title,
      subtitle: loc.register.step6_form_subtitle,
      description: loc.register.step6_form_description,
      child: Form(
        key: provider.step6FormKey,
        child: Column(
          children: <Widget>[
            TextFormField(
              controller: provider.grdNameCtrl,
              validator: requiredValidator,
              keyboardType: TextInputType.text,
              decoration: defaultInputDecoration(loc.register.step6_name),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.grdRelationship,
              items: provider.relationshipItems,
              onChanged: (val) => provider.grdRelationship = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step6_relation),
            ),
            Visibility(
              visible: provider.grdRelationship == 'Guardian',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.grdRelationshipDetailCtrl,
                    validator: requiredValidator,
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.step6_detail),
                  ),
                ],
              ),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.grdPhoneNumberCtrl,
              validator: requiredValidator,
              keyboardType: TextInputType.phone,
              decoration: defaultInputDecoration(
                loc.register.step6_phone,
                prefix: PhoneCodeSelect(
                  currentValue: provider.grdPhoneNumberCode,
                  items: provider.phoneCodeList,
                  onSelected: (val) => provider.grdPhoneNumberCode = val,
                ),
              ),
            ),
            formSectionSpacer,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: DropdownButtonFormField<String>(
                    value: provider.grdIdentityType,
                    items: provider.identityTypeItems,
                    onChanged: (val) => provider.grdIdentityType = val,
                    isDense: true,
                    isExpanded: true,
                    decoration:
                        defaultInputDecoration(loc.register.step6_id_type),
                  ),
                ),
                Container(width: 10),
                Expanded(
                  child: TextFormField(
                    controller: provider.grdIdentityNumberCtrl,
                    validator: requiredValidator,
                    keyboardType: TextInputType.number,
                    decoration:
                        defaultInputDecoration(loc.register.step6_id_number),
                  ),
                ),
              ],
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.grdExpiryStatus,
              items: provider.expiryStatusItem,
              onChanged: (val) => provider.grdExpiryStatus = val,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step6_id_expiry_status),
            ),
            formInputSpacer,
            Visibility(
              visible: provider.grdExpiryStatus != 'Lifetime',
              child: DateTimeField(
                initialValue: provider.grdExpiryDate,
                validator: (val) {
                  if (val == null) return loc.general.general_required_field;
                  return null;
                },
                resetIcon: null,
                format: DateFormat('dd MMMM yyyy'),
                readOnly: true,
                decoration:
                    defaultInputDecoration(loc.register.step6_id_expiry_date),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                    context: context,
                    firstDate: DateTime(1900),
                    initialDatePickerMode: DatePickerMode.year,
                    initialDate: currentValue ?? DateTime.now(),
                    lastDate: DateTime(2100),
                  );
                },
                onChanged: (val) => provider.grdExpiryDate = val,
              ),
            ),
            formSectionSpacer,
            DropdownButtonFormField<String>(
              value: provider.grdOccupation,
              items: provider.occupationItems,
              onChanged: (val) => provider.grdOccupation = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step6_occupation),
            ),
            Visibility(
              visible: provider.grdOccupation == 'Others',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.grdOtherOccupationCtrl,
                    validator: requiredValidator,
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step6_other_occupation),
                  ),
                ],
              ),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.grdCompanyNameCtrl,
              validator: requiredValidator,
              keyboardType: TextInputType.text,
              decoration:
                  defaultInputDecoration(loc.register.step6_company_name),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.grdLineOfBusiness,
              items: provider.businessList,
              onChanged: (val) => provider.grdLineOfBusiness = val,
              validator: requiredValidator,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step6_line_of_business),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.grdPositionCtrl,
              validator: requiredValidator,
              keyboardType: TextInputType.text,
              decoration: defaultInputDecoration(loc.register.step6_position),
            ),
            formSectionSpacer,
            TextFormField(
              controller: provider.grdCompanyAddress,
              validator: requiredValidator,
              keyboardType: TextInputType.multiline,
              minLines: 2,
              maxLines: null,
              decoration: defaultInputDecoration(
                loc.register.step6_company_address,
                alignLabelWithHint: true,
              ),
            ),
            formInputSpacer,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: TextFormField(
                    controller: provider.grdCompanyProvince,
                    validator: requiredValidator,
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.general_province),
                  ),
                ),
                Container(width: 10),
                Expanded(
                  child: TextFormField(
                    controller: provider.grdCompanyCity,
                    validator: requiredValidator,
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.general_city),
                  ),
                ),
              ],
            ),
            formInputSpacer,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: DropdownButtonFormField<String>(
                    value: provider.grdCompanyCountry,
                    items: provider.countryList,
                    onChanged: (val) => provider.grdCompanyCountry = val,
                    validator: requiredValidator,
                    isDense: true,
                    isExpanded: true,
                    decoration:
                        defaultInputDecoration(loc.register.general_country),
                  ),
                ),
                Container(width: 10),
                Expanded(
                  child: TextFormField(
                    controller: provider.grdCompanyPostal,
                    validator: requiredValidator,
                    keyboardType: TextInputType.number,
                    decoration:
                        defaultInputDecoration(loc.register.general_zip),
                  ),
                ),
              ],
            ),
            formSectionSpacer,
            DropdownButtonFormField<String>(
              value: provider.grdAnnualIncome,
              items: provider.annualIncomeItems,
              onChanged: (val) => provider.grdAnnualIncome = val,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step6_annual_income),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.grdPrimaryIncome,
              items: provider.primaryIncomeItems,
              onChanged: (val) => provider.grdPrimaryIncome = val,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step6_primary_source),
            ),
            Visibility(
              visible: provider.grdPrimaryIncome == 'Others',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.grdOtherPrimaryIncomeCtrl,
                    validator: requiredValidator,
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.step6_other_source),
                  ),
                ],
              ),
            ),
            // Fields Here
            formActionSpacer,
            SizedBox(
              width: double.infinity,
              child: MaterialButton(
                elevation: 0,
                highlightElevation: 0,
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () {
                  if (provider.step6FormKey.currentState.validate())
                    provider.nextPage();
                },
                child: Text(loc.register.general_next),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
