import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:kisi_flutter/views/register_screen/register_step.dart';
import 'package:kisi_flutter/widgets/text_cencored.dart';
import 'package:provider/provider.dart';

import 'form_utils.dart';

class Step2Fragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(context);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return RegisterStep(
      title: loc.register.general_form_title,
      subtitle: loc.register.step2_form_subtitle,
      description: loc.register.general_form_description,
      child: Form(
        key: provider.step2FormKey,
        child: Column(
          children: <Widget>[
            (provider.wni())
                ? TextCencored(
                    title: loc.register.step2_full_name,
                    text: provider.clientFullNameCtrl.text,
                  )
                : TextFormField(
                    controller: provider.clientFullNameCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    readOnly: provider.wni(),
                    decoration:
                        defaultInputDecoration(loc.register.step2_full_name),
                  ),
            formInputSpacer,
            IgnorePointer(
              ignoring: provider.wni(),
              child: DropdownButtonFormField<String>(
                value: provider.gender,
                items: provider.genderItems,
                onChanged: (val) => provider.gender = val,
                isDense: true,
                isExpanded: true,
                decoration: defaultInputDecoration(loc.register.step2_gender),
              ),
            ),
            formInputSpacer,
            IgnorePointer(
              ignoring: true,
              child: DropdownButtonFormField<String>(
                value: provider.nationality,
                items: provider.nationalityItems,
                isDense: true,
                isExpanded: true,
                decoration:
                    defaultInputDecoration(loc.register.step2_nationality),
              ),
            ),
            Visibility(
              visible: !provider.wni(),
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  DropdownButtonFormField<String>(
                    value: provider.countryOfOrigin,
                    items: provider.countryList,
                    onChanged: (val) => provider.countryOfOrigin = val,
                    validator: (val) {
                      if (val == null)
                        return loc.general.general_required_field;
                      return null;
                    },
                    isDense: true,
                    isExpanded: true,
                    decoration:
                        defaultInputDecoration(loc.register.step2_origin),
                  )
                ],
              ),
            ),
            Visibility(
              visible: provider.countryOfOrigin == 'United States',
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  formSectionSpacerLabel(loc.register.step2_us_taxpayer),
                  DropdownButtonFormField<String>(
                    value: provider.usTaxpayer,
                    items: provider.yesNoItems,
                    onChanged: (val) => provider.usTaxpayer = val,
                    isDense: true,
                    isExpanded: true,
                    decoration:
                        defaultInputDecoration(loc.register.general_answer),
                  ),
                  formInputSpacerLabel(loc.register.step2_us_green_card),
                  DropdownButtonFormField<String>(
                    value: provider.usGreenCardHolder,
                    items: provider.yesNoItems,
                    onChanged: (val) => provider.usGreenCardHolder = val,
                    isDense: true,
                    isExpanded: true,
                    decoration:
                        defaultInputDecoration(loc.register.general_answer),
                  ),
                  formInputSpacerLabel(loc.register.step2_born_in_us),
                  DropdownButtonFormField<String>(
                    value: provider.bornInUs,
                    items: provider.yesNoItems,
                    onChanged: (val) => provider.bornInUs = val,
                    isDense: true,
                    isExpanded: true,
                    decoration:
                        defaultInputDecoration(loc.register.general_answer),
                  ),
                  formInputSpacerLabel(loc.register.step2_us_telephone),
                  DropdownButtonFormField<String>(
                    value: provider.hasUsPhoneNumber,
                    items: provider.yesNoItems,
                    onChanged: (val) => provider.hasUsPhoneNumber = val,
                    isDense: true,
                    isExpanded: true,
                    decoration:
                        defaultInputDecoration(loc.register.general_answer),
                  ),
                  formInputSpacerLabel(loc.register.step2_us_mailing_address),
                  DropdownButtonFormField<String>(
                    value: provider.hasUsMailingAddress,
                    items: provider.yesNoItems,
                    onChanged: (val) => provider.hasUsMailingAddress = val,
                    isDense: true,
                    isExpanded: true,
                    decoration:
                        defaultInputDecoration(loc.register.general_answer),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.usTinNumber,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    readOnly: provider.wni(),
                    decoration:
                        defaultInputDecoration(loc.register.step2_us_tin),
                  ),
                ],
              ),
            ),
            formSectionSpacer,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: IgnorePointer(
                    ignoring: provider.wni(),
                    child: DropdownButtonFormField<String>(
                      value: provider.identityType,
                      items: provider.identityTypeItems,
                      onChanged: (val) => provider.identityType = val,
                      isDense: true,
                      isExpanded: true,
                      decoration:
                          defaultInputDecoration(loc.register.step2_id_type),
                    ),
                  ),
                ),
                Container(width: 10),
                Expanded(
                  child: (provider.wni())
                      ? TextCencored(
                          title: loc.register.step2_id_number,
                          text: provider.identityNumberCtrl.text,
                        )
                      : TextFormField(
                          controller: provider.identityNumberCtrl,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.number,
                          readOnly: provider.wni(),
                          decoration: defaultInputDecoration(
                              loc.register.step2_id_number),
                        ),
                ),
              ],
            ),
            formInputSpacer,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: (provider.wni())
                      ? TextCencored(
                          title: loc.register.step2_place_of_birth,
                          text: provider.placeOfBirthCtrl.text,
                        )
                      : TextFormField(
                          controller: provider.placeOfBirthCtrl,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          readOnly: provider.wni(),
                          decoration: defaultInputDecoration(
                              loc.register.step2_place_of_birth),
                        ),
                ),
                Container(width: 10),
                Expanded(
                  child: (provider.wni())
                      ? TextCencored(
                          title: loc.register.step1_date_of_birth,
                          text: DateFormat('dd-MM-yyyy')
                              .format(provider.dateOfBirth),
                        )
                      : DateTimeField(
                          initialValue: provider.dateOfBirth,
                          validator: (val) {
                            if (val == null)
                              return loc.general.general_required_field;
                            return null;
                          },
                          resetIcon: null,
                          format: DateFormat('dd MMMM yyyy'),
                          readOnly: true,
                          decoration: defaultInputDecoration(
                              loc.register.step1_date_of_birth),
                          onShowPicker: (context, currentValue) {
                            return showDatePicker(
                              context: context,
                              firstDate: DateTime(1900),
                              initialDatePickerMode: DatePickerMode.year,
                              initialDate: currentValue ?? DateTime.now(),
                              lastDate: DateTime(2100),
                            );
                          },
                          onChanged: (val) => provider.dateOfBirth = val,
                        ),
                ),
              ],
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.expiryStatus,
              items: provider.expiryStatusItem,
              onChanged: (val) => provider.expiryStatus = val,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step2_expiry_status),
            ),
            formInputSpacer,
            Visibility(
              visible: provider.expiryStatus != 'Lifetime',
              child: DateTimeField(
                initialValue: provider.expiryDate,
                validator: (val) {
                  if (val == null) return loc.general.general_required_field;
                  return null;
                },
                resetIcon: null,
                format: DateFormat('dd MMMM yyyy'),
                readOnly: true,
                decoration:
                    defaultInputDecoration(loc.register.step2_expiry_date),
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                    context: context,
                    firstDate: DateTime(1900),
                    initialDatePickerMode: DatePickerMode.year,
                    initialDate: currentValue ?? DateTime.now(),
                    lastDate: DateTime(2100),
                  );
                },
                onChanged: (val) => provider.expiryDate = val,
              ),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.hasNpwp,
              items: provider.yesNoSpecifyItems,
              onChanged: (val) => provider.hasNpwp = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step2_has_npwp),
            ),
            Visibility(
              visible: provider.hasNpwp == 'Yes',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.npwpCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      if (val.length < 15)
                        return loc.general.general_length_field
                            .replaceAll('%s', '15');
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    maxLength: 15,
                    decoration: defaultInputDecoration(loc.register.step2_npwp),
                  ),
                ],
              ),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.educationBackground,
              items: provider.educationBackgroundItems,
              onChanged: (val) => provider.educationBackground = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step2_education),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.maritalStatus,
              items: provider.maritalStatusItems,
              onChanged: (val) => provider.maritalStatus = val,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step2_marital_status),
            ),
            Visibility(
              visible: provider.maritalStatus == 'Married',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.spouseCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.step2_spouse_name),
                  ),
                ],
              ),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.religion,
              items: provider.religionItems,
              onChanged: (val) => provider.religion = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step2_religion),
            ),
            formActionSpacer,
            SizedBox(
              width: double.infinity,
              child: MaterialButton(
                elevation: 0,
                highlightElevation: 0,
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () {
                  if (provider.step2FormKey.currentState.validate())
                    provider.nextPage();
                },
                child: Text(loc.register.general_next),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
