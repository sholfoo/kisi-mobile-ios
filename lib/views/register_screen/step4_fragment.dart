import 'package:flutter/material.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:kisi_flutter/views/register_screen/register_step.dart';
import 'package:kisi_flutter/widgets/phone_code_select.dart';
import 'package:provider/provider.dart';
import 'package:string_validator/string_validator.dart';

import 'form_utils.dart';

class Step4Fragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(context);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return RegisterStep(
      title: loc.register.general_form_title,
      subtitle: loc.register.step4_form_subtitle,
      description: loc.register.general_form_description,
      child: Form(
        key: provider.step4FormKey,
        child: Column(
          children: <Widget>[
            // Fields Here
            DropdownButtonFormField<String>(
              value: provider.occupation,
              items: provider.occupationItems,
              onChanged: (val) => provider.occupation = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step4_occupation),
            ),
            Visibility(
              visible: provider.occupation != 'House Wife' &&
                  provider.occupation != 'Student',
              child: Column(
                children: <Widget>[
                  Visibility(
                    visible: provider.occupation == 'Others',
                    child: Column(
                      children: <Widget>[
                        formInputSpacer,
                        TextFormField(
                          controller: provider.otherOccupationCtrl,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          decoration: defaultInputDecoration(
                              loc.register.step4_other_occupation),
                        ),
                      ],
                    ),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.companyNameCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.step4_company_name),
                  ),
                  formInputSpacer,
                  DropdownButtonFormField<String>(
                    value: provider.lineOfBusiness,
                    items: provider.businessList,
                    onChanged: (val) => provider.lineOfBusiness = val,
                    validator: (val) {
                      if (val == null)
                        return loc.general.general_required_field;
                      return null;
                    },
                    isDense: true,
                    isExpanded: true,
                    decoration: defaultInputDecoration(
                        loc.register.step4_line_of_business),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.positionCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.step4_position),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.officePhoneNumberCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.phone,
                    decoration: defaultInputDecoration(
                      loc.register.step4_office_phone,
                      prefix: PhoneCodeSelect(
                        currentValue: provider.officePhoneNumberCode,
                        items: provider.phoneCodeList,
                        onSelected: (val) =>
                            provider.officePhoneNumberCode = val,
                      ),
                    ),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.workFacsimileNumberCtrl,
                    keyboardType: TextInputType.phone,
                    decoration: defaultInputDecoration(
                      loc.register.step4_office_fax,
                      prefix: PhoneCodeSelect(
                        currentValue: provider.workFacsimileNumberCode,
                        items: provider.phoneCodeList,
                        onSelected: (val) =>
                            provider.workFacsimileNumberCode = val,
                      ),
                    ),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.workEmailCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      if (!isEmail(val)) return loc.general.general_email_field;
                      return null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    decoration:
                        defaultInputDecoration(loc.register.step4_work_email),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.workLengthCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    decoration:
                        defaultInputDecoration(loc.register.step4_work_length),
                  ),
                  formSectionSpacer,
                  TextFormField(
                    controller: provider.companyAddress,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.multiline,
                    minLines: 2,
                    maxLines: null,
                    decoration: defaultInputDecoration(
                      loc.register.step4_company_address,
                      alignLabelWithHint: true,
                    ),
                  ),
                  formInputSpacer,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          controller: provider.companyProvince,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          decoration: defaultInputDecoration(
                              loc.register.general_province),
                        ),
                      ),
                      Container(width: 10),
                      Expanded(
                        child: TextFormField(
                          controller: provider.companyCity,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.text,
                          decoration:
                              defaultInputDecoration(loc.register.general_city),
                        ),
                      ),
                    ],
                  ),
                  formInputSpacer,
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: DropdownButtonFormField<String>(
                          value: provider.companyCountry,
                          items: provider.countryList,
                          onChanged: (val) => provider.companyCountry = val,
                          validator: (val) {
                            if (val == null)
                              return loc.general.general_required_field;
                            return null;
                          },
                          isDense: true,
                          isExpanded: true,
                          decoration: defaultInputDecoration(
                              loc.register.general_country),
                        ),
                      ),
                      Container(width: 10),
                      Expanded(
                        child: TextFormField(
                          controller: provider.companyPostal,
                          validator: (val) {
                            if (val.trim().isEmpty)
                              return loc.general.general_required_field;
                            return null;
                          },
                          keyboardType: TextInputType.number,
                          decoration:
                              defaultInputDecoration(loc.register.general_zip),
                        ),
                      ),
                    ],
                  ),
                  formSectionSpacer,
                  DropdownButtonFormField<String>(
                    value: provider.annualIncome,
                    items: provider.annualIncomeItems,
                    onChanged: (val) => provider.annualIncome = val,
                    isDense: true,
                    isExpanded: true,
                    decoration: defaultInputDecoration(
                        loc.register.step4_annual_income),
                  ),
                ],
              ),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.primaryIncome,
              items: provider.primaryIncomeItems,
              onChanged: (val) => provider.primaryIncome = val,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step4_primary_source),
            ),
            Visibility(
              visible: provider.primaryIncome == 'Others',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.otherPrimaryIncomeCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.step4_other_source),
                  ),
                ],
              ),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.investmentFund,
              items: provider.investFundItems,
              onChanged: (val) => provider.investmentFund = val,
              isDense: true,
              isExpanded: true,
              decoration:
                  defaultInputDecoration(loc.register.step4_invest_fund),
            ),
            Visibility(
              visible: provider.investmentFund == 'Others',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.otherInvestmentFundCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                        defaultInputDecoration(loc.register.step4_other_fund),
                  ),
                ],
              ),
            ),
            // Batas Field
            formActionSpacer,
            SizedBox(
              width: double.infinity,
              child: MaterialButton(
                elevation: 0,
                highlightElevation: 0,
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () {
                  if (provider.step4FormKey.currentState.validate())
                    provider.nextPage();
                },
                child: Text(loc.register.general_next),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
