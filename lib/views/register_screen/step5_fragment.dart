import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:kisi_flutter/views/register_screen/register_step.dart';
import 'package:kisi_flutter/widgets/app_checkbox.dart';
import 'package:kisi_flutter/widgets/phone_code_select.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'form_utils.dart';
import 'package:kisi_flutter/widgets/custom_dialog.dart' as customDialog;

class Step5Fragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(context);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return RegisterStep(
      title: loc.register.general_form_title,
      subtitle: loc.register.step5_form_subtitle,
      description: loc.register.general_form_description,
      child: Form(
        key: provider.step5FormKey,
        child: Column(
          children: <Widget>[
            // ===
            // INFORMASI KELUARGA
            // ===
            Visibility(
              visible: provider.maritalStatus == 'Married',
              child: Column(
                children: <Widget>[
                  _SectionHeader(loc.register.step5_family_section),
                  TextFormField(
                    controller: provider.spouseCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                    defaultInputDecoration(loc.register.step5_spouse_name),
                  ),
                  formSectionSpacer,
                ],
              ),
            ),
            // ===
            // DATA AHLI WARIS
            // ===
            _SectionHeader(loc.register.step5_heir_section),
            TextFormField(
              controller: provider.heirNameCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.text,
              decoration: defaultInputDecoration(loc.register.step5_heir_name),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.heirRelationCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.text,
              decoration:
              defaultInputDecoration(loc.register.step5_heir_relation),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.heirPhoneNumberCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.phone,
              decoration: defaultInputDecoration(
                loc.register.step5_heir_phone_number,
                prefix: PhoneCodeSelect(
                  currentValue: provider.heirPhoneNumberCode,
                  items: provider.phoneCodeList,
                  onSelected: (val) => provider.heirPhoneNumberCode = val,
                ),
              ),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.heirMobilePhoneNumberCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.phone,
              decoration: defaultInputDecoration(
                loc.register.step5_heir_mobile_number,
                prefix: PhoneCodeSelect(
                  currentValue: provider.heirMobilePhoneNumberCode,
                  items: provider.phoneCodeList,
                  onSelected: (val) => provider.heirMobilePhoneNumberCode = val,
                ),
              ),
            ),
            formSectionSpacer,
            // ===
            // INFORMASI KONTRAK DARURAT
            // ===
            _SectionHeader(
              loc.register.step5_emergency_section,
              description: loc.register.step5_emergency_desc,
            ),
            TextFormField(
              controller: provider.emergencyNameCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.text,
              decoration: defaultInputDecoration(loc.register.step5_em_name),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.emergencyRelationCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.text,
              decoration:
              defaultInputDecoration(loc.register.step5_em_relation),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.emergencyPhoneNumberCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.phone,
              decoration: defaultInputDecoration(
                loc.register.step5_em_phone_number,
                prefix: PhoneCodeSelect(
                  currentValue: provider.emergencyPhoneNumberCode,
                  items: provider.phoneCodeList,
                  onSelected: (val) => provider.emergencyPhoneNumberCode = val,
                ),
              ),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.emergencyMobilePhoneNumberCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.phone,
              decoration: defaultInputDecoration(
                loc.register.step5_em_mobile_number,
                prefix: PhoneCodeSelect(
                  currentValue: provider.emergencyMobilePhoneNumberCode,
                  items: provider.phoneCodeList,
                  onSelected: (val) =>
                  provider.emergencyMobilePhoneNumberCode = val,
                ),
              ),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.emergencyAddress,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.multiline,
              minLines: 2,
              maxLines: null,
              decoration: defaultInputDecoration(
                loc.register.step5_em_address,
                alignLabelWithHint: true,
              ),
            ),
            formInputSpacer,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: TextFormField(
                    controller: provider.emergencyProvince,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                    defaultInputDecoration(loc.register.general_province),
                  ),
                ),
                Container(width: 10),
                Expanded(
                  child: TextFormField(
                    controller: provider.emergencyCity,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                    defaultInputDecoration(loc.register.general_city),
                  ),
                ),
              ],
            ),
            formInputSpacer,
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: DropdownButtonFormField<String>(
                    value: provider.emergencyCountry,
                    items: provider.countryList,
                    onChanged: (val) => provider.emergencyCountry = val,
                    validator: (val) {
                      if (val == null)
                        return loc.general.general_required_field;
                      return null;
                    },
                    isDense: true,
                    isExpanded: true,
                    decoration:
                    defaultInputDecoration(loc.register.general_country),
                  ),
                ),
                Container(width: 10),
                Expanded(
                  child: TextFormField(
                    controller: provider.emergencyPostal,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    decoration:
                    defaultInputDecoration(loc.register.general_zip),
                  ),
                ),
              ],
            ),
            // ===
            // INFORMASI TAMBAHAN
            // ===
            formSectionSpacer,
            _SectionHeader(loc.register.step5_additional_section),
            DropdownButtonFormField<String>(
              value: provider.correspondentAddress,
              items: provider.correspondentAddressItems,
              onChanged: (val) => provider.correspondentAddress = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(
                  loc.register.step5_correspondent_address),
            ),
            Visibility(
              visible: provider.correspondentAddress == 'Others Address',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.correspondentAddressSpecifyCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_other_address),
                  ),
                ],
              ),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.confirmationMode,
              items: provider.confirmationModeItems,
              onChanged: (val) => provider.confirmationMode = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step5_confirm),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.bankName,
              items: provider.bankList,
              onChanged: (val) => provider.bankName = val,
              validator: (val) {
                if (val == null) return loc.general.general_required_field;
                return null;
              },
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step5_bank_name),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.bankBranchCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.text,
              decoration: defaultInputDecoration(loc.register.step5_branch),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.bankHolderNameCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.text,
              decoration: defaultInputDecoration(loc.register.step5_holder),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.bankTypeCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.text,
              decoration:
              defaultInputDecoration(loc.register.step5_account_type),
            ),
            formInputSpacer,
            TextFormField(
              controller: provider.bankAccountCtrl,
              validator: (val) {
                if (val.trim().isEmpty)
                  return loc.general.general_required_field;
                return null;
              },
              keyboardType: TextInputType.number,
              decoration:
              defaultInputDecoration(loc.register.step5_account_number),
            ),
            // ===
            // PROFIL RESIKO & LATAR BELAKANG INVESTASI
            // ===
            formSectionSpacer,
            _SectionHeader(loc.register.step5_invest_profile_section),
            DropdownButtonFormField<String>(
              value: provider.riskTolerance,
              items: provider.riskToleranceItems,
              onChanged: (val) => provider.riskTolerance = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step5_tolerance),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.timeHorizon,
              items: provider.timeHorizonItems,
              onChanged: (val) => provider.timeHorizon = val,
              isDense: true,
              isExpanded: true,
              decoration:
              defaultInputDecoration(loc.register.step5_time_horizon),
            ),
            formInputSpacer,
            DropdownButtonFormField<String>(
              value: provider.investObjective,
              items: provider.investObjectiveItems,
              onChanged: (val) => provider.investObjective = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.step5_invest_obj),
            ),
            // ADD INFO 1
            formInputSpacerLabel(loc.register.step5_info1),
            DropdownButtonFormField<String>(
              value: provider.addInfo1,
              items: provider.yesNoItems,
              onChanged: (val) => provider.addInfo1 = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.general_answer),
            ),
            Visibility(
              visible: provider.addInfo1 == 'Yes',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo1NameCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                    defaultInputDecoration(loc.register.step5_info1_name),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo1CompanyCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_info1_company_name),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo1PositionCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_info1_position),
                  ),
                ],
              ),
            ),
            // ADD INFO 2
            formInputSpacerLabel(loc.register.step5_info2),
            DropdownButtonFormField<String>(
              value: provider.addInfo2,
              items: provider.yesNoItems,
              onChanged: (val) => provider.addInfo2 = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.general_answer),
            ),
            Visibility(
              visible: provider.addInfo2 == 'Yes',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo2NameCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                    defaultInputDecoration(loc.register.step5_info2_name),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo2CompanyCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_info2_company_name),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo2PositionCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_info2_position),
                  ),
                ],
              ),
            ),
            // ADD INFO 3
            formInputSpacerLabel(loc.register.step5_info3),
            DropdownButtonFormField<String>(
              value: provider.addInfo3,
              items: provider.yesNoItems,
              onChanged: (val) => provider.addInfo3 = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.general_answer),
            ),
            Visibility(
              visible: provider.addInfo3 == 'Yes',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo3ShareCompanyCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_info3_company_name),
                  ),
                ],
              ),
            ),
            // ADD INFO 4
            formInputSpacerLabel(loc.register.step5_info4),
            DropdownButtonFormField<String>(
              value: provider.addInfo4,
              items: provider.yesNoItems,
              onChanged: (val) => provider.addInfo4 = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.general_answer),
            ),
            Visibility(
              visible: provider.addInfo4 == 'Yes',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo4CompanyNameCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_info4_company_name),
                  ),
                ],
              ),
            ),
            // ADD INFO 5
            formInputSpacerLabel(loc.register.step5_info5),
            DropdownButtonFormField<String>(
              value: provider.addInfo5,
              items: provider.yesNoItems,
              onChanged: (val) => provider.addInfo5 = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.general_answer),
            ),
            Visibility(
              visible: provider.addInfo5 == 'Yes',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo5PositionCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_info5_position),
                  ),
                ],
              ),
            ),
            // ADD INFO 6
            formInputSpacerLabel(loc.register.step5_info6),
            DropdownButtonFormField<String>(
              value: provider.addInfo6,
              items: provider.yesNoItems,
              onChanged: (val) => provider.addInfo6 = val,
              isDense: true,
              isExpanded: true,
              decoration: defaultInputDecoration(loc.register.general_answer),
            ),
            Visibility(
              visible: provider.addInfo6 == 'Yes',
              child: Column(
                children: <Widget>[
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo6NameCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration:
                    defaultInputDecoration(loc.register.step5_info6_name),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo6RelationCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_info6_relation),
                  ),
                  formInputSpacer,
                  TextFormField(
                    controller: provider.addInfo6PositionCtrl,
                    validator: (val) {
                      if (val.trim().isEmpty)
                        return loc.general.general_required_field;
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: defaultInputDecoration(
                        loc.register.step5_info6_position),
                  ),
                ],
              ),
            ),
            // Fields Here
            formActionSpacer,
            SizedBox(
              width: double.infinity,
              child: MaterialButton(
                elevation: 0,
                highlightElevation: 0,
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () async {
                  if (provider.step5FormKey.currentState.validate()) {
                    var result = await provider.validateBca(context);
                    if (result) {
                      var lang = await showDialog<Locale>(
                          context: context,
                          builder: (ctx) => ShowDialogValidationBCA(provider)
                      );
                    }
                  }
                },
                child: Text(loc.register.general_next),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _SectionHeader extends StatelessWidget {
  final String title;
  final String description;

  _SectionHeader(this.title, {this.description});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        Divider(),
        (description == null)
            ? Container()
            : Column(
          children: <Widget>[
            Text(
              description,
              textAlign: TextAlign.justify,
            ),
            formInputSpacer,
          ],
        )
      ],
    );
  }
}

class ShowDialogValidationBCA extends StatefulWidget {
  var provider;
  ShowDialogValidationBCA(this.provider);

  @override
  _ShowDialogValidationBCAState createState() => _ShowDialogValidationBCAState();
}

class _ShowDialogValidationBCAState extends State<ShowDialogValidationBCA> {

  var _checkedOne = false;
  var _checkedTwo = false;
  var _checkedThree = false;
  get checkedOne => _checkedOne;
  get checkedTwo => _checkedTwo;
  get checkedThree => _checkedThree;

  set checkedOne(value) {
    _checkedOne = value;
  }
  set checkedTwo(value) {
    _checkedTwo = value;
  }
  set checkedThree(value) {
    _checkedThree = value;
  }

  WebViewController _controller;
  _loadHtmlFromAssets() async {
    String fileText = await rootBundle.loadString(
      'assets/reading/disclaimer.html',
    );
    _controller.loadUrl(
      Uri.dataFromString(
        fileText,
        mimeType: 'text/html',
        encoding: Encoding.getByName('utf-8'),
      ).toString(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return customDialog.AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius:
            BorderRadius.all(
                Radius.circular(16.0))),
        content : Container(
            child: SizedBox(
              width: double.infinity,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: WebView(
                      javascriptMode: JavascriptMode.unrestricted,
                      onWebViewCreated: (WebViewController webViewController) {
                        _controller = webViewController;
                        _loadHtmlFromAssets();
                      },
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  AppCheckbox(
                    value: checkedOne,
                    label: "Saya dengan ini memberikan persetujuan kepada PT. Korea Investment and Sekuritas Indonesia untuk meminta konfirmasi atas data rekening saya tersebut di atas kepada PT Bank Central Asia Tbk",
                    labelStyle: TextStyle(color: Colors.black45, fontSize: 14),
                    checkColor: Colors.blue,
                    activeColor: Colors.white,
                    onChanged: (val) {
                      setState(() {
                        checkedOne = val;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  AppCheckbox(
                    value: checkedTwo,
                    label: "Saya dengan ini menyatakan telah membaca, memahami, dan menyetujui isi Pernyataan tersebut di atas",
                    labelStyle: TextStyle(color: Colors.black45, fontSize: 14),
                    checkColor: Colors.blue,
                    activeColor: Colors.white,
                    onChanged: (bool val) {
                      setState(() {
                        checkedTwo = val;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  AppCheckbox(
                    value: checkedThree,
                    label: "Saya dengan ini menyatakan telah membaca, memahami, dan menyetujui isi dari ketentuan terkait dengan Rekening Dana Nasabah di BCA sebagaimana tersebut di atas",
                    labelStyle: TextStyle(color: Colors.black45, fontSize: 14),
                    checkColor: Colors.blue,
                    activeColor: Colors.white,
                    onChanged: (val) {
                      setState(() {
                        checkedThree = val;
                      });
                    },
                  ),
                  Padding(padding: EdgeInsets.only(top: 16)),
                  SizedBox(
                    width: double.infinity,
                    child: FlatButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32.0)),
                      color: Theme.of(context).primaryColor,
                      textColor: Colors.white,
                      padding: EdgeInsets.only(
                          left: 32.0,
                          top: 12.0,
                          right: 32.0,
                          bottom: 12.0),
                      onPressed: () async {
                        if (!checkedOne || !checkedTwo || !checkedThree){
                          Fluttertoast.showToast(
                              msg: "Anda belum menyetujui disclaimer dan pernyataan",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                        } else {
                          Navigator.pop(context);
                          widget.provider.nextPage();
                        }
                      },
                      child: Text(
                        "OK".toUpperCase(),
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
        )
    );
  }
}


