import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  var model = RegisterScreenModel();

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    model.fetchSelectionItems(context);

    return ChangeNotifierProvider.value(
      value: model,
      child: _Content(),
    );
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(
      context,
      listen: false,
    );

    return WillPopScope(
      onWillPop: () async {
        if (provider.currentPage > 0) {
          provider.prevPage();
          return false;
        } else
          return true;
      },
      child: Scaffold(
        appBar: AppBar(
          title: Image.asset(
            'assets/logo-kisi-long.png',
            height: 28,
          ),
          automaticallyImplyLeading: true,
          centerTitle: false,
          actions: <Widget>[
            Consumer<RegisterScreenModel>(
              builder: (_, model, __) {
                return Visibility(
                  visible: (provider.currentPage + 1) <= 7,
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Center(
                      child: Text(
                        '${provider.currentPage + 1} of ${provider.pages.length - 2}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
        body: Consumer<RegisterScreenModel>(
          builder: (_, model, __) {
            return SimpleViewState(
              state: provider.state,
              message: provider.message,
              onRetry: () => provider.setup(),
              child: (ctx) => model.activePage,
            );
          },
        ),
      ),
    );
  }
}
