import 'package:flutter/material.dart';

InputDecoration defaultInputDecoration(
  String title, {
  bool alignLabelWithHint = false,
  Widget prefix,
}) {
  return InputDecoration(
    border: OutlineInputBorder(),
    // contentPadding: EdgeInsets.symmetric(
    //   vertical: 10,
    //   horizontal: 12,
    // ),
    alignLabelWithHint: alignLabelWithHint,
    labelText: title,
    prefixIcon: prefix != null ? prefix : null,
    isDense: true,
  );
}

var formInputSpacer = Container(height: 10);

var formSectionSpacer = Container(height: 24);

var formActionSpacer = Container(height: 32);

formInputSpacerLabel(String label) => Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(height: 10),
        Text(
          label,
          textAlign: TextAlign.justify,
        ),
        Container(height: 4),
      ],
    );

formSectionSpacerLabel(String label) => Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(height: 24),
        Text(
          label,
          textAlign: TextAlign.justify,
        ),
        Container(height: 4),
      ],
    );
