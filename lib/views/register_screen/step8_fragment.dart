import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/dtos/submit_questionnaire_dto.dart';
import 'package:kisi_flutter/dtos/submit_register_dto.dart';
import 'package:kisi_flutter/dtos/submit_schedule_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/choose_schedule_screen.dart';
import 'package:kisi_flutter/views/register_screen/register_complete_screen.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class Step8Fragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(
      context,
      listen: false,
    );

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          children: <Widget>[
            Text(
              loc.register.step8_title,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Text(
                loc.register.step8_text_line1,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            Container(height: 25),
            Padding(
              padding: const EdgeInsets.all(16),
              child: Text(
                loc.register.step8_text_line2,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            Consumer<RegisterScreenModel>(
              builder: (_, model, __) {
                return model.schedule != null
                    ? Column(
                        children: <Widget>[
                          Text(
                            DateFormat('dd MMMM yyyy\nHH:mm')
                                .format(model.schedule),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(height: 16),
                        ],
                      )
                    : Container();
              },
            ),
            OutlineButton(
              onPressed: () async {
                var result = await Navigator.push<DateTime>(
                  context,
                  MaterialPageRoute(
                    builder: (ctx) => ChooseScheduleScreen(),
                  ),
                );
                if (result != null) provider.schedule = result;
              },
              child: Text(loc.register.step8_choose),
            ),
            Expanded(child: Container()),
            SizedBox(
              width: double.infinity,
              child: MaterialButton(
                elevation: 0,
                highlightElevation: 0,
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () async {
                  // ====================================================
                  // MODIFIED HERE TO COUNTER IF SHOW OR HIDE QUESTIONERE
                  // ====================================================
                  if (provider.schedule != null) {
                    if (provider.isShowQuestionare) {
                      provider.nextPage();
                    } else {
                      progressDialog(context);
                      try {
                        Response response;

                        // Submit register if not submitted
                        if (!provider.registerSubmitted) {
                          response = await api
                              .submitRegister(provider.buildRegisterModel());
                          print(response.data);
                          provider.registerResult =
                              SubmitRegisterDto.fromJson(response.data);

                          if (!provider.registerResult.success) {
                            Navigator.pop(context);
                            error(context,
                                message: provider.registerResult.message);
                            return;
                          }

                          provider.registerSubmitted = true;
                        }

                        // Submit schedule if not submitted
                        if (!provider.scheduleSubmitted) {
                          response = await api.submitSchedule(
                            userId: provider.registerResult.data.userId,
                            schedule: provider.schedule,
                          );
                          print(response.data);
                          var scheduleResult =
                          SubmitScheduleDto.fromJson(response.data);
                          if (!scheduleResult.success) {
                            Navigator.pop(context);
                            error(context, message: scheduleResult.message);
                            return;
                          }

                          provider.scheduleSubmitted = true;
                        }

                        Navigator.pop(context);
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (ctx) => RegisterCompleteScreen(),
                          ),
                              (_) => false,
                        );
                      } catch (e) {
                        Navigator.pop(context);
                        error(context, message: api.parseError(e));
                      }
                    }
                  }
                },
                child: Text(loc.register.general_next),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
