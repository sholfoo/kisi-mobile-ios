import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/classes/utils.dart';
import 'package:kisi_flutter/dtos/questionary_dto.dart';
import 'package:kisi_flutter/dtos/submit_questionnaire_dto.dart';
import 'package:kisi_flutter/dtos/submit_register_dto.dart';
import 'package:kisi_flutter/dtos/submit_schedule_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/register_complete_screen.dart';
import 'package:kisi_flutter/views/register_screen/register_screen_model.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;
import 'package:string_validator/string_validator.dart';

import 'form_utils.dart';

class Step9Fragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RegisterScreenModel>(context);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    var locale = Localizations.localeOf(context);
    var lang = locale.languageCode.toLowerCase();

    return SingleChildScrollView(
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ...map<Widget>(provider.questionList,
                  (index, QuestionaryDataDto item) {
                return Column(
                  children: <Widget>[
                    formInputSpacerLabel(
                      lang == 'id' ? item.question : item.questionEn,
                    ),
                    DropdownButtonFormField<String>(
                      value: provider.questionAnswerList[index],
                      items: (item)
                          .answers
                          .map(
                            (x) => DropdownMenuItem<String>(
                              value: x.id.toString(),
                              child: Text(x.answer),
                            ),
                          )
                          .toList(),
                      onChanged: (val) =>
                          provider.updateQuestionAnswer(index, val),
                      isDense: true,
                      isExpanded: true,
                      decoration:
                          defaultInputDecoration(loc.register.general_answer),
                    ),
                  ],
                );
              }),
              formActionSpacer,
              SizedBox(
                width: double.infinity,
                child: MaterialButton(
                  elevation: 0,
                  highlightElevation: 0,
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  onPressed: () async {
                    if (!provider.questionAnswerList.any((x) => x == null)) {
                      progressDialog(context);
                      try {
                        Response response;

                        // Submit register if not submitted
                        if (!provider.registerSubmitted) {
                          response = await api
                              .submitRegister(provider.buildRegisterModel());
                          print(response.data);
                          provider.registerResult =
                              SubmitRegisterDto.fromJson(response.data);

                          if (!provider.registerResult.success) {
                            Navigator.pop(context);
                            error(context,
                                message: provider.registerResult.message);
                            return;
                          }

                          provider.registerSubmitted = true;
                        }

                        // Submit schedule if not submitted
                        if (!provider.scheduleSubmitted) {
                          response = await api.submitSchedule(
                            userId: provider.registerResult.data.userId,
                            schedule: provider.schedule,
                          );
                          print(response.data);
                          var scheduleResult =
                              SubmitScheduleDto.fromJson(response.data);
                          if (!scheduleResult.success) {
                            Navigator.pop(context);
                            error(context, message: scheduleResult.message);
                            return;
                          }

                          provider.scheduleSubmitted = true;
                        }

                        // Submit questionary if not submitted
                        if (!provider.questionarySubmitted) {
                          response = await api.submitQuestionnaire(
                            userId: provider.registerResult.data.userId,
                            questionIds: provider.questionList
                                .map<int>((x) => x.id)
                                .toList(),
                            answerIds: provider.questionAnswerList
                                .map<int>((x) => toInt(x))
                                .toList(),
                          );
                          print(response.data);
                          var questionnaireResult =
                              SubmitQuestionnaireDto.fromJson(response.data);
                          if (!questionnaireResult.success) {
                            Navigator.pop(context);
                            error(context,
                                message: questionnaireResult.message);
                            return;
                          }

                          provider.questionarySubmitted = true;
                        } else {
                          Navigator.pop(context);
                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (ctx) => RegisterCompleteScreen(),
                            ),
                                (_) => false,
                          );
                        }

                        Navigator.pop(context);
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (ctx) => RegisterCompleteScreen(),
                          ),
                          (_) => false,
                        );
                      } catch (e) {
                        Navigator.pop(context);
                        error(context, message: api.parseError(e));
                      }
                    }
                  },
                  child: Text(loc.register.general_next),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
