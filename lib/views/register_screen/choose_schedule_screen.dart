import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/dtos/schedule_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class ChooseScheduleScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: Container(child: _Content()),
    );
  }
}

class _ViewModel with ChangeNotifier {
  List<ScheduleDto> data;
  var state = ScreenState.content;
  var message = '';

  _ViewModel() {
    fetch();
  }

  Future fetch() async {
    state = ScreenState.progress;
    notifyListeners();

    try {
      var response = await api.schedules();
      data = (response.data as List)
          .map<ScheduleDto>((x) => ScheduleDto.fromJson(x))
          .toList();

      if (data.isEmpty)
        state = ScreenState.empty;
      else
        state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);
      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  DateTime _combine(DateTime date, String time) {
    var dateFormat = DateFormat('yyyy-MM-dd');
    return DateTime.parse(dateFormat.format(date) + ' ' + time);
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);

    var dateFormat = DateFormat('dd MMM yyyy');

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      appBar: AppBar(
        title: Text(loc.register.step8_choose_title),
      ),
      body: SimpleViewState(
        state: provider.state,
        message: provider.message,
        onRetry: () => provider.fetch(),
        child: (ctx) => SingleChildScrollView(
          child: SafeArea(
            child: Column(
              children: provider.data
                  .map(
                    (date) => Column(
                      children: <Widget>[
                        Container(
                          width: double.infinity,
                          padding: EdgeInsets.all(8),
                          color: Theme.of(context).primaryColor,
                          child: Text(
                            dateFormat.format(date.date),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(16),
                          child: Wrap(
                            spacing: 10,
                            runSpacing: 10,
                            children: date.time
                                .map(
                                  (time) => _TimeItem(
                                    onPressed: time.status == 'available'
                                        ? () {
                                            Navigator.pop(
                                              context,
                                              _combine(
                                                date.date,
                                                time.time,
                                              ),
                                            );
                                          }
                                        : null,
                                    time: time,
                                  ),
                                )
                                .toList(),
                          ),
                        ),
                      ],
                    ),
                  )
                  .toList(),
            ),
          ),
        ),
      ),
    );
  }
}

class _TimeItem extends StatelessWidget {
  final Function onPressed;
  final ScheduleTimeDto time;

  const _TimeItem({
    this.onPressed,
    this.time,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: onPressed == null ? Colors.grey.shade300 : Colors.transparent,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(
            width: 1,
            color: Colors.grey,
          ),
        ),
        child: Text(time.time),
      ),
    );
  }
}
