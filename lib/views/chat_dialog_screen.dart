import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:kisi_flutter/dtos/MemberDto.dart';
import 'package:kisi_flutter/provider/auth_provider.dart';
import 'package:kisi_flutter/provider/quickblox/qb_channel.dart';
import 'package:kisi_flutter/provider/quickblox/qb_chat_dialog.dart';
import 'package:kisi_flutter/views/chat_screen.dart';
import 'package:kisi_flutter/views/contact_screen.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class ChatDialogScreen extends StatelessWidget {
  final _model = _ViewModel();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _model,
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var dialogs = List<QbChatDialog>();
  var state = ScreenState.content;
  var message = '';

  _ViewModel() {
    QbChannel.addOnMessageReceivedListener((msg) {
      fetch(false);
    });

    fetch();
  }
  Future fetch([bool showLoading = true]) async {
    if (showLoading) {
      state = ScreenState.progress;
      notifyListeners();
    }

    try {
      var qbResponse = await QbChannel.getChatDialogs();
      dialogs = (qbResponse as List)
          .map((x) => QbChatDialog.fromJson(HashMap<String, dynamic>.from(x)))
          .toList();

      if (dialogs.isEmpty)
        state = ScreenState.empty;
      else
        state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);
      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);
    var loginProvider = Provider.of<AuthProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Chat'),
      ),
      body: SimpleViewState(
        state: provider.state,
        message: provider.message,
        onRetry: () => provider.fetch(),
        child: (ctx) => ListView.separated(
          itemCount: provider.dialogs.length,
          itemBuilder: (ctx, i) {
            var dialog = provider.dialogs[i];
            return ListTile(
              onTap: () async {
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (ctx) => ChatScreen(
                      opponent: MemberDto(
                        id: dialog.recipientId,
                        qbId: dialog.recipientId,
                        name: dialog.name,
                      ),
                    ),
                  ),
                );

                QbChannel.removeListeners();
                QbChannel.addOnMessageReceivedListener((msg) {
                  provider.fetch(false);
                });
                provider.fetch(false);
              },
              dense: true,
              title: Text(dialog.name),
              subtitle: Text(
                '${loginProvider.user.qbId == dialog.lastMessageUserId ? 'me: ' : ''}${dialog.lastMessage}',
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
              ),
              trailing: Visibility(
                visible: dialog.unreadMessageCount > 0,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 8,
                    vertical: 2,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Text(
                    dialog.unreadMessageCount.toString(),
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            );
          },
          separatorBuilder: (ctx, i) {
            return Container(
              color: Colors.grey.shade300,
              height: 0.5,
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var result = await Navigator.push<MemberDto>(
            context,
            MaterialPageRoute(
              builder: (ctx) => ContactScreen(),
            ),
          );

          if (result != null) {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (ctx) => ChatScreen(
                  opponent: result,
                ),
              ),
            );

            QbChannel.removeListeners();
            QbChannel.addOnMessageReceivedListener((msg) {
              provider.fetch(false);
            });
            provider.fetch(false);
          }
        },
        child: Icon(Icons.chat),
      ),
    );
  }
}
