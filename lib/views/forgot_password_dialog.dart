import 'package:flutter/material.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/dtos/action_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/form_utils.dart';
import 'package:kisi_flutter/services/api.dart' as api;
import 'package:string_validator/string_validator.dart';

class ForgotPasswordDialog extends StatefulWidget {
  @override
  _ForgotPasswordDialogState createState() => _ForgotPasswordDialogState();
}

class _ForgotPasswordDialogState extends State<ForgotPasswordDialog> {
  var _formKey = GlobalKey<FormState>();

  var _emailCtrl = TextEditingController();

  var loading = false;

  @override
  Widget build(BuildContext context) {
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return AlertDialog(
      title: Text(
        loc.login.forgot_password_title,
        textAlign: TextAlign.center,
      ),
      contentPadding: EdgeInsets.all(0),
      content: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: _emailCtrl,
                  validator: (val) {
                    if (val.isEmpty) return loc.general.general_required_field;
                    if (!isEmail(val)) return loc.general.general_email_field;
                    return null;
                  },
                  keyboardType: TextInputType.text,
                  decoration:
                      defaultInputDecoration(loc.login.forgot_password_email),
                ),
              ],
            ),
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: loading ? null : () => Navigator.pop(context),
          child: Text(loc.general.dialog_act_cancel),
        ),
        FlatButton(
          onPressed: loading
              ? null
              : () async {
                  if (_formKey.currentState.validate()) {
                    try {
                      progressDialog(context);
                      var response = await api.forgotPassword(
                        email: _emailCtrl.text,
                      );
                      Navigator.pop(context);

                      var result = ActionDto.fromJson(response.data);

                      if (result.success) {
                        // Close dialog
                        Navigator.pop(context);
                        shortToast(message: result.message);
                      } else
                        error(context, message: result.message);
                    } catch (e) {
                      Navigator.pop(context);
                      error(context, message: api.parseError(e));
                    }
                  }
                },
          child: Text(loc.login.forgot_password_send),
        )
      ],
    );
  }
}
