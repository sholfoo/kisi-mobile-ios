import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/login_choose.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PrivacyPolicyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var data = '';

  _ViewModel() {
    fetch();
  }

  void fetch() async {
    data = await rootBundle.loadString('assets/reading/privacy-policy.html');
    notifyListeners();
  }
}

class _Content extends StatelessWidget {
  WebViewController _controller;

  _loadHtmlFromAssets() async {
    String fileText = await rootBundle.loadString(
      'assets/reading/privacy-policy.html',
    );
    _controller.loadUrl(
      Uri.dataFromString(
        fileText,
        mimeType: 'text/html',
        encoding: Encoding.getByName('utf-8'),
      ).toString(),
    );
  }

  @override
  Widget build(BuildContext context) {
    // final provider = Provider.of<_ViewModel>(context);
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: <Widget>[
              Expanded(
                child: WebView(
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (WebViewController webViewController) {
                    _controller = webViewController;
                    _loadHtmlFromAssets();
                  },
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: MaterialButton(
                  color: Color.fromARGB(255, 156, 50, 41),
                  textColor: Colors.white,
                  child: Text(loc.permission.btn_agree),
                  onPressed: () async {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (ctx) => LoginChooseScreen(),
                      ),
                    );
                  },
                ),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (ctx) => LoginChooseScreen(),
                    ),
                  );
                },
                child: Text(loc.permission.btn_skip),
                textColor: Colors.grey.shade300,
              )
            ],
          ),
        ),
      ),
    );
  }
}
