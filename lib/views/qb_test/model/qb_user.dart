class QbUser {
  final int id;
  final String name;
  final String email;
  final bool isAdmin;

  QbUser({
    this.id,
    this.name,
    this.email,
    this.isAdmin,
  });
}
