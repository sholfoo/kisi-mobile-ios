import 'package:flutter/material.dart';
import 'package:kisi_flutter/views/qb_test/model/qb_user.dart';

class QbLoginViewModel with ChangeNotifier {
  QbUser selectedUser;

  bool get isLogged => selectedUser != null;
  bool get isAdmin => isLogged && selectedUser.isAdmin;

  QbLoginViewModel();
}
