import 'package:flutter/material.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/provider/quickblox/qb_channel.dart';
import 'package:kisi_flutter/views/qb_test/qb_dialogs_screen.dart';
import 'package:kisi_flutter/views/qb_test/qb_users_screen.dart';
import 'package:provider/provider.dart';

import 'model/qb_login_view_model.dart';
import 'model/qb_user.dart';

class QbLoginScreen extends StatelessWidget {
  final _model = _ViewModel();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _model,
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {
  _ViewModel();
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<QbLoginViewModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('QB Login Screen'),
      ),
      body: Center(
        child: OutlineButton(
          onPressed: () async {
            var result = await Navigator.push<QbUser>(
              context,
              MaterialPageRoute(builder: (ctx) => QbUsersScreen(false)),
            );

            if (result != null) {
              progressDialog(context);
              provider.selectedUser = result;
              try {
                await QbChannel.disconnect();
              } catch (e) {
                print(e.toString());
              }

              await QbChannel.connect(
                login: result.name.toLowerCase(),
                password: '12345678',
              );
              Navigator.pop(context);
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (ctx) => QbDialogsScreen()),
              );
            }
          },
          child: Text('LOGIN'),
        ),
      ),
    );
  }
}
