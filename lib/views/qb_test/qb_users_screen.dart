import 'package:flutter/material.dart';
import 'package:kisi_flutter/provider/quickblox/qb_channel.dart';
import 'package:kisi_flutter/views/qb_test/model/qb_login_view_model.dart';
import 'package:kisi_flutter/views/qb_test/model/qb_user.dart';
import 'package:provider/provider.dart';

class QbUsersScreen extends StatelessWidget {
  final _model = _ViewModel();

  final bool showAction;

  QbUsersScreen(this.showAction);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _model,
      child: _Content(showAction),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var users = List<QbUser>();

  _ViewModel() {
    fetch();
  }

  fetch() {
    users.clear();
    users.add(QbUser(
      id: 99726101,
      name: 'Admin1',
      email: 'admin1@app.id',
      isAdmin: true,
    ));
    users.add(QbUser(
      id: 99726114,
      name: 'Admin2',
      email: 'admin2@app.id',
      isAdmin: true,
    ));
    users.add(QbUser(
      id: 98812078,
      name: 'Gunawan',
      email: 'gunawan@app.id',
      isAdmin: false,
    ));
    users.add(QbUser(
      id: 98812092,
      name: 'Client',
      email: 'client@app.id',
      isAdmin: false,
    ));
    users.add(QbUser(
      id: 98867460,
      name: 'Riad',
      email: 'riad@app.id',
      isAdmin: false,
    ));
    users.add(QbUser(
      id: 98867578,
      name: 'Rudi',
      email: 'rudi@app.id',
      isAdmin: false,
    ));
    users.add(QbUser(
      id: 98995875,
      name: 'Lucy',
      email: 'lucy@app.id',
      isAdmin: false,
    ));
    users.add(QbUser(
      id: 98995894,
      name: 'Tika',
      email: 'tika@app.id',
      isAdmin: false,
    ));

    notifyListeners();
  }
}

class _Content extends StatelessWidget {
  final bool showAction;

  _Content(this.showAction);

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);
    var loginProvider = Provider.of<QbLoginViewModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('QB Pick User'),
      ),
      body: ListView.separated(
        itemCount: provider.users.length,
        itemBuilder: (ctx, i) {
          var item = provider.users[i];
          return ListTile(
            onTap: () {
              Navigator.pop(context, item);
            },
            dense: true,
            title: Text(item.name),
            subtitle: Text(
              '${item.isAdmin ? 'ADMIN' : 'MEMBER'} - ${item.email}',
            ),
            trailing: Visibility(
              visible: loginProvider.isAdmin && showAction,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  IconButton(
                    onPressed: () => QbChannel.startCall(
                      item.id,
                      ConferenceType.video,
                    ),
                    color: Colors.blue,
                    icon: Icon(Icons.video_call),
                  ),
                  IconButton(
                    onPressed: () => QbChannel.startCall(
                      item.id,
                      ConferenceType.audio,
                    ),
                    color: Colors.blue,
                    icon: Icon(Icons.call),
                  )
                ],
              ),
            ),
          );
        },
        separatorBuilder: (ctx, i) {
          return Container(
            color: Colors.grey.shade300,
            height: 0.5,
          );
        },
      ),
    );
  }
}
