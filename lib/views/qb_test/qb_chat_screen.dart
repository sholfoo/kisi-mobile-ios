import 'dart:collection';
import "package:collection/collection.dart";

import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/provider/quickblox/qb_channel.dart';
import 'package:kisi_flutter/provider/quickblox/qb_chat_message.dart';
import 'package:kisi_flutter/views/qb_test/model/qb_user.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class QbChatScreen extends StatefulWidget {
  final QbUser opponent;

  const QbChatScreen({
    this.opponent,
  });

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<QbChatScreen> {
  var _model = _ViewModel();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _model,
      child: _Content(
        opponent: widget.opponent,
      ),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var textController = TextEditingController();
  var showSendButton = false;

  var _opponentId = 0;
  List<QbChatMessage> messages = List();

  _ViewModel() {
    QbChannel.addOnMessageReceivedListener((msg) {
      messages.add(msg);
      notifyListeners();
    });

    // Setup text message listener
    textController.addListener(() {
      if (textController.text.isEmpty) {
        if (showSendButton) {
          showSendButton = false;
          notifyListeners();
        }
      } else {
        if (!showSendButton) {
          showSendButton = true;
          notifyListeners();
        }
      }
    });
  }

  fetch() async {
    var response = await QbChannel.qbGetChatDialogHistories(_opponentId);
    messages = (response as List)
        .map((x) => QbChatMessage.fromJson(HashMap<String, dynamic>.from(x)))
        .toList();
    print((response as List));
    notifyListeners();
  }

  setOpponetId(int id) {
    if (_opponentId != id) {
      _opponentId = id;
      fetch();
    }
  }

  sendMessage(QbChatMessage message) {
    messages.add(message);
    notifyListeners();
  }
}

class _Content extends StatelessWidget {
  final QbUser opponent;

  const _Content({
    this.opponent,
  });

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);
    provider.setOpponetId(opponent.id);

    var grouped = groupBy<QbChatMessage, String>(
      provider.messages,
      (QbChatMessage x) => x.dateSentString,
    );
    var widgets = [];
    grouped.forEach((group, items) {
      var dateNow = DateFormat('dd MMMM').format(DateTime.now());
      var dateChat = DateFormat('dd MMMM').format(DateTime.parse(group));

      widgets.add(Column(
        children: <Widget>[
          Bubble(
            alignment: Alignment.center,
            color: Color.fromRGBO(212, 234, 244, 1.0),
            margin: BubbleEdges.all(10),
            stick: true,
            child: Text(
              dateNow == dateChat ? 'Today' : dateChat,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.0),
            ),
          ),
          ...items.map(
            (x) => ChatBubble(
              message: x,
              opponentId: opponent.id,
            ),
          )
        ],
      ));
    });

    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        title: Text(opponent.name),
        actions: <Widget>[
          PopupMenuButton<int>(
            onSelected: (selected) {
              if (selected == 1)
                confirm(
                  context,
                  message: 'Apakah Anda ingin menghapus history chat ini?',
                  onOk: () async {
                    try {
                      // Dialog
                      Navigator.pop(context);

                      progressDialog(context);
                      await QbChannel.deleteChatDialog(opponent.id);
                      // Progress Dialog
                      Navigator.pop(context);

                      // Page
                      Navigator.pop(context);
                    } catch (e) {
                      Navigator.pop(context);
                      error(context, message: api.parseError(e));
                    }
                  },
                );
            },
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem<int>(
                  value: 1,
                  child: Text('DELETE CHAT'),
                )
              ];
            },
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              reverse: true,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  verticalDirection: VerticalDirection.down,
                  children: <Widget>[
                    ...widgets,
                  ],
                ),
              ),
            ),
          ),
          Material(
            color: Colors.white,
            child: Container(
              padding: EdgeInsets.all(8),
              child: SafeArea(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        controller: provider.textController,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                          hintText: 'Type message...',
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.all(12),
                        ),
                      ),
                    ),
                    Visibility(
                      visible: provider.showSendButton,
                      child: IconButton(
                        onPressed: () async {
                          var response = await QbChannel.sendDialogMessage(
                            opponent.id,
                            provider.textController.text,
                          );
                          provider.textController.text = '';

                          var result = QbChatMessage.fromJson(
                              HashMap<String, dynamic>.from(response));
                          provider.setOpponetId(opponent.id);
                          provider.sendMessage(result);
                        },
                        color: Colors.blue.shade600,
                        icon: Icon(Icons.send),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ChatBubble extends StatelessWidget {
  final QbChatMessage message;
  final int opponentId;

  const ChatBubble({
    this.message,
    this.opponentId,
  });

  bool get _me => message.recipientId == opponentId;

  @override
  Widget build(BuildContext context) {
    return Bubble(
      margin: BubbleEdges.only(top: 10),
      padding: BubbleEdges.all(10),
      alignment: _me ? Alignment.topRight : Alignment.topLeft,
      nip: _me ? BubbleNip.rightTop : BubbleNip.leftTop,
      color: _me ? Color.fromRGBO(225, 255, 199, 1.0) : Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment:
            _me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            message.body,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 14,
              color: Colors.black,
            ),
          ),
          Container(width: 1, height: 2),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                message.timeSentString,
                style: TextStyle(
                  fontSize: 11,
                  color: Colors.grey.shade700,
                ),
              ),
              Text(
                '', // ' delivered',
                style: TextStyle(
                  fontSize: 11,
                  color: Colors.grey.shade700,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
