import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:kisi_flutter/services/api.dart' as api;
import 'package:provider/provider.dart';

class TestScreen extends StatefulWidget {
  @override
  _TestScreenState createState() => _TestScreenState();
}

class _TestScreenState extends State<TestScreen> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: Container(child: _Content()),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var state = ScreenState.content;
  var message = '';

  _ViewModel() {
    fetch();
  }

  fetch() async {
    state = ScreenState.progress;
    notifyListeners();

    try {
      // var response = await api.client.get('http://www.mocky.io/v2/5dc6a34d3800005900cdebfc?mocky-delay=2500ms');
      var client = Dio();
      client.options.followRedirects = false;
      // var response = await client.get('http://www.mocky.io/v2/5dc6a34d3800005900cdebfc?mocky-delay=7000ms');
      // var response = await api.client.get('http://www.mocky.io/v2/5dc6a39d3800002035cdec01?mocky-delay=1000ms');
      var response = await api.client.get(
          'http://www.mocky.io/v2/5dc6ab453800001c35cdec11?mocky-delay=1000ms');

      state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);

      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Test Screen'),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: SimpleViewState(
          state: provider.state,
          onRetry: () => provider.fetch(),
          message: provider.message,
          child: (ctx) => Text('data'),
        ),
      ),
    );
  }
}
