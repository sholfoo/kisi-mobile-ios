import 'package:flutter/material.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/dtos/action_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/register_screen/form_utils.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class ChangePasswordDialog extends StatefulWidget {
  @override
  _ChangePasswordDialogState createState() => _ChangePasswordDialogState();
}

class _ChangePasswordDialogState extends State<ChangePasswordDialog> {
  var _formKey = GlobalKey<FormState>();

  var _oldPasswordCtrl = TextEditingController();
  var _newPasswordCtrl = TextEditingController();
  var _confirmPasswordCtrl = TextEditingController();

  var loading = false;

  @override
  Widget build(BuildContext context) {
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return AlertDialog(
      title: Text(
        loc.main.change_pass_title,
        textAlign: TextAlign.center,
      ),
      contentPadding: EdgeInsets.all(0),
      content: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  controller: _oldPasswordCtrl,
                  validator: (val) {
                    if (val.isEmpty) return loc.general.general_required_field;
                    return null;
                  },
                  obscureText: true,
                  keyboardType: TextInputType.text,
                  decoration: defaultInputDecoration(loc.main.old_pass),
                ),
                formInputSpacer,
                TextFormField(
                  controller: _newPasswordCtrl,
                  validator: (val) {
                    if (val.isEmpty) return loc.general.general_required_field;
                    if (val.length < 8)
                      return loc.general.general_min_length_field
                          .replaceAll('%s', '8');
                    return null;
                  },
                  obscureText: true,
                  keyboardType: TextInputType.text,
                  decoration: defaultInputDecoration(loc.main.new_pass),
                ),
                formInputSpacer,
                TextFormField(
                  controller: _confirmPasswordCtrl,
                  validator: (val) {
                    if (val.isEmpty) return loc.general.general_required_field;
                    if (_confirmPasswordCtrl.text != _newPasswordCtrl.text)
                      return loc.main.confrim_error;
                    return null;
                  },
                  keyboardType: TextInputType.text,
                  obscureText: true,
                  decoration: defaultInputDecoration(loc.main.confirm),
                ),
              ],
            ),
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: loading ? null : () => Navigator.pop(context),
          child: Text(loc.general.dialog_act_cancel),
        ),
        FlatButton(
          onPressed: loading
              ? null
              : () async {
                  if (_formKey.currentState.validate()) {
                    try {
                      progressDialog(context);
                      var response = await api.updatePassword(
                        oldPassword: _oldPasswordCtrl.text,
                        newPassword: _newPasswordCtrl.text,
                      );
                      Navigator.pop(context);

                      var result = ActionDto.fromJson(response.data);

                      if (result.success) {
                        // Close dialog
                        Navigator.pop(context);
                        shortToast(message: result.message);
                      } else
                        error(context, message: result.message);
                    } catch (e) {
                      Navigator.pop(context);
                      error(context, message: api.parseError(e));
                    }
                  }
                },
          child: Text(loc.general.dialog_act_save),
        )
      ],
    );
  }
}
