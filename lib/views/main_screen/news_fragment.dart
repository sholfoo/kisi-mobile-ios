import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kisi_flutter/dtos/news_dto.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/news_section.dart';
import 'package:kisi_flutter/widgets/section.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

import '../../journal_detail_screen.dart';

class NewsFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: Container(child: _Content()),
    );
  }
}

class _ViewModel with ChangeNotifier {
  NewsDto data;
  var state = ScreenState.content;
  var message = '';

  _ViewModel() {
    fetch();
  }

  Future fetch() async {
    state = ScreenState.progress;
    notifyListeners();

    try {
      var response = await api.news();
      data = NewsDto.fromJson(response.data);

      state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);
      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);

    return Center(
      child: SingleChildScrollView(
        child: SimpleViewState(
          state: provider.state,
          message: provider.message,
          onRetry: () => provider.fetch(),
          child: (ctx) {
            return Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            JournalDetailScreen(provider.data.header),
                      ),
                    );
                  },
                  child: Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: AspectRatio(
                            aspectRatio: 2 / 1,
                            child: Image.network(
                              provider.data.header.image,
                              height: 1000,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Container(height: 10),
                        Text(
                          provider.data.header.title,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Container(height: 10),
                        Text(
                          provider.data.header.content,
                          textAlign: TextAlign.justify,
                          maxLines: 6,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                ),
                AppPageSectionSpacer(),
                NewsSection(
                  type: NewsSectionType.stocks,
                  mode: NewsSectionMode.horizontal,
                  data: provider.data.datas.stocknews,
                  state: provider.data.datas.stocknews.isEmpty
                      ? ScreenState.empty
                      : ScreenState.content,
                ),
                AppPageSectionSpacer(),
                NewsSection(
                  type: NewsSectionType.market,
                  mode: NewsSectionMode.vertical,
                  data: provider.data.datas.marketnews,
                  state: provider.data.datas.marketnews.isEmpty
                      ? ScreenState.empty
                      : ScreenState.content,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
