import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/dtos/notification_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class NotificationFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: Container(child: _Content()),
    );
  }
}

class _ViewModel with ChangeNotifier {
  List<NotificationDto> data;
  var state = ScreenState.content;
  var message = '';

  _ViewModel() {
    fetch();
  }

  Future fetch() async {
    state = ScreenState.progress;
    notifyListeners();

    try {
      var response = await api.notifications();
      data = (response.data as List)
          .map<NotificationDto>((x) => NotificationDto.fromJson(x))
          .toList();

      if (data.isEmpty)
        state = ScreenState.empty;
      else
        state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);
      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      appBar: AppBar(title: Text(loc.main.notification_title)),
      body: SimpleViewState(
        state: provider.state,
        message: provider.message,
        onRetry: () => provider.fetch(),
        child: (ctx) => ListView.separated(
          itemCount: provider.data.length,
          itemBuilder: (ctx, i) {
            var item = provider.data[i];
            return _NotificationItem(item: item);
          },
          separatorBuilder: (ctx, i) {
            return Container(
              height: 0.25,
              color: Colors.grey,
            );
          },
        ),
      ),
    );
  }
}

class _NotificationItem extends StatelessWidget {
  final NotificationDto item;

  const _NotificationItem({
    @required this.item,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          showDialog(
              context: context,
              builder: (ctx) {
                return AlertDialog(
                  title: Text(item.title),
                  content: Text(item.content),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text('OK'),
                    )
                  ],
                );
              });
        },
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 16,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    item.title,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    DateFormat('dd MMM yyyy').format(item.createdAt),
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 12,
                    ),
                  )
                ],
              ),
              Text(
                item.content,
                textAlign: TextAlign.justify,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
