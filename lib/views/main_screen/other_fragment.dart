import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/provider/auth_provider.dart';
import 'package:kisi_flutter/provider/quickblox/qb_channel.dart';
import 'package:kisi_flutter/views/login_screen.dart';
import 'package:kisi_flutter/views/main_screen/change_password_dialog.dart';
import 'package:kisi_flutter/views/term_condition_screen.dart';
import 'package:kisi_flutter/widgets/section.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class OtherFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel extends ChangeNotifier {
  PackageInfo packageInfo;

  _ViewModel() {
    fetchPackageInfo();
  }

  fetchPackageInfo() async {
    packageInfo = await PackageInfo.fromPlatform();
    notifyListeners();
  }
}

class _Content extends StatelessWidget {
  _launchSocialMedia(String url) async {
    if (await canLaunch(url)) {
      launch(url);
    } else {
      shortToast(message: 'Tidak bisa membuka berkas');
    }
  }

  @override
  Widget build(BuildContext context) {
    var authProvider = Provider.of<AuthProvider>(
      context,
      listen: false,
    );

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Material(
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(16),
              color: Theme.of(context).primaryColor,
              child: Consumer<AuthProvider>(
                builder: (_, model, __) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        loc.main.cust_data_title,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Text(
                          model.user.name,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      _UserInfoItem(
                        icon: Icons.mail,
                        text: model.user.email,
                      ),
                      _UserInfoItem(
                        icon: Icons.phone,
                        text: model.user.msisdn ?? "",
                      ),
                      Container(height: 8),
                      OutlineButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (ctx) => ChangePasswordDialog(),
                          );
                        },
                        textColor: Colors.white,
                        borderSide: BorderSide(
                          color: Colors.white,
                          style: BorderStyle.solid,
                          width: 1.0,
                        ),
                        highlightedBorderColor: Colors.white,
                        child: Text(loc.main.change_pass),
                      )
                    ],
                  );
                },
              ),
            ),
            AppPageSection(
              title: loc.main.features_title,
              subtitle: loc.main.features_subtitle,
              child: Column(
                children: <Widget>[
                  Container(height: 16),
                  Container(
                    height: 0.25,
                    color: Colors.grey,
                  ),
                  _OtherMenu(
                    icon: 'assets/favorite_menu/menu_faq.png',
                    title: loc.main.act_faq,
                    subtitle: loc.main.act_faq_desc,
                    onTap: () {},
                  ),
                  _OtherMenu(
                    icon: 'assets/favorite_menu/menu_terms.png',
                    title: loc.main.act_policy,
                    subtitle: loc.main.act_policy_desc,
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (ctx) => TermConditionScreen(),
                        ),
                      );
                    },
                  ),
                  Consumer<_ViewModel>(builder: (_, model, __) {
                    return _OtherMenu(
                      icon: 'assets/favorite_menu/menu_info.png',
                      title: loc.main.app_version,
                      subtitle: model.packageInfo == null
                          ? '...'
                          : 'KISI ${model.packageInfo.version}',
                      showChevron: false,
                    );
                  }),
                  _OtherMenu(
                    icon: 'assets/favorite_menu/menu_logout.png',
                    title: loc.main.act_logout,
                    subtitle: loc.main.act_logout_desc,
                    showChevron: false,
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (ctx) {
                          return AlertDialog(
                            title: Text(loc.general.dialog_title_confirm),
                            content: Text(loc.main.act_logout_confirm),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text(loc.general.dialog_act_cancel),
                              ),
                              FlatButton(
                                onPressed: () async {
                                  Navigator.pop(context);
                                  authProvider.clear();
                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                      builder: (ctx) => LoginScreen(),
                                    ),
                                    (_) => false,
                                  );

                                  try {
                                    await QbChannel.disconnect();
                                  } catch (e) {}
                                },
                                child: Text(loc.general.dialog_act_yes),
                              )
                            ],
                          );
                        },
                      );
                    },
                  ),
                  Container(
                    height: 0.25,
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
            Container(height: 40),
            Text('Korea Investment & Sekuritas Indonesia'),
            Container(height: 8),
            Center(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      _launchSocialMedia(
                          'https://www.instagram.com/koreainvestment.id/?hl=id');
                    },
                    child: Icon(
                      FontAwesomeIcons.instagram,
                      color: Theme.of(context).primaryColor,
                      size: 28,
                    ),
                  ),
                  Container(
                    width: 10,
                  ),
                  InkWell(
                    onTap: () {
                      _launchSocialMedia(
                          'https://www.facebook.com/koreainvestment.id/?ref=bookmarks');
                    },
                    child: Icon(
                      FontAwesomeIcons.facebook,
                      color: Theme.of(context).primaryColor,
                      size: 28,
                    ),
                  ),
                  Container(
                    width: 10,
                  ),
                  InkWell(
                    onTap: () {
                      _launchSocialMedia(
                          'https://www.youtube.com/channel/UCQLZXrAMGhKi3GZB8jaBeWQ?view_as=subscriber');
                    },
                    child: Icon(
                      FontAwesomeIcons.youtube,
                      color: Theme.of(context).primaryColor,
                      size: 28,
                    ),
                  ),
                  Container(
                    width: 10,
                  ),
                  InkWell(
                    onTap: () {
                      _launchSocialMedia(
                          'https://www.linkedin.com/company/korea-investment-and-sekuritas-indonesia-pt');
                    },
                    child: Icon(
                      FontAwesomeIcons.linkedin,
                      color: Theme.of(context).primaryColor,
                      size: 28,
                    ),
                  ),
                ],
              ),
            ),
            AppPageSectionSpacer(),
          ],
        ),
      ),
    );
  }
}

class _UserInfoItem extends StatelessWidget {
  final IconData icon;
  final String text;

  const _UserInfoItem({
    this.icon,
    this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          icon,
          size: 15,
          color: Colors.white,
        ),
        Container(width: 8),
        Text(
          text,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}

class _OtherMenu extends StatelessWidget {
  final String icon;
  final String title;
  final String subtitle;
  final bool showChevron;
  final Function onTap;

  const _OtherMenu({
    this.icon,
    this.title,
    this.subtitle,
    this.showChevron = true,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: ListTile(
        leading: Padding(
          padding: const EdgeInsets.all(6),
          child: Image.asset(
            icon,
            width: 28,
          ),
        ),
        title: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Text(subtitle),
        trailing: showChevron ? Icon(Icons.chevron_right) : null,
        onTap: onTap,
      ),
    );
  }
}
