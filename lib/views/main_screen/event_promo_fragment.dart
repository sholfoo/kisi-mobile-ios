import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:kisi_flutter/dtos/event_dto.dart';
import 'package:kisi_flutter/dtos/event_item_dto.dart';
import 'package:kisi_flutter/views/event_promo_detail_screen.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/section.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class EventPromoFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: Container(child: _Content()),
    );
  }
}

class _ViewModel with ChangeNotifier {
  EventDto data;
  var state = ScreenState.content;
  var message = '';

  _ViewModel() {
    fetch();
  }

  Future fetch() async {
    state = ScreenState.progress;
    notifyListeners();

    try {
      var response = await api.events();
      data = EventDto.fromJson(response.data);

      state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);
      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);

    return SimpleViewState(
      state: provider.state,
      message: provider.message,
      onRetry: () => provider.fetch(),
      child: (ctx) => SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (ctx) =>
                          EventPromoDetailScreen(provider.data.header),
                    ),
                  );
                },
                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: AspectRatio(
                          aspectRatio: 2 / 1,
                          child: Image.network(
                            provider.data.header.image,
                            height: 1000,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Container(height: 10),
                      Text(
                        provider.data.header.title,
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Container(height: 10),
                      Html(
                        data: provider.data.header.content.substring(0, 300) +
                            '...',
                      )
                    ],
                  ),
                ),
              ),
              AppPageSectionSpacer(),
              Container(
                color: Colors.white,
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Column(
                  children: provider.data.event
                      .map<Widget>((item) => _EventItem(item))
                      .toList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _EventItem extends StatelessWidget {
  final EventItemDto item;

  const _EventItem(this.item);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (ctx) => EventPromoDetailScreen(item),
              ));
        },
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 8,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    item.image,
                    width: 130,
                    height: 130,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      item.title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Container(height: 8),
                    Html(
                      data: item.content.substring(0, 250) + '...',
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
