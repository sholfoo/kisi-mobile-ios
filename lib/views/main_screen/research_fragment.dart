import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/dtos/research_dto.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;
import 'package:url_launcher/url_launcher.dart';

class ResearchFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var data = List<ResearchDto>();
  var state = ScreenState.content;
  var message = '';

  _ViewModel() {
    fetch();
  }

  Future fetch() async {
    state = ScreenState.progress;
    notifyListeners();

    try {
      var responses = await Future.wait([
        api.researches('daily'),
        api.researches('weekly'),
      ]);

      data.clear();
      responses.forEach((response) {
        data.addAll((response.data as List)
            .map<ResearchDto>((x) => ResearchDto.fromJson(x))
            .toList());
      });

      data.sort((a, b) => b.createdAt.compareTo(a.createdAt));

      if (data.isEmpty)
        state = ScreenState.empty;
      else
        state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);
      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);

    return SimpleViewState(
      state: provider.state,
      message: provider.message,
      onRetry: () => provider.fetch(),
      child: (ctx) => ListView.separated(
        itemCount: provider.data.length,
        itemBuilder: (ctx, i) {
          var item = provider.data[i];
          return _ResearchItem(item: item);
        },
        separatorBuilder: (ctx, i) {
          return Container(
            height: 0.25,
            color: Colors.grey,
          );
        },
      ),
    );
  }
}

class _ResearchItem extends StatelessWidget {
  final ResearchDto item;

  const _ResearchItem({
    @required this.item,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: ListTile(
        onTap: () async {
          if (await canLaunch(item.documentFile)) {
            launch(item.documentFile);
          } else {
            shortToast(message: 'Tidak bisa membuka berkas');
          }
        },
        title: Text(
          item.title.toUpperCase(),
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Row(
          children: <Widget>[
            Text(
              DateFormat('yyyy-MM-dd     hh:mm').format(item.createdAt),
            ),
            Container(width: 20),
            Text(item.type.toUpperCase()),
          ],
        ),
        trailing: Icon(Icons.chevron_right),
      ),
    );
  }
}
