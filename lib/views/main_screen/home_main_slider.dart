import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kisi_flutter/classes/utils.dart';
import 'package:provider/provider.dart';

class HomeMainSlider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var pages = [
    'assets/banner1.jpg',
    'assets/banner2.jpg',
    'assets/banner3.jpg'
  ];
  var _currentPage = 0;

  hasNextPage() {
    return _currentPage < pages.length - 1;
  }

  hasPreviousPage() {
    return _currentPage > 0;
  }

  setCurrentPage(int page) {
    _currentPage = page;
    notifyListeners();
  }

  getCurrentPage() {
    return _currentPage;
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context, listen: false);

    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 16),
          child: CarouselSlider(
            height: MediaQuery.of(context).size.width * 0.9 / 2,
            enlargeCenterPage: false,
            viewportFraction: 0.9,
            initialPage: 0,
            items: provider.pages.map((i) {
              return Builder(
                builder: (BuildContext context) {
                  return Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      child: InkWell(
                        onTap: () {},
                        child: Image.asset(
                          i,
                          fit: BoxFit.cover,
                          width: 1000,
                        ),
                      ),
                    ),
                  );
                },
              );
            }).toList(),
            onPageChanged: (page) {
              provider.setCurrentPage(page);
            },
          ),
        ),
        Positioned(
          // alignment: Alignment.bottomCenter,
          bottom: 8,
          left: 0,
          right: 0,
          child: Consumer<_ViewModel>(
            builder: (_, model, __) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: map<Widget>(
                  provider.pages,
                  (index, url) {
                    return Container(
                      width: 8.0,
                      height: 8.0,
                      margin: EdgeInsets.symmetric(
                        vertical: 0,
                        horizontal: 2,
                      ),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: model.getCurrentPage() == index
                            ? Color.fromRGBO(255, 255, 255, 0.9)
                            : Color.fromRGBO(255, 255, 255, 0.4),
                      ),
                    );
                  },
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
