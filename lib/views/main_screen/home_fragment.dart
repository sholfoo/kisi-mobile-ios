import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kisi_flutter/dtos/news_item_dto.dart';
import 'package:kisi_flutter/views/main_screen/home_main_slider.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/news_section.dart';
import 'package:kisi_flutter/widgets/section.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class HomeFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: Container(
        child: _Content(),
      ),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var stockNewsState = ScreenState.empty;
  var stockNewsMessage = "";
  List<NewsItemDto> stockNewsData;

  var marketNewsState = ScreenState.empty;
  var marketNewsMessage = "";
  List<NewsItemDto> marketNewsData;

  _ViewModel() {
    fetchStockNews();
    fetchMarketNews();
  }

  Future fetchStockNews() async {
    stockNewsState = ScreenState.progress;
    notifyListeners();

    try {
      var response = await api.stockNews();
      stockNewsData = (response.data as List)
          .map<NewsItemDto>((x) => NewsItemDto.fromJson(x))
          .toList();

      if (stockNewsData.isEmpty)
        stockNewsState = ScreenState.empty;
      else
        stockNewsState = ScreenState.content;
      notifyListeners();
    } catch (e) {
      stockNewsState = ScreenState.error;
      stockNewsMessage = api.parseError(e);
      notifyListeners();
    }
  }

  Future fetchMarketNews() async {
    marketNewsState = ScreenState.progress;
    notifyListeners();

    try {
      var response = await api.marketNews();
      marketNewsData = (response.data as List)
          .map<NewsItemDto>((x) => NewsItemDto.fromJson(x))
          .toList();

      if (marketNewsData.isEmpty)
        marketNewsState = ScreenState.empty;
      else
        marketNewsState = ScreenState.content;
      notifyListeners();
    } catch (e) {
      marketNewsState = ScreenState.error;
      marketNewsMessage = api.parseError(e);
      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          HomeMainSlider(),
          AppPageSectionSpacer(),
          NewsSection(
            state: provider.marketNewsState,
            type: NewsSectionType.market,
            mode: NewsSectionMode.horizontal,
            data: provider.marketNewsData,
            message: provider.marketNewsMessage,
            onRetry: () => provider.fetchMarketNews(),
          ),
          AppPageSectionSpacer(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: Image.asset(
                'assets/banner1.jpg',
                width: 1000,
              ),
            ),
          ),
          AppPageSectionSpacer(),
          NewsSection(
            state: provider.stockNewsState,
            type: NewsSectionType.stocks,
            mode: NewsSectionMode.horizontal,
            data: provider.stockNewsData,
            message: provider.stockNewsMessage,
            onRetry: () => provider.fetchStockNews(),
          ),
          AppPageSectionSpacer(),
        ],
      ),
    );
  }
}
