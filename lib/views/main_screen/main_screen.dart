import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/provider/auth_provider.dart';
import 'package:kisi_flutter/views/chat_dialog_screen.dart';
import 'package:kisi_flutter/views/main_screen/event_promo_fragment.dart';
import 'package:kisi_flutter/views/main_screen/home_fragment.dart';
import 'package:kisi_flutter/views/main_screen/news_fragment.dart';
import 'package:kisi_flutter/views/main_screen/notification_fragment.dart';
import 'package:kisi_flutter/views/main_screen/other_fragment.dart';
import 'package:kisi_flutter/views/main_screen/research_fragment.dart';
import 'package:kisi_flutter/views/video_schedule_screen.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

import '../contact_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  var _model = _ViewModel();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _model,
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var _currentPage = 0;

  int get currentPage => _currentPage;
  set currentPage(int value) {
    _currentPage = value;
    notifyListeners();
  }

  List<Widget> pages;

  _ViewModel() {
    pages = [
      HomeFragment(),
      NewsFragment(),
      ResearchFragment(),
      EventPromoFragment(),
      OtherFragment(),
    ];

    // Update device token every main screen open
    api.updateToken();
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);
    var authProvider = Provider.of<AuthProvider>(context);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        title: Image.asset(
          'assets/logo-kisi-long.png',
          height: 28,
        ),
        centerTitle: false,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.notifications),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => NotificationFragment(),
                ),
              );
            },
          ),
          IconButton(
            icon: Icon(Icons.video_call),
            onPressed: () async {
              Widget nextPage = ContactScreen(isPicker: false);
              if (!authProvider.isAdmin) nextPage = VideoScheduleScreen();

              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => nextPage,
                ),
              );
            },
          ),
          IconButton(
            icon: Icon(Icons.chat),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ChatDialogScreen(),
                ),
              );
            },
          )
        ],
      ),
      body: provider.pages[provider.currentPage],
      bottomNavigationBar: BottomNavigationBar(
        onTap: (idx) => provider.currentPage = idx,
        currentIndex: provider.currentPage,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 12,
        unselectedFontSize: 12,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(FontAwesomeIcons.home),
            title: new Text(loc.main.mn_home),
          ),
          BottomNavigationBarItem(
            icon: new Icon(FontAwesomeIcons.newspaper),
            title: new Text(loc.main.mn_news),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.chartPie),
            title: Text(loc.main.mn_research),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.tag),
            title: Text(loc.main.mn_event),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.ellipsisH),
            title: Text(loc.main.mn_other),
          )
        ],
      ),
    );
  }
}
