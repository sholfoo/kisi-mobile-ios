import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/views/login_choose.dart';
import 'package:kisi_flutter/views/privacy_policy_screen.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(
            vertical: 16,
            horizontal: 16,
          ),
          child: Column(
            children: <Widget>[
              Text(
                loc.permission.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(height: 8),
              Text(
                loc.permission.description,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w100,
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(height: 32),
                      _PermissionItemTile(
                          icon: Icons.camera,
                          title: loc.permission.camera,
                          description: loc.permission.camera_desc),
                      Divider(color: Colors.white),
                      _PermissionItemTile(
                        icon: Icons.folder_open,
                        title: loc.permission.file,
                        description: loc.permission.file_desc,
                      ),
                      Divider(color: Colors.white),
                      _PermissionItemTile(
                        icon: Icons.mic,
                        title: loc.permission.mic,
                        description: loc.permission.mic_desc,
                      ),
                      Divider(color: Colors.white),
                      _PermissionItemTile(
                        icon: Icons.location_on,
                        title: loc.permission.loc,
                        description: loc.permission.loc_desc,
                      ),
                      Container(height: 32),
                    ],
                  ),
                ),
              ),
              // Text(
              //   loc.permission.agreement,
              //   textAlign: TextAlign.center,
              //   style: TextStyle(
              //     color: Colors.white,
              //     fontSize: 10,
              //     fontWeight: FontWeight.w100,
              //   ),
              // ),
              SizedBox(
                width: double.infinity,
                child: MaterialButton(
                  color: Color.fromARGB(255, 156, 50, 41),
                  textColor: Colors.white,
                  child: Text(loc.permission.btn_agree),
                  onPressed: () async {
                    var permissions =
                        await PermissionHandler().requestPermissions(
                      [
                        PermissionGroup.camera,
                        // PermissionGroup.storage,
                        PermissionGroup.microphone,
                        // PermissionGroup.location,
                      ],
                    );

                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (ctx) => PrivacyPolicyScreen(),
                      ),
                    );
                  },
                ),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (ctx) => LoginChooseScreen(),
                    ),
                  );
                },
                child: Text(loc.permission.btn_skip),
                textColor: Colors.grey.shade300,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _PermissionItemTile extends StatelessWidget {
  final IconData icon;
  final String title;
  final String description;

  const _PermissionItemTile({
    this.icon,
    this.title,
    this.description,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        icon,
        color: Colors.white,
      ),
      title: Text(
        title,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(
        description,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w100,
        ),
      ),
    );
  }
}
