import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermConditionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var data = '';

  _ViewModel() {
    fetch();
  }

  void fetch() async {
    data =
        await rootBundle.loadString('assets/reading/term-and-condition.html');
    notifyListeners();
  }
}

class _Content extends StatelessWidget {
  WebViewController _controller;

  @override
  Widget build(BuildContext context) {
    // final provider = Provider.of<_ViewModel>(context);
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      appBar: AppBar(title: Text(loc.main.act_policy)),
      body: WebView(
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          _controller = webViewController;
          _loadHtmlFromAssets();
        },
      ),
    );
  }

  _loadHtmlFromAssets() async {
    String fileText =
        await rootBundle.loadString('assets/reading/term-and-condition.html');
    _controller.loadUrl(Uri.dataFromString(fileText,
            mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
        .toString());
  }
}
