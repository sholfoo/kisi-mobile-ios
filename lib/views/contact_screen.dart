import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kisi_flutter/dtos/MemberDto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/provider/auth_provider.dart';
import 'package:kisi_flutter/provider/quickblox/qb_channel.dart';
import 'package:kisi_flutter/widgets/base_screen_state.dart';
import 'package:kisi_flutter/widgets/simple_view_state.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class ContactScreen extends StatefulWidget {
  final bool isPicker;

  ContactScreen({
    this.isPicker = true,
  });

  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  var _model = _ViewModel();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _model,
      child: Container(
        child: _Content(
          isPicker: widget.isPicker,
        ),
      ),
    );
  }
}

class _ViewModel with ChangeNotifier {
  List<MemberDto> data;
  var state = ScreenState.content;
  var message = '';

  var searchController = TextEditingController();

  _ViewModel() {
    fetch();
  }

  Future fetch() async {
    state = ScreenState.progress;
    notifyListeners();

    try {
      var search = searchController.text.toLowerCase();

      var response = await api.members();
      data = (response.data as List)
          .map<MemberDto>((x) => MemberDto.fromJson(x))
          .where((x) =>
              x.name.toLowerCase().contains(search) ||
              x.email.toLowerCase().contains(search))
          .toList();

      if (data.isEmpty)
        state = ScreenState.empty;
      else
        state = ScreenState.content;
      notifyListeners();
    } catch (e) {
      state = ScreenState.error;
      message = api.parseError(e);
      notifyListeners();
    }
  }
}

class _Content extends StatelessWidget {
  final bool isPicker;

  _Content({this.isPicker});

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);
    var authProvider = Provider.of<AuthProvider>(context);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    Timer _debounce;

    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        title: Text(loc.main.contact_title),
      ),
      body: Column(
        children: <Widget>[
          Material(
            color: Colors.white,
            child: Container(
              padding: EdgeInsets.all(8),
              child: TextField(
                controller: provider.searchController,
                onChanged: (str) {
                  if (_debounce?.isActive ?? false) _debounce.cancel();
                  _debounce = Timer(const Duration(milliseconds: 500), () {
                    provider.fetch();
                  });
                },
                keyboardType: TextInputType.text,
                maxLines: 1,
                decoration: InputDecoration(
                  hintText: 'Pencarian...',
                  border: OutlineInputBorder(),
                  contentPadding: const EdgeInsets.all(12),
                ),
              ),
            ),
          ),
          Expanded(
            child: SimpleViewState(
              state: provider.state,
              message: provider.message,
              onRetry: () => provider.fetch(),
              child: (ctx) => ListView.separated(
                itemCount: provider.data.length,
                itemBuilder: (ctx, i) {
                  var item = provider.data[i];
                  return Material(
                    color: Colors.white,
                    child: ListTile(
                      dense: true,
                      onTap: () {
                        if (isPicker) Navigator.pop(context, item);
                      },
                      title: Text(
                        item.name,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        item.email,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      trailing: Visibility(
                        visible: authProvider.isAdmin,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            IconButton(
                              onPressed: () => QbChannel.startCall(
                                item.qbId,
                                ConferenceType.video,
                              ),
                              color: Colors.blue,
                              icon: Icon(Icons.video_call),
                            ),
                            IconButton(
                              onPressed: () => QbChannel.startCall(
                                item.qbId,
                                ConferenceType.audio,
                              ),
                              color: Colors.blue,
                              icon: Icon(Icons.call),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (ctx, i) {
                  return Container(
                    height: 0.25,
                    color: Colors.grey,
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
