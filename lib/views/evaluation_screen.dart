import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';

class EvaluationScreen extends StatelessWidget {
  final Function(BuildContext ctx) onStartPressed;
  final DateTime expireDate;

  EvaluationScreen({
    @required this.expireDate,
    @required this.onStartPressed,
  });

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);

    return ChangeNotifierProvider.value(
      value: _ViewModel(),
      child: _Content(
        expireDate: this.expireDate,
        onStartPressed: this.onStartPressed,
      ),
    );
  }
}

class _ViewModel extends ChangeNotifier {
  PackageInfo packageInfo;

  _ViewModel() {
    fetchPackageInfo();
  }

  fetchPackageInfo() async {
    packageInfo = await PackageInfo.fromPlatform();
    notifyListeners();
  }
}

class _Content extends StatelessWidget {
  final Function(BuildContext ctx) onStartPressed;
  final DateTime expireDate;

  _Content({
    @required this.expireDate,
    @required this.onStartPressed,
  });

  @override
  Widget build(BuildContext context) {
    var currentDate = DateTime.now();

    var expired = currentDate.isAfter(expireDate);
    var expiresIn = currentDate.difference(expireDate).inDays.abs();

    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 16,
              right: 16,
              child: FlatButton.icon(
                onPressed: () {},
                icon: Icon(Icons.info), 
                label: Text('Info'),
              ),
            ),
            Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    'Evaluation Version',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                    ),
                  ),
                  Consumer<_ViewModel>(
                    builder: (_, model, __) {
                      var pi = model.packageInfo;
                      return (pi != null)
                          ? Text(
                              '${pi.appName} (${pi.version}+${pi.buildNumber})',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 15),
                            )
                          : Text('fetching...');
                    },
                  ),
                  Container(height: 20),
                  Text(
                      'Valid until ${DateFormat('d MMM yyyy').format(expireDate)}'),
                  Visibility(
                    visible: !expired,
                    child: Text(
                      expiresIn > 0
                          ? 'Expires in $expiresIn day${expiresIn > 1 ? 's' : ''}'
                          : 'Expired today',
                      style: TextStyle(
                        color: expiresIn > 0 ? Colors.green : Colors.orange,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: expired,
                    child: Text(
                      'Expired',
                      style: TextStyle(color: Colors.red),
                    ),
                  ),
                  Container(height: 20),
                  OutlineButton(
                    onPressed: expired ? null : () => onStartPressed(context),
                    child: Text('START'),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
