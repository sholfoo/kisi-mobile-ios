import 'dart:collection';
import "package:collection/collection.dart";

import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/dtos/MemberDto.dart';
import 'package:kisi_flutter/provider/quickblox/qb_channel.dart';
import 'package:kisi_flutter/provider/quickblox/qb_chat_message.dart';
import 'package:provider/provider.dart';
import 'package:kisi_flutter/services/api.dart' as api;

class ChatScreen extends StatefulWidget {
  final MemberDto opponent;

  const ChatScreen({
    this.opponent,
  });

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  var _model = _ViewModel();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _model,
      child: _Content(
        opponent: widget.opponent,
      ),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var textController = TextEditingController();
  var showSendButton = false;

  var _opponentId = 0;
  List<QbChatMessage> messages = List();

  _ViewModel() {
    QbChannel.addOnMessageReceivedListener((msg) {
      QbChannel.qbMarkAsRead(msg.id, msg.dialogId);
      messages.add(msg);
      notifyListeners();
    });
    QbChannel.addOnMessageDeliveredListener((messageId, dialogId, toUserId) {
      var message = messages.firstWhere(
          (x) => x.id == messageId && x.dialogId == dialogId,
          orElse: () => null);
      if (message != null) {
        message.deliveredIds.add(toUserId);
        notifyListeners();
      }
    });
    QbChannel.addOnMessageReadedListener((messageId, dialogId, readerId) {
      var message = messages.firstWhere(
          (x) => x.id == messageId && x.dialogId == dialogId,
          orElse: () => null);
      if (message != null) {
        message.readIds.add(readerId);
        notifyListeners();
      }
    });
    QbChannel.addOnMessageSentListener((msg) {
      var message = messages.firstWhere((x) => x.requestCode == msg.requestCode,
          orElse: () => null);
      if (message != null) {
        message.id = msg.id;
        message.dialogId = msg.dialogId;
        message.requestCode = null;
        message.deliveredIds = msg.deliveredIds;
        notifyListeners();
      }
    });

    // Setup text message listener
    textController.addListener(() {
      if (textController.text.isEmpty) {
        if (showSendButton) {
          showSendButton = false;
          notifyListeners();
        }
      } else {
        if (!showSendButton) {
          showSendButton = true;
          notifyListeners();
        }
      }
    });
  }

  fetch() async {
    var response = await QbChannel.qbGetChatDialogHistories(_opponentId);
    messages = (response as List)
        .map((x) => QbChatMessage.fromJson(HashMap<String, dynamic>.from(x)))
        .toList();
    print((response as List));
    notifyListeners();
  }

  setOpponetId(int id) {
    if (_opponentId != id) {
      _opponentId = id;
      fetch();
    }
  }

  sendMessage(QbChatMessage message) {
    messages.add(message);
    notifyListeners();
  }
}

class _Content extends StatelessWidget {
  final MemberDto opponent;

  const _Content({
    this.opponent,
  });

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(context);
    provider.setOpponetId(opponent.qbId);

    var grouped = groupBy<QbChatMessage, String>(
      provider.messages,
      (QbChatMessage x) => x.dateSentString,
    );
    var widgets = [];
    grouped.forEach((group, items) {
      var dateNow = DateFormat('dd MMMM').format(DateTime.now());
      var dateChat = DateFormat('dd MMMM').format(DateTime.parse(group));

      widgets.add(Column(
        children: <Widget>[
          Bubble(
            alignment: Alignment.center,
            color: Color.fromRGBO(212, 234, 244, 1.0),
            margin: BubbleEdges.all(10),
            stick: true,
            child: Text(
              dateNow == dateChat ? 'Today' : dateChat,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 11.0),
            ),
          ),
          ...items.map(
            (x) => ChatBubble(
              message: x,
              opponentId: opponent.qbId,
            ),
          )
        ],
      ));
    });

    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        title: Text(opponent.name),
        actions: <Widget>[
          PopupMenuButton<int>(
            onSelected: (selected) {
              if (selected == 1)
                confirm(
                  context,
                  message: 'Apakah Anda ingin menghapus history chat ini?',
                  onOk: () async {
                    try {
                      // Dialog
                      Navigator.pop(context);

                      progressDialog(context);
                      await QbChannel.deleteChatDialog(opponent.qbId);
                      // Progress Dialog
                      Navigator.pop(context);

                      // Page
                      Navigator.pop(context);
                    } catch (e) {
                      Navigator.pop(context);
                      error(context, message: api.parseError(e));
                    }
                  },
                );
            },
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem<int>(
                  value: 1,
                  child: Text('DELETE CHAT'),
                )
              ];
            },
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              reverse: true,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  verticalDirection: VerticalDirection.down,
                  children: <Widget>[
                    ...widgets,
                  ],
                ),
              ),
            ),
          ),
          Material(
            color: Colors.white,
            child: Container(
              padding: EdgeInsets.all(8),
              child: SafeArea(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        controller: provider.textController,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                          hintText: 'Type message...',
                          border: OutlineInputBorder(),
                          contentPadding: EdgeInsets.all(12),
                        ),
                      ),
                    ),
                    Visibility(
                      visible: provider.showSendButton,
                      child: IconButton(
                        onPressed: () async {
                          var response = await QbChannel.sendDialogMessage(
                            opponent.qbId,
                            provider.textController.text,
                          );
                          provider.textController.text = '';

                          var result = QbChatMessage.fromJson(
                            HashMap<String, dynamic>.from(response),
                          );
                          // provider.setOpponetId(opponent.qbId);
                          provider.sendMessage(result);
                        },
                        color: Colors.blue.shade600,
                        icon: Icon(Icons.send),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ChatBubble extends StatelessWidget {
  final QbChatMessage message;
  final int opponentId;

  const ChatBubble({
    this.message,
    this.opponentId,
  });

  bool get _me => message.recipientId == opponentId;

  @override
  Widget build(BuildContext context) {
    var isSent = message.requestCode == null || message.requestCode.isEmpty;
    var isDelivered = message.deliveredIds.any((x) => x == opponentId);
    var isReaded = message.readIds.any((x) => x == opponentId);

    var statusIcon = Icons.timelapse;
    if (isSent) statusIcon = Icons.done;
    if (isDelivered || isSent) statusIcon = Icons.done_all;

    var statusColor = Colors.grey.shade700;
    if (isReaded) statusColor = Colors.blue.shade300;

    return Bubble(
      margin: BubbleEdges.only(top: 10),
      padding: BubbleEdges.all(10),
      alignment: _me ? Alignment.topRight : Alignment.topLeft,
      nip: _me ? BubbleNip.rightTop : BubbleNip.leftTop,
      color: _me ? Color.fromRGBO(225, 255, 199, 1.0) : Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment:
            _me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            message.body,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 14,
              color: Colors.black,
            ),
          ),
          Container(width: 1, height: 2),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                message.timeSentString,
                style: TextStyle(
                  fontSize: 11,
                  color: Colors.grey.shade700,
                ),
              ),
              Visibility(
                visible: _me,
                child: Row(children: <Widget>[
                  Container(width: 5),
                  Icon(
                    statusIcon,
                    size: 13,
                    color: statusColor,
                  ),
                ]),
              ),
            ],
          )
        ],
      ),
    );
  }
}
