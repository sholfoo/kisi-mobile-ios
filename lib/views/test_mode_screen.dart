import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:kisi_flutter/provider/auth_provider.dart';
import 'package:kisi_flutter/views/permission_screen.dart';
import 'package:kisi_flutter/views/qb_test/qb_login_screen.dart';

import 'main_screen/main_screen.dart';

class TestModeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            OutlineButton(
              onPressed: () {
                var authProvider = GetIt.I<AuthProvider>();
                Widget nextPage = PermissionScreen();
                if (authProvider.isLogged) nextPage = MainScreen();
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (ctx) => nextPage),
                );
              },
              child: Text('TEST APP'),
            ),
            OutlineButton(
              onPressed: () async {
                // var permissions = await PermissionHandler().requestPermissions(
                //   [
                //     PermissionGroup.camera,
                //     PermissionGroup.storage,
                //     PermissionGroup.microphone,
                //     PermissionGroup.location,
                //   ],
                // );

                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (ctx) => QbLoginScreen()),
                );
              },
              child: Text('TEST QUICKBLOX INTEGRATION'),
            )
          ],
        ),
      ),
    );
  }
}
