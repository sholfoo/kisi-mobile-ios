import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:kisi_flutter/dtos/event_item_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';

class EventPromoDetailScreen extends StatelessWidget {
  final EventItemDto event;

  EventPromoDetailScreen(this.event);

  @override
  Widget build(BuildContext context) {
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(loc.main.event_detail_title),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: AspectRatio(
                    aspectRatio: 2 / 1,
                    child: Image.network(
                      event.image,
                      height: 1000,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(height: 10),
                Text(
                  event.title,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Container(height: 20),
                Html(data: event.content)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
