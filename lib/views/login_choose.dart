import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:get_it/get_it.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/provider/locale_provider.dart';
import 'package:kisi_flutter/views/register_screen/register_screen.dart';

import 'login_screen.dart';

class LoginChooseScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setNavigationBarWhiteForeground(true);

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      backgroundColor: Color.fromARGB(255, 113, 59, 39),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.1, 0.3, 0.9],
            colors: [
              Color.fromARGB(255, 115, 61, 39),
              Color.fromARGB(255, 86, 35, 30),
              Color.fromARGB(255, 128, 57, 51),
            ],
          ),
        ),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 32),
                      child: Image.asset(
                        'assets/logo-kisi.png',
                        height: 40,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 80,
                      bottom: 8,
                    ),
                    child: Text(
                      loc.login.choose1,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                  ),
                  _ButtonChoose(
                    title: loc.login.choose1_title,
                    subtitle: loc.login.choose1_desc,
                    onTap: () async {
                      var lang = await showDialog<Locale>(
                        context: context,
                        builder: (ctx) {
                          return AlertDialog(
                            content: Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    loc.login.choose_lang_title,
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                    ),
                                  ),
                                  Container(height: 20),
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Expanded(
                                        child: InkWell(
                                          onTap: () => Navigator.pop(
                                              context, Locale('id', 'ID')),
                                          child: AspectRatio(
                                            aspectRatio: 1 / 1,
                                            child: Image.asset(
                                              'assets/id_flag_button.png',
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(width: 16),
                                      Expanded(
                                        child: InkWell(
                                          onTap: () => Navigator.pop(
                                              context, Locale('en', 'US')),
                                          child: AspectRatio(
                                            aspectRatio: 1 / 1,
                                            child: Image.asset(
                                              'assets/uk_flag_button.png',
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      );

                      if (lang != null) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (ctx) => RegisterScreen(),
                          ),
                        );

                        var localeProvider = GetIt.I<LocaleProvider>();
                        localeProvider.changeLocale(lang);
                      }
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 48,
                      bottom: 8,
                    ),
                    child: Text(
                      loc.login.choose2,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                  ),
                  _ButtonChoose(
                    title: loc.login.choose2_title,
                    subtitle: loc.login.choose2_desc,
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (ctx) => LoginScreen()),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _ButtonChoose extends StatelessWidget {
  final String title;
  final String subtitle;
  final Function onTap;

  _ButtonChoose({this.title, this.subtitle, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(10),
      onTap: () {
        if (onTap != null) onTap();
      },
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: Colors.white.withAlpha(25),
          border: Border.all(
            color: Colors.white,
            width: 0.25,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    this.title,
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Icon(
                  Icons.chevron_right,
                  color: Colors.white,
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                this.subtitle,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w100,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
