import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kisi_flutter/classes/dialog.dart';
import 'package:kisi_flutter/dtos/login_dto.dart';
import 'package:kisi_flutter/generated/locale_base.dart';
import 'package:kisi_flutter/provider/auth_provider.dart';
import 'package:kisi_flutter/provider/quickblox/qb_channel.dart';
import 'package:kisi_flutter/views/forgot_password_dialog.dart';
import 'package:kisi_flutter/views/login_choose.dart';
import 'package:kisi_flutter/views/main_screen/main_screen.dart';
import 'package:kisi_flutter/widgets/app_checkbox.dart';
import 'package:kisi_flutter/widgets/app_checkbox_dialog.dart';
import 'package:provider/provider.dart';
import '../services/api.dart' as api;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var _model = _ViewModel();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: _model,
      child: Container(child: _Content()),
    );
  }
}

class _ViewModel with ChangeNotifier {
  var usernameCtrl = TextEditingController(text: 'rizali@kisi.co.id');
  var passwordCtrl = TextEditingController(text: 'password');

  var _rememberMe = true;
  get rememberMe => _rememberMe;
  set rememberMe(rememberMe) {
    _rememberMe = rememberMe;
    notifyListeners();
  }

  var _loading = false;
  get loading => _loading;
  set loading(loading) {
    _loading = loading;
    notifyListeners();
  }
}

class _Content extends StatelessWidget {
  connectQuickBlox(AuthProvider authProvider) async {
    if (authProvider.isLogged) {
      await QbChannel.connect(
        login: authProvider.qbUsername,
        password: authProvider.user.qbPass,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ViewModel>(
      context,
      listen: false,
    );
    var authProvider = Provider.of<AuthProvider>(
      context,
      listen: false,
    );

    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      backgroundColor: Color.fromARGB(255, 113, 59, 39),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.1, 0.3, 0.9],
            colors: [
              Color.fromARGB(255, 115, 61, 39),
              Color.fromARGB(255, 86, 35, 30),
              Color.fromARGB(255, 128, 57, 51),
            ],
          ),
        ),
        child: SingleChildScrollView(
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 32),
                      child: Image.asset(
                        'assets/logo-kisi.png',
                        height: 40,
                      ),
                    ),
                  ),
                  Consumer<_ViewModel>(
                    builder: (_, model, __) => Container(
                      padding: const EdgeInsets.only(
                        top: 80,
                        left: 16,
                        right: 16,
                      ),
                      child: Column(
                        children: <Widget>[
                          _TextField(
                            controller: model.usernameCtrl,
                            inputType: TextInputType.emailAddress,
                            icon: Icons.person,
                            title: loc.login.email,
                          ),
                          Padding(padding: EdgeInsets.only(top: 8)),
                          _TextField(
                            controller: model.passwordCtrl,
                            inputType: TextInputType.text,
                            icon: Icons.lock,
                            title: loc.login.password,
                            obsecureText: true,
                          ),
                          Container(height: 10),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              AppCheckboxDialog(
                                value: model.rememberMe,
                                label: loc.login.remember_me,
                                labelStyle: TextStyle(color: Colors.white),
                                checkColor: Colors.brown,
                                activeColor: Colors.white,
                                onChanged: (val) {
                                  model.rememberMe = val;
                                },
                              ),
                              InkWell(
                                onTap: () {
                                  showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (ctx) => ForgotPasswordDialog(),
                                  );
                                },
                                child: Text(
                                  loc.login.forgot_password,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(top: 32)),
                          SizedBox(
                            width: double.infinity,
                            height: 45,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: RaisedButton(
                                color: Color.fromARGB(255, 156, 50, 41),
                                onPressed: model.loading
                                    ? null
                                    : () async {
                                        try {
                                          var email = model.usernameCtrl.text;
                                          var password =
                                              model.passwordCtrl.text;

                                          if (email.isEmpty) return;
                                          if (password.isEmpty) return;

                                          model.loading = true;

                                          var response = await api.login({
                                            'email': email,
                                            'password': password,
                                          });
                                          var result =
                                              LoginDto.fromJson(response.data);

                                          if (!result.success) {
                                            error(
                                              context,
                                              message: result.message,
                                            );
                                            model.loading = false;
                                            return;
                                          }

                                          authProvider.update(
                                            result.data.token,
                                            result.data.data,
                                            email,
                                            password,
                                          );
                                          if (provider.rememberMe)
                                            authProvider.makePersistent();

                                          // QuickBlox Conect
                                          connectQuickBlox(authProvider);

                                          model.loading = false;

                                          Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                              builder: (ctx) => MainScreen(),
                                            ),
                                            (_) => false,
                                          );
                                        } catch (e) {
                                          model.loading = false;
                                          error(
                                            context,
                                            message: api.parseError(e),
                                          );
                                        }
                                      },
                                textColor: Colors.white,
                                disabledTextColor: Colors.grey.shade300,
                                child: Text(
                                  model.loading
                                      ? loc.login.loading
                                      : loc.login.login,
                                ),
                              ),
                            ),
                          ),
                          FlatButton(
                            onPressed: model.loading
                                ? null
                                : () {
                                    Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                        builder: (ctx) => LoginChooseScreen(),
                                      ),
                                      (_) => false,
                                    );
                                  },
                            child: Text(loc.login.cancel),
                            textColor: Colors.grey.shade300,
                            disabledTextColor: Colors.grey.shade500,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _TextField extends StatelessWidget {
  final TextEditingController controller;
  final IconData icon;
  final String title;
  final TextInputType inputType;
  final bool obsecureText;

  _TextField({
    this.controller,
    this.icon,
    this.title,
    this.inputType = TextInputType.text,
    this.obsecureText = false,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: Container(
        color: Colors.white,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              height: 55,
              width: 55,
              color: Color.fromARGB(255, 182, 69, 52),
              child: Center(
                child: Icon(
                  icon,
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
              child: TextField(
                controller: controller,
                keyboardType: inputType,
                obscureText: obsecureText,
                decoration: InputDecoration(
                  hintText: title,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.all(16),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
