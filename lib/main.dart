import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:kisi_flutter/provider/auth_provider.dart';
import 'package:kisi_flutter/provider/locale_provider.dart';
import 'package:kisi_flutter/provider/quickblox/qb_channel.dart';
import 'package:kisi_flutter/services/app_localization.dart';
import 'package:kisi_flutter/views/evaluation_screen.dart';
import 'package:kisi_flutter/views/main_screen/main_screen.dart';
import 'package:kisi_flutter/views/permission_screen.dart';
import 'package:kisi_flutter/views/qb_test/model/qb_login_view_model.dart';
import 'package:provider/provider.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

const MaterialColor _colorPrimary = MaterialColor(
  _colorPrimaryValue,
  <int, Color>{
    50: Color(0xFFE3F2FD),
    100: Color(0xFFBBDEFB),
    200: Color(0xFF90CAF9),
    300: Color(0xFF64B5F6),
    400: Color(0xFF42A5F5),
    500: Color(_colorPrimaryValue),
    600: Color(0xFF1E88E5),
    700: Color(0xFF1976D2),
    800: Color(0xFF1565C0),
    900: Color(0xFF0D47A1),
  },
);
const int _colorPrimaryValue = 0xFF567DB3;

_printToken() async {
  var fcm = FirebaseMessaging();
  var token = await fcm.getToken();
  print(token);
}

Future<dynamic> _backgroundMessageHandler(Map<String, dynamic> message) async {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
    print(data);
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    print(notification);
  }

  // Or do other work.
}

setupQuickBlox() async {
  var authProvider = GetIt.I<AuthProvider>();

  await QbChannel.initialize(
    // DEMO
    // appId: '78916',
    // authKey: 'NcBdkSx2mzJ-ZPn',
    // authSecret: '4AADna3GjNjfm3g',
    // accountKey: '-nfSas1fB7ZmCo9BuzzX',
    // KISI
    appId: '78817',
    authKey: 'xg4Puaxkm6BmNcw',
    authSecret: 'smg2TfmtRnAszwD',
    accountKey: 'iad6VCq3bLrm7XdApxUT',
  );
  if (authProvider.isLogged) {
    await QbChannel.connect(
      login: authProvider.qbUsername,
      password: authProvider.user.qbPass,
    );
  }
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  _printToken();

  // var fcm = FirebaseMessaging();
  // fcm.configure(
  //   onMessage: (Map<String, dynamic> message) async {
  //     print("onMessage: $message");
  //   },
  //   onBackgroundMessage: _backgroundMessageHandler,
  //   onLaunch: (Map<String, dynamic> message) async {
  //     print("onLaunch: $message");
  //   },
  //   onResume: (Map<String, dynamic> message) async {
  //     print("onResume: $message");
  //   },
  // );

  GetIt.I.registerSingleton<AuthProvider>(AuthProvider());
  GetIt.I.registerSingleton<LocaleProvider>(LocaleProvider());

  setupQuickBlox();

  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => GetIt.I<AuthProvider>()),
        ChangeNotifierProvider(create: (_) => GetIt.I<LocaleProvider>()),
        ChangeNotifierProvider(create: (_) => QbLoginViewModel())
      ],
      child: _Content(),
    );
  }
}

class _Content extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var localeProvider = Provider.of<LocaleProvider>(context);

    var authProvider = GetIt.I<AuthProvider>();
    Widget nextPage = PermissionScreen();
    if (authProvider.isLogged) nextPage = MainScreen();

    return MaterialApp(
      title: 'KISI Mobile',
      theme: ThemeData(
        primarySwatch: _colorPrimary,
        textTheme: Theme.of(context).textTheme.apply(
              fontFamily: 'OpenSans',
            ),
      ),
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: localeProvider.currentLocale,
      localeResolutionCallback: (
        Locale locale,
        Iterable<Locale> supportedLocales,
      ) {
        if (locale != null)
          for (var supportedLocale in supportedLocales)
            if (locale.countryCode == supportedLocale.countryCode)
              return supportedLocale;
        return supportedLocales.first;
      },
      supportedLocales: [
        Locale('id', 'ID'),
        Locale('en', 'US'),
      ],
      debugShowCheckedModeBanner: false,
      home: nextPage,
      // home: EvaluationScreen(
      //   expireDate: DateTime(2019, 12, 25, 23, 59),
      //   onStartPressed: (ctx) async {
      //     Navigator.pushReplacement(
      //       ctx,
      //       MaterialPageRoute(builder: (_) => nextPage),
      //     );
      //   },
      // ),
    );
  }
}
