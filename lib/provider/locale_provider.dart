import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/classes/settings.dart';

class LocaleProvider with ChangeNotifier {
  Locale currentLocale = Locale('id', 'ID');

  LocaleProvider() {
    loadLocale();
  }

  changeLocale(Locale locale) async {
    await Settings.setLocale(locale);
    loadLocale();
  }

  loadLocale() async {
    currentLocale = await Settings.getLocale();
    Intl.defaultLocale = currentLocale.languageCode;
    notifyListeners();
  }
}
