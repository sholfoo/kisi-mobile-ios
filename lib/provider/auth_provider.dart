import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:kisi_flutter/dtos/login_dto.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider with ChangeNotifier {
  String token;
  UserData user;
  String qbUsername;
  // String qbPassword;

  bool get isLogged => token != null && token.trim().isNotEmpty && user != null;
  bool get isAdmin => user.roleName == 'admin-kisi';

  AuthProvider() {
    load();
  }

  update(
    String token,
    UserData user,
    String qbUsername,
    String qbPassword,
  ) async {
    this.token = token;
    this.user = user;
    this.qbUsername = qbUsername;

    notifyListeners();
  }

  clear() async {
    var sp = await SharedPreferences.getInstance();
    sp.clear();

    token = '';
    user = null;
  }

  makePersistent() async {
    var sp = await SharedPreferences.getInstance();
    sp.setString('token', token);
    sp.setString('user-data', jsonEncode(user.toJson()));
    sp.setString('qb-username', qbUsername);
  }

  load() async {
    var sp = await SharedPreferences.getInstance();
    token = sp.getString('token');
    if (token != null && token.isNotEmpty) {
      user = UserData.fromJson(jsonDecode(sp.getString('user-data')));
      qbUsername = sp.getString('qb-username');
    }
    notifyListeners();
  }
}
