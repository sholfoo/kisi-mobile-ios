import 'dart:collection';

import 'package:intl/intl.dart';

class QbChatMessage {
  String id;
  String dialogId;
  String requestCode;
  int recipientId;
  int senderId;
  String body;
  int dateSent;
  String dateSentString;
  String timeSentString;
  List<int> readIds;
  List<int> deliveredIds;

  QbChatMessage.fromJson(HashMap<String, dynamic> json)
      : id = json['id'],
        dialogId = json['dialogId'],
        requestCode = json['requestCode'],
        recipientId = json['recipientId'],
        senderId = json['senderId'],
        body = json['body'],
        dateSent = json['dateSent'] ?? (DateTime.now().millisecondsSinceEpoch~/1000),
        dateSentString = DateFormat('yyyy-MM-dd').format(
          DateTime.fromMillisecondsSinceEpoch(((json['dateSent'] ?? (DateTime.now().millisecondsSinceEpoch~/1000))) * 1000),
        ),
        timeSentString = DateFormat('HH:mm').format(
          DateTime.fromMillisecondsSinceEpoch((json['dateSent'] ?? (DateTime.now().millisecondsSinceEpoch~/1000)) * 1000),
        ),
        readIds = (json['readIds'] as List).map((x) => x as int).toList(),
        deliveredIds =
            (json['deliveredIds'] as List).map((x) => x as int).toList();
}
