import 'dart:collection';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:kisi_flutter/classes/utils.dart';
import 'package:kisi_flutter/provider/quickblox/qb_chat_message.dart';

enum ConferenceType { video, audio }

class QbChannel {
  static const platform = const MethodChannel("qb_channel");
  static const platformStream = const EventChannel('qb_event_channel');

  static Future<bool> initialize({
    String appId,
    String authKey,
    String authSecret,
    String accountKey,
  }) async {
    // Setup event listener on initialize
    _setupEventListener();

    // Call QB Initialize
    try {
      await platform.invokeMethod(
        'initialize',
        {
          'appId': appId,
          'authKey': authKey,
          'authSecret': authSecret,
          'accountKey': accountKey,
        },
      );
      return true;
    } on PlatformException catch (e) {
      print(e.message);
      return false;
    }
  }

  static _setupEventListener() {
    // Initial event listener, for comunicate with native
    platformStream.receiveBroadcastStream().listen(
      (dynamic event) {
        print('Stream: ' + jsonEncode(event));
        var map = HashMap<String, dynamic>.from(event);

        var type = map['type'];
        if (type == 'message_received') {
          var message = QbChatMessage.fromJson(
            HashMap<String, dynamic>.from(map['data']),
          );
          _onMessageReceivedListeners.forEach((x) => x(message));
        }
        if (type == 'message_delivered') {
          var data = HashMap<String, dynamic>.from(map['data']);
          var messageId = data['messageId'] as String;
          var dialogId = data['dialogId'] as String;
          var toUserId = data['toUserId'] as int;
          _onMessageDeliveredListeners
              .forEach((x) => x(messageId, dialogId, toUserId));
        }
        if (type == 'message_readed') {
          var data = HashMap<String, dynamic>.from(map['data']);
          var messageId = data['messageId'] as String;
          var dialogId = data['dialogId'] as String;
          var readerId = data['readerId'] as int;
          _onMessageReadedListeners
              .forEach((x) => x(messageId, dialogId, readerId));
        }
        if (type == 'message_sent') {
          var message = QbChatMessage.fromJson(
            HashMap<String, dynamic>.from(map['data']),
          );
          _onMessageSentListeners.forEach((x) => x(message));
        }
      },
      onError: (dynamic error) => print('Received error: ${error.message}'),
    );
  }

  static connect({
    String login,
    String password,
  }) {
    try {
      print('Connect with $login, $password');
      return platform.invokeMethod(
        'qbConnect',
        {
          'login': login,
          'password': password,
        },
      );
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  static disconnect() {
    try {
      return platform.invokeMethod('qbDisconnect');
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  // static signIn({
  //   String login,
  //   String password,
  // }) {
  //   try {
  //     return platform.invokeMethod(
  //       'qbSignIn',
  //       {
  //         'login': login,
  //         'password': password,
  //       },
  //     );
  //   } on PlatformException catch (e) {
  //     print(e.message);
  //   }
  // }

  // static signOut() {
  //   try {
  //     return platform.invokeMethod('qbSignOut');
  //   } on PlatformException catch (e) {
  //     print(e.message);
  //   }
  // }

  // static connectChatService({@required String password}) {
  //   try {
  //     return platform.invokeMethod('qbConnectChatService', {
  //       'password': password,
  //     });
  //   } on PlatformException catch (e) {
  //     print(e.message);
  //   }
  // }

  static startCall(int opponentId, ConferenceType conferenceType) {
    try {
      return platform.invokeMethod(
        'startCall',
        {
          'opponentId': opponentId,
          'conferenceType': conferenceType == ConferenceType.video ? 1 : 2,
        },
      );
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  static getChatDialogs() {
    try {
      return platform.invokeMethod('qbGetChatDialogs');
    } on PlatformException catch (e) {
      print(e.message);
      return null;
    }
  }

  static deleteChatDialog(int opponentId) {
    try {
      return platform.invokeMethod(
        'qbDeleteChatDialog',
        {'opponentId': opponentId},
      );
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  static qbGetChatDialogHistories(int opponentId) {
    try {
      return platform.invokeMethod(
        'qbGetChatDialogHistories',
        {"opponentId": opponentId},
      );
    } on PlatformException catch (e) {
      print(e.message);
    }

    return null;
  }

  static sendDialogMessage(int opponentId, String text) {
    try {
      return platform.invokeMethod(
        'qbSendDialogMessage',
        {
          'requestCode': randomString(),
          'opponentId': opponentId,
          'text': text,
        },
      );
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  static qbMarkAsRead(String messageId, String dialogId) {
    try {
      return platform.invokeMethod(
        'qbMarkAsRead',
        {
          'messageId': messageId,
          'dialogId': dialogId,
        },
      );
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  static var _onMessageReceivedListeners = List<Function(QbChatMessage)>();
  static addOnMessageReceivedListener(Function(QbChatMessage) onReceive) {
    _onMessageReceivedListeners.add(onReceive);
  }

  static var _onMessageDeliveredListeners =
      List<Function(String messageId, String dialogId, int toUserId)>();
  static addOnMessageDeliveredListener(
      Function(String messageId, String dialogId, int toUserId) onDelivered) {
    _onMessageDeliveredListeners.add(onDelivered);
  }

  static var _onMessageReadedListeners =
      List<Function(String messageId, String dialogId, int readerId)>();
  static addOnMessageReadedListener(
      Function(String messageId, String dialogId, int readerId) onReaded) {
    _onMessageReadedListeners.add(onReaded);
  }

  static var _onMessageSentListeners = List<Function(QbChatMessage)>();
  static addOnMessageSentListener(Function(QbChatMessage) onSent) {
    _onMessageSentListeners.add(onSent);
  }

  static removeListeners(){
    _onMessageReceivedListeners.clear();
    _onMessageDeliveredListeners.clear();
    _onMessageReadedListeners.clear();
    _onMessageSentListeners.clear();
  }
}
