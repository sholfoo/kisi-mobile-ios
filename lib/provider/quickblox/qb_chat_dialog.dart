import 'dart:collection';

class QbChatDialog {
  String dialogId;
  String name;
  int recipientId;
  int unreadMessageCount;
  String lastMessage;
  int lastMessageDateSent;
  int lastMessageUserId;

  QbChatDialog.fromJson(HashMap<String, dynamic> json)
      : dialogId = json['dialogId'],
        name = json['name'],
        recipientId = json['recipientId'],
        unreadMessageCount = json['unreadMessageCount'],
        lastMessage = json['lastMessage'],
        lastMessageDateSent = json['lastMessageDateSent'],
        lastMessageUserId = json['lastMessageUserId'];
}