class BankDto {
  final String bankName;

  BankDto.fromJson(Map<String, dynamic> json)
      : bankName = json['bank_name'];
}
