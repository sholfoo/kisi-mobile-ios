class DukcapilDto {
  bool success;
  String message;
  DukcapilData data;

  DukcapilDto.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        message = json['message'],
        data =
            (json['data'] != null) ? DukcapilData.fromJson(json['data']) : null;
}

class DukcapilData {
  String nationality;
  String placeOfBirth;
  DateTime dateOfBirth;
  String idNumber;
  String identityAddress;
  String clientFullName;
  String gender;
  String identityProvince;
  String identityCity;
  String mothersMaidenName;

  DukcapilData.fromJson(Map<String, dynamic> json)
      : nationality = json['nationality'],
        placeOfBirth = json['place_of_birth'],
        // dateOfBirth = DateTime.parse(json['date_of_birth']),
        idNumber = json['id_number'].toString(),
        identityAddress = json['identity_address'],
        clientFullName = json['client_full_name'],
        gender = json['gender'],
        identityProvince = json['identity_province'],
        identityCity = json['identity_city'],
        mothersMaidenName = json['mothers_maiden_name'];
}
