import 'event_item_dto.dart';

class EventDto {
  final EventItemDto header;
  final List<EventItemDto> event;

  EventDto.fromJson(Map<String, dynamic> json)
      : header = EventItemDto.fromJson(json['header']),
        event = (json['event'] as List)
            .map<EventItemDto>((x) => EventItemDto.fromJson(x))
            .toList();
}