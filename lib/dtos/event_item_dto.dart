class EventItemDto {
  final int id;
  final String title;
  final String content;
  final String image;
  final String slug;
  final DateTime createdAt;

  EventItemDto.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        content = json['content'],
        image = json['image'],
        slug = json['slug'],
        createdAt = DateTime.parse(json['created_at']);
}
