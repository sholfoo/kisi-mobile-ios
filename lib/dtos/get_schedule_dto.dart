import 'dart:collection';

class GetScheduleDto {
  bool success;
  String message;
  GetScheduleData data;

  GetScheduleDto.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        message = json['message'],
        data = json['data'] != null
            ? GetScheduleData.fromJson(HashMap.from(json['data']))
            : null;
}

class GetScheduleData {
  final int id;
  final int userId;
  final String name;
  final String email;
  final String mobilePhoneNumber;
  final String date;
  final String time;
  final String status;
  final String createdAt;
  final String updatedAt;

  GetScheduleData.fromJson(HashMap<String, dynamic> json)
      : id = json['id'],
        userId = json['user_id'],
        name = json['name'],
        email = json['email'],
        mobilePhoneNumber = json['mobile_phone_number'],
        date = json['date'],
        time = json['time'],
        status = json['status'],
        createdAt = json['created_at'],
        updatedAt = json['updated_at'];
}
