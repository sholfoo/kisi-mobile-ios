class ActionDto {
  bool success;
  String message;

  ActionDto.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        message = json['message'];
}