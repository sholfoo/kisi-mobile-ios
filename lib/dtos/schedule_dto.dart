class ScheduleDto {
  final DateTime date;
  final List<ScheduleTimeDto> time;

  ScheduleDto.fromJson(Map<String, dynamic> json)
      : date = DateTime.parse(json['date']),
        time = (json['time'] as List)
            .map<ScheduleTimeDto>((x) => ScheduleTimeDto.fromJson(x))
            .toList();
}

class ScheduleTimeDto {
  final String time;
  final String status;

  ScheduleTimeDto.fromJson(Map<String, dynamic> json)
      : time = json['time'],
        status = json['status'];
}
