
class NewsItemDto {
  final String newsID;
  final DateTime createdDate;
  final String createdBy;
  final String modifiedBy;
  final DateTime modifiedDate;
  final DateTime publishDate;
  final String no;
  final String subject;
  final String title;
  final String source;
  final String content;
  final String image;

  NewsItemDto.fromJson(Map<String, dynamic> json)
      : newsID = json['NewsID'],
        createdDate = DateTime.parse(json['CreatedDate']),
        createdBy = json['CreatedBy'],
        modifiedBy = json['ModifiedBy'],
        modifiedDate = DateTime.parse(json['ModifiedDate']),
        publishDate = DateTime.parse(json['PublishDate']),
        no = json['NO'],
        subject = json['Subject'],
        title = json['Title'],
        source = json['Source'],
        content = json['Content'],
        image = json['image'];
}
