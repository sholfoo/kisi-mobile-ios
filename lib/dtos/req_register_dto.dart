import 'dart:io';

import 'package:dio/dio.dart';
import 'package:intl/intl.dart';

class ReqRegisterDto {
  // Step 1 & 2
  String nationality;
  String nationalityOrigin;

  String clientFullName;
  String gender;
  String placeOfBirth;
  DateTime dateOfBirth;

  // Identity Section
  String idType;
  String idNumber;
  DateTime idExpiredDate;
  String npwp;

  // US Nationality Section
  String usTaxPayer;
  String usGreenCardHolder;
  String bornInUs;
  String usPhoneNumber;
  String usCorrespondentAddress;
  String usTinNumber;

  String educationalBackground;
  String maritalStatus;
  String spouseName;
  String religion;

  // Tahap 3
  String identityAddress;
  String identityProvince;
  String identityCity;
  String identityCountry;
  String identityPostal;

  String currentAddress;
  String othersStreet;
  String othersProvince;
  String othersCity;
  String othersCountry;
  String othersPostal;

  String homeStatus;
  String mothersMaidenName;
  String emailAddress;
  String homePhoneNumber;
  String mobilePhoneNumber;
  String facsimileNumber;

  // Tahap 4
  String occupation;
  String othersOccupation;
  String companyName;
  String lineOfBusiness;
  String position;
  String officePhoneNumber;
  String workFacsimileNumber;
  String workEmailAddress;
  int lengthOfWork;

  String companyAddress;
  String companyProvince;
  String companyCity;
  String companyCountry;
  String companyPostal;

  String annualIncome;
  String sourceOfPrimaryIncome;
  String othersSourceOfPrimaryIncome;
  String sourceOfInvestmentFund;
  String othersSourceOfInvestmentFund;

  // Tahap 5
  String heirName;
  String heirRelationship;
  String heirPhone;
  String heirMobileNumber;

  String emergencyName;
  String emergencyRelation;
  String emergencyPhone;
  String emergencyMobilePhone;
  String emergencyAddress;
  String emergencyProvince;
  String emergencyCity;
  String emergencyCountry;
  String emergencyZip;

  String correspondentAddress;
  String correspondentAddressSpecify;
  String confirmationService;
  String bankName;
  String bankBranch;
  String accountHolderName;
  String typeOfAccount;
  String accountNumber;

  String riskTolerance;
  String timeHorizon;
  String investmentObjective;

  String addInfo_1;
  String addInfo_1Name;
  String addInfo_1Company;
  String addInfo_1Position;
  String addInfo_2;
  String addInfo_2Name;
  String addInfo_2Company;
  String addInfo_2Position;
  String addInfo_3;
  String addInfo_3ShareCompany;
  String addInfo_4;
  String addInfo_4CompanyName;
  String addInfo_5;
  String addInfo_5Position;
  String addInfo_6;
  String addInfo_6Name;
  String addInfo_6Relation;
  String addInfo_6Position;

  // Tahap 6
  String guardiansName;
  String guardiansRelationship;
  String guardiansRelationshipOthers;
  String mobileNumberGuardian;

  String idTypeGuardian;
  String idNumberGuardian;
  DateTime idExpiredDateGuardian;

  String occGuardian;
  String othersOccGuardian;
  String companyNameGuardian;
  String lineOfBusinessGuardian;
  String positionGuardian;

  String companyAddressGuardian;
  String companyProvinceGuardian;
  String companyCityGuardian;
  String companyCountryGuardian;
  String companyZipGuardian;

  String annualIncomeGuardian;
  String sourceOfPrimaryIncomeGuardian;
  String othersSourceOfPrimaryIncomeGuardian;

  // Step 7
  File fileKtp;
  File fileNpwp;
  File fileSwafotoKtp;
  File fileSignature;

  Future<Map<String, dynamic>> toMap() async {
    var dateFormat = DateFormat('dd-MM-yyyy');

    return {
      // Step 1 & 2
      'nationality': nationality,
      'nationality_origin': nationalityOrigin,

      'client_full_name': clientFullName,
      'gender': gender,
      'place_of_birth': placeOfBirth,
      'date_of_birth': dateFormat.format(dateOfBirth),

      // Identity Section
      'id_type': idType,
      'id_number': idNumber,
      'id_expired_date': dateFormat.format(idExpiredDate),
      'npwp': npwp,

      // US Nationality Section
      'us_tax_payer': usTaxPayer,
      'us_green_card_holder': usGreenCardHolder,
      'born_in_us': bornInUs,
      'us_phone_number': usPhoneNumber,
      'us_correspondent_address': usCorrespondentAddress,
      'us_tin_number': usTinNumber,

      'educational_background': educationalBackground,
      'marital_status': maritalStatus,
      'spouse_name': spouseName,
      'religion': religion,

      // Tahap 3
      'identity_address': identityAddress,
      'identity_province': identityProvince,
      'identity_city': identityCity,
      'identity_country': identityCountry,
      'identity_postal': identityPostal,

      'current_address': currentAddress,
      'others_street': othersStreet,
      'others_province': othersProvince,
      'others_city': othersCity,
      'others_country': othersCountry,
      'others_postal': othersPostal,

      'home_status': homeStatus,
      'mothers_maiden_name': mothersMaidenName,
      'email_address': emailAddress,
      'home_phone_number': homePhoneNumber,
      'mobile_phone_number': mobilePhoneNumber,
      'facsimile_number': facsimileNumber,

      // Tahap 4
      'occupation': occupation,
      'others_occupation': othersOccupation,
      'company_name': companyName,
      'line_of_business': lineOfBusiness,
      'position': position,
      'office_phone_number': officePhoneNumber,
      'work_facsimile_number': workFacsimileNumber,
      'work_email_address': workEmailAddress,
      'length_of_work': lengthOfWork.toString(),

      'company_address': companyAddress,
      'company_province': companyProvince,
      'company_city': companyCity,
      'company_country': companyCountry,
      'company_postal': companyPostal,

      'annual_income': annualIncome,
      'source_of_primary_income': sourceOfPrimaryIncome,
      'others_source_of_primary_income': othersSourceOfPrimaryIncome,
      'source_of_investment_fund': sourceOfInvestmentFund,
      'others_source_of_investment_fund': othersSourceOfInvestmentFund,

      // Tahap 5
      'heir_name': heirName,
      'heir_relationship': heirRelationship,
      'heir_phone': heirPhone,
      'heir_mobile_number': heirMobileNumber,

      'emergency_name': emergencyName,
      'emergency_relation': emergencyRelation,
      'emergency_phone': emergencyPhone,
      'emergency_mobile_phone': emergencyMobilePhone,
      'emergency_address': emergencyAddress,
      'emergency_province': emergencyProvince,
      'emergency_city': emergencyCity,
      'emergency_country': emergencyCountry,
      'emergency_zip': emergencyZip,

      'correspondent_address': correspondentAddress,
      'correspondent_address_specify': correspondentAddressSpecify,
      'confirmation_service': confirmationService,
      'bank_name': bankName,
      'bank_branch': bankBranch,
      'account_holder_name': accountHolderName,
      'type_of_account': typeOfAccount,
      'account_number': accountNumber,

      'risk_tolerance': riskTolerance,
      'time_horizon': timeHorizon,
      'investment_objective': investmentObjective,

      'add_info_1': addInfo_1,
      'add_info_1_name': addInfo_1Name,
      'add_info_1_company': addInfo_1Company,
      'add_info_1_position': addInfo_1Position,
      'add_info_2': addInfo_2,
      'add_info_2_name': addInfo_2Name,
      'add_info_2_company': addInfo_2Company,
      'add_info_2_position': addInfo_2Position,
      'add_info_3': addInfo_3,
      'add_info_3_share_company': addInfo_3ShareCompany,
      'add_info_4': addInfo_4,
      'add_info_4_company_name': addInfo_4CompanyName,
      'add_info_5': addInfo_5,
      'add_info_5_position': addInfo_5Position,
      'add_info_6': addInfo_6,
      'add_info_6_name': addInfo_6Name,
      'add_info_6_relation': addInfo_6Relation,
      'add_info_6_position': addInfo_6Position,

      // Tahap 6
      'guardians_name': guardiansName,
      'guardians_relationship': guardiansRelationship,
      'guardians_relationship_others': guardiansRelationshipOthers,
      'mobile_number_guardian': mobileNumberGuardian,

      'id_type_guardian': idTypeGuardian,
      'id_number_guardian': idNumberGuardian,
      'id_expired_date_guardian': dateFormat.format(idExpiredDateGuardian),

      'occ_guardian': occGuardian,
      'others_occ_guardian': othersOccGuardian,
      'company_name_guardian': companyNameGuardian,
      'line_of_business_guardian': lineOfBusinessGuardian,
      'position_guardian': positionGuardian,

      'company_address_guardian': companyAddressGuardian,
      'company_province_guardian': companyProvinceGuardian,
      'company_city_guardian': companyCityGuardian,
      'company_country_guardian': companyCountryGuardian,
      'company_zip_guardian': companyZipGuardian,

      'annual_income_guardian': annualIncomeGuardian,
      'source_of_primary_income_guardian': sourceOfPrimaryIncomeGuardian,
      'others_source_of_primary_income_guardian':
          othersSourceOfPrimaryIncomeGuardian,

      // Step 7
      'file_ktp': MultipartFile.fromBytes(
        await fileKtp.readAsBytes(),
        filename: 'ktp.png',//basename(file_ktp.path),
      ),
      'file_npwp': MultipartFile.fromBytes(
        await fileNpwp.readAsBytes(),
        filename: 'npwp.png',//basename(file_npwp.path),
      ),
      'file_swafoto_ktp': MultipartFile.fromBytes(
        await fileSwafotoKtp.readAsBytes(),
        filename: 'swafoto.png',//basename(file_swafoto_ktp.path),
      ),
      'file_signature': MultipartFile.fromBytes(
        await fileSignature.readAsBytes(),
        filename: 'signature.png',// basename(file_signature.path),
      ),
    };
  }
}
