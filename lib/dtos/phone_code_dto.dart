class PhoneCodeDto {
  final String phonecode;
  final String nicename;

  PhoneCodeDto.fromJson(Map<String, dynamic> json)
      : phonecode = json['phonecode'],
        nicename = json['nicename'];
}
