class LoginDto {
  bool success;
  String message;
  LoginData data;

  LoginDto.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        message = json['message'],
        data = json['data'] != null ? LoginData.fromJson(json['data']) : null;
}

class LoginData {
  String token;
  UserData data;

  LoginData({
    this.token,
    this.data,
  });

  LoginData.fromJson(Map<String, dynamic> json)
      : token = json['token'],
        data = UserData.fromJson(json['data']);
}

class UserData {
  int id;
  int roleId;
  String roleName;
  String name;
  String email;
  int qbId;
  String msisdn;
  String avatar;
  String deviceToken;
  String objectId;
  String qbPass;

  UserData.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        roleId = json['role_id'],
        roleName = json['role_name'],
        name = json['name'],
        email = json['email'],
        qbId = json['qb_id'],
        msisdn = json['msisdn'],
        avatar = json['avatar'],
        deviceToken = json['device_token'],
        objectId = json['object_id'],
        qbPass = json['qb_pass'];

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'role_id': roleId,
      'role_name': roleName,
      'name': name,
      'email': email,
      'qb_id': qbId,
      'msisdn': msisdn,
      'avatar': avatar,
      'device_token': deviceToken,
      'object_id': objectId,
      'qb_pass': qbPass,
    };
  }
}
