class SubmitRegisterDto {
  bool success;
  String message;
  SubmitRegisterData data;

  SubmitRegisterDto.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        message = json['message'],
        data = (json['data'] != null)
            ? SubmitRegisterData.fromJson(json['data'])
            : null;
}

class SubmitRegisterData {
  String idNumber;
  int id;
  String email;
  int userId;

  SubmitRegisterData.fromJson(Map<String, dynamic> json)
      : idNumber = json['id_number'],
        id = json['id'],
        email = json['email'],
        userId = json['user_id'];
}
