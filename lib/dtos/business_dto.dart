class BusinessDto {
  final String businessName;

  BusinessDto.fromJson(Map<String, dynamic> json)
      : businessName = json['business_name'];
}
