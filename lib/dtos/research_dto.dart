class ResearchDto {
  final int id;
  final String title;
  final String documentFile;
  final DateTime date;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String type;

  ResearchDto.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        documentFile = json['document_file'],
        date = DateTime.parse(json['date']),
        createdAt = DateTime.parse(json['created_at']),
        updatedAt = DateTime.parse(json['updated_at']),
        type = json['type'];
}
