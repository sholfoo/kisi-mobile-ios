class SubmitScheduleDto {
  bool success;
  String message;
  SubmitScheduleData data;

  SubmitScheduleDto.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        message = json['message'],
        data = (json['data'] != null)
            ? SubmitScheduleData.fromJson(json['data'])
            : null;
}

class SubmitScheduleData {
  int userId;
  String name;
  String email;
  String mobilePhoneNumber;
  String date;
  String status;
  String time;
  String updatedAt;
  String createdAt;
  int id;

  SubmitScheduleData.fromJson(Map<String, dynamic> json)
      : userId = json['user_id'],
        name = json['name'],
        email = json['email'],
        mobilePhoneNumber = json['mobile_phone_number'],
        date = json['date'],
        status = json['status'],
        time = json['time'],
        updatedAt = json['updated_at'],
        createdAt = json['created_at'],
        id = json['id'];
}
