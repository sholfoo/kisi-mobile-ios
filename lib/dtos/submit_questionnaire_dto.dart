class SubmitQuestionnaireDto {
  bool success;
  String message;
  SubmitQuestionnaireData data;

  SubmitQuestionnaireDto.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        message = json['message'],
        data = (json['data'] != null)
            ? SubmitQuestionnaireData.fromJson(json['data'])
            : null;
}

class SubmitQuestionnaireData {
  int id;
  int userId;
  String formGroup;
  int score;
  String scoreText;

  SubmitQuestionnaireData.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        userId = json['user_id'],
        formGroup = json['form_group'],
        score = json['score'],
        scoreText = json['score_text'];
}
