import 'package:kisi_flutter/dtos/news_item_dto.dart';

class NewsDto {
  final NewsItemDto header;
  final NewsData datas;

  NewsDto.fromJson(Map<String, dynamic> json)
      : header = NewsItemDto.fromJson(json['header']),
        datas = NewsData.fromJson(json['datas']);
}

class NewsData {
  final List<NewsItemDto> marketnews;
  final List<NewsItemDto> stocknews;

  NewsData.fromJson(Map<String, dynamic> json)
      : marketnews = ((json['marketnews'] as List)
            .map<NewsItemDto>((item) => NewsItemDto.fromJson(item))
            .toList()),
        stocknews = ((json['stocknews'] as List)
            .map<NewsItemDto>((item) => NewsItemDto.fromJson(item))
            .toList());
}
