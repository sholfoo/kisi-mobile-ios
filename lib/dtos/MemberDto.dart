class MemberDto {
  final int id;
  final int roleId;
  final String roleName;
  final String name;
  final int qbId;
  final String email;
  final String avatar;
  final String deviceToken;
  final String objectId;

  MemberDto({
    this.id,
    this.roleId,
    this.roleName,
    this.name,
    this.qbId,
    this.email,
    this.avatar,
    this.deviceToken,
    this.objectId,
  });

  MemberDto.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        roleId = json['role_id'],
        roleName = json['role_name'],
        name = json['name'],
        qbId = json['qb_id'],
        email = json['email'],
        avatar = json['avatar'],
        deviceToken = json['device_token'],
        objectId = json['object_id'];
}
