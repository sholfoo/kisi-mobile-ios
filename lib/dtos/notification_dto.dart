class NotificationDto {
  final int id;
  final String target;
  final String title;
  final String caption;
  final String content;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String image;

  NotificationDto.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        target = json['target'],
        title = json['title'],
        caption = json['caption'],
        content = json['content'],
        createdAt = DateTime.parse(json['created_at']),
        updatedAt = DateTime.parse(json['updated_at']),
        image = json['image'];
}
