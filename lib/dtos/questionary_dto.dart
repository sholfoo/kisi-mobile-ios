class QuestionaryDto {
  bool showQuestionnaire;
  List<QuestionaryDataDto> data;

  QuestionaryDto.fromJson(Map<String, dynamic> json)
      : showQuestionnaire = json['show_questionnaire'],
        data = (json['data'] as List)
            .map((x) => QuestionaryDataDto.fromJson(x))
            .toList();
}

class QuestionaryDataDto{
  final int id;
  final String formGroup;
  final String question;
  final String questionEn;
  final List<QuestionaryAnswerDto> answers;

  QuestionaryDataDto.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        formGroup = json['form_group'],
        question = json['question'],
        questionEn = json['question_en'],
        answers = (json['answers'] as List)
            .map((x) => QuestionaryAnswerDto.fromJson(x))
            .toList();
}

class QuestionaryAnswerDto {
  final int id;
  final String answer;
  final int points;
  final String operator;

  QuestionaryAnswerDto.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        answer = json['answer'],
        points = json['points'],
        operator = json['operator'];
}
