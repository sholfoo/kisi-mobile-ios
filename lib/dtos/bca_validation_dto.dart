class BcaValidationDto {
  bool success;
  String message;

  BcaValidationDto.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        message = json['message'];
}