import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kisi_flutter/dtos/news_item_dto.dart';

import 'generated/locale_base.dart';

class JournalDetailScreen extends StatelessWidget {
  final NewsItemDto news;

  JournalDetailScreen(this.news);

  @override
  Widget build(BuildContext context) {
    final loc = Localizations.of<LocaleBase>(context, LocaleBase);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(loc.main.news_detail_title),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: AspectRatio(
                    aspectRatio: 2 / 1,
                    child: Image.network(
                      news.image,
                      height: 1000,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(height: 10),
                Text(
                  news.title,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                Container(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      DateFormat('dd MMM yyyy').format(news.publishDate),
                      style: TextStyle(
                        color: Colors.grey.shade500,
                        fontSize: 13,
                      ),
                    ),
                    Text(
                      news.subject,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        color: Colors.grey.shade500,
                        fontSize: 13,
                      ),
                    ),
                  ],
                ),
                Container(height: 10),
                Text(
                  news.content,
                  textAlign: TextAlign.justify,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
