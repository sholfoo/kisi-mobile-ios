import UIKit
import Flutter
import Quickblox
import QuickbloxWebRTC

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate, QBRTCClientDelegate, FlutterStreamHandler, QBChatDelegate {

    var _eventSink: FlutterEventSink? = nil

    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        _eventSink = events
        return nil
    }

    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        _eventSink = nil
        return nil
    }

    func createAppEvent(type: String, data: Any?) -> [String: Any?] {
        let ev = AppEvent()
        ev.type = type
        ev.data = data
        return ev.toHashMap()
    }

    func chatDidReceive (_ message: QBChatMessage) {
        if (_eventSink != nil) {
            (self._eventSink!)(self.createAppEvent(type: "message_received", data: message.toHashMap()))
        }
    }

    func chatRoomDidReceive(_ message: QBChatMessage, fromDialogID dialogID: String) {
        if (_eventSink != nil) {
            (self._eventSink!)(self.createAppEvent(type: "message_received", data: message.toHashMap()))
        }
    }

    func chatDidDeliverMessage(withID messageID: String, dialogID: String, toUserID userID: UInt) {
        if (_eventSink != nil) {
            var tmp: [String: Any?] = [:]
            tmp["messageId"] = messageID
            tmp["dialogId"] = dialogID
            tmp["toUserId"] = userID
            (self._eventSink!)(self.createAppEvent(type: "message_delivered", data: tmp))
        }
    }

    func chatDidReadMessage(withID messageID: String, dialogID: String, readerID: UInt) {
        if (_eventSink != nil) {
            var tmp: [String: Any?] = [:]
            tmp["messageId"] = messageID
            tmp["dialogId"] = dialogID
            tmp["readerId"] = readerID
            (self._eventSink!)(self.createAppEvent(type: "message_readed", data: tmp))
        }
    }

    var isCalling = false {
        didSet {
            if UIApplication.shared.applicationState == .background,
                isCalling == false {
            }
        }
    }

    var navigationController: UINavigationController? = nil

    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        GeneratedPluginRegistrant.register(with: self)

        let channelName = "qb_channel"
        let rootViewController: FlutterViewController = window?.rootViewController as! FlutterViewController

        navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController!.isNavigationBarHidden = true
        window?.rootViewController = navigationController

        let eventChannel = FlutterEventChannel(name: "qb_event_channel", binaryMessenger: rootViewController.binaryMessenger)
        eventChannel.setStreamHandler(self)

        let methodChannel = FlutterMethodChannel(name: channelName, binaryMessenger: rootViewController.binaryMessenger)

        methodChannel.setMethodCallHandler { (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            let args = call.arguments as? NSDictionary

            if (call.method == "initialize") {
                let appId: String = args?["appId"] as! String
                let authKey: String = args?["authKey"] as! String
                let authSecret: String = args?["authSecret"] as! String
                let accountKey: String = args?["accountKey"] as! String
                self.qbInitialize(result: result, appId: UInt(appId) ?? 0, authKey: authKey, authSecret: authSecret, accountKey: accountKey)
            } else if (call.method == "qbConnect") {
                let login: String = args?["login"] as! String
                let password: String = args?["password"] as! String
                self.qbConnect(result: result, login: login, password: password)
            } else if (call.method == "qbDisconnect") {
                self.qbDisconnect(result: result)
            } else if (call.method == "startCall") {
                let opponentId: UInt = args?["opponentId"] as! UInt
                let conferenceType: UInt = args?["conferenceType"] as! UInt
                self.qbStartCall(result: result, opponentId: opponentId, conferenceType: QBRTCConferenceType.init(rawValue: conferenceType) ?? QBRTCConferenceType.video)
            } else if (call.method == "qbGetChatDialogs") {
                self.qbGetChatDialogs(result: result)
            } else if (call.method == "qbDeleteChatDialog") {
                let opponentId: UInt = args?["opponentId"] as! UInt
                self.qbDeleteChatDialog(result: result, opponentId: opponentId)
            } else if (call.method == "qbGetChatDialogHistories") {
                let opponentId: UInt = args?["opponentId"] as! UInt
                self.qbGetChatDialogHistories(result: result, opponentId: opponentId)
            } else if (call.method == "qbSendDialogMessage") {
                let requestCode: String = args?["requestCode"] as! String
                let opponentId: UInt = args?["opponentId"] as! UInt
                let text: String = args?["text"] as! String
                self.qbSendDialogMessage(result: result, requestCode: requestCode, opponentId: opponentId, text: text)
            } else if (call.method == "qbMarkAsRead") {
                let messageId: String = args?["messageId"] as! String
                let dialogId: String = args?["dialogId"] as! String
                self.qbMarkAsRead(result: result, messageId: messageId, dialogId: dialogId)
            }
        }

        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    override func applicationDidEnterBackground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        // Logging out from chat.
        disconnectChat()
    }

    override func applicationWillEnterForeground(_ application: UIApplication) {
        // Logging in to chat.
        registerForRemoteNotifications()

        let currentUser = QBSession.current.currentUser

        guard currentUser != nil else {
            return
        }

        QBChat.instance
            .connect(withUserID: currentUser?.id ?? 0, password: currentUser? .password ?? "") { (error) in
                print("")
        }
    }

    override func applicationWillTerminate(_ application: UIApplication) {
        // Logging out from chat.
        disconnectChat()
    }

    private func registerForRemoteNotifications() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { granted, error in
            if let error = error {
                debugPrint("[AppDelegate] requestAuthorization error: \(error.localizedDescription)")
                return
            }
            center.getNotificationSettings(completionHandler: { settings in
                if settings.authorizationStatus != .authorized {
                    return
                }
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            })
        })
    }

    private func qbInitialize(result: FlutterResult, appId: UInt, authKey: String, authSecret: String, accountKey: String) {
        // Set QuickBlox credentials
        QBSettings.applicationID = appId
        QBSettings.authKey = authKey
        QBSettings.authSecret = authSecret
        QBSettings.accountKey = accountKey
        // enabling carbons for chat
        QBSettings.carbonsEnabled = true
        // Enables Quickblox REST API calls debug console output.
        QBSettings.logLevel = .debug
        // Enables detailed XMPP logging in console output.
        QBSettings.enableXMPPLogging()

        // Init Chat
        QBChat.instance.addDelegate(self)

        // Init RTC
        QBRTCClient.initializeRTC()
        QBRTCClient.instance().add(self)

        result(nil)
    }

    private func qbIsConected(result: @escaping FlutterResult) {
        result(QBChat.instance.isConnected)
    }

    private func loginToChat(result: @escaping FlutterResult, userId: UInt, password: String) {
        if (QBChat.instance.isConnected) {
            // Return Success
            result(true)
            return
        }

        // Connect to Chat
        QBChat.instance.connect(
            withUserID: userId,
            password: password,
            completion: { (error) in
                if let error = error {
                    // Return Error
                    result(FlutterError(code: "QBError", message: error.localizedDescription, details: nil))
                } else {
                    // Return Success
                    result(true)
                }
            }
        )
    }

    private func qbConnect(result: @escaping FlutterResult, login: String, password: String) {
        if(QBSession.current.currentUser != nil) {
            let userId = QBSession.current.currentUser!.id
            self.loginToChat(result: result, userId: userId, password: password)
            return
        }

        // Login to Quickblox
        QBRequest.logIn(
            withUserLogin: login,
            password: password,
            successBlock: { (response, user) in
                self.loginToChat(result: result, userId: user.id, password: password)
            },
            errorBlock: { (response) in
                result(FlutterError(code: "QBError", message: response.error?.error?.localizedDescription, details: nil))
            }
        )
    }

    func disconnectChat() {
        if(QBChat.instance.isConnected) {
            QBChat.instance.disconnect { (error) in
            }
        }
    }

    private func qbDisconnect(result: @escaping FlutterResult) {
        disconnectChat()

        QBRequest.logOut(
            successBlock: { (response) in
                result(nil)
            },
            errorBlock: { (response) in
                result(FlutterError(code: "QBError", message: response.error?.error?.localizedDescription, details: nil))
            }
        )
    }

    var session: QBRTCSession? = nil
    private func qbStartCall(result: @escaping FlutterResult, opponentId: UInt, conferenceType: QBRTCConferenceType) {
        session = QBRTCClient.instance()
            .createNewSession(withOpponents: [NSNumber(value: opponentId)], with: conferenceType)

        self.callUUID = UUID()
        CallKitManager.instance.startCall(withUserIDs: [NSNumber(value: opponentId)], session: session, uuid: self.callUUID)

        let storyboard = UIStoryboard(name: "CallFlow", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CallFlowController") as! CallFlowController
        vc.setup(opponentId: opponentId, callType: conferenceType, isIncomingCall: false)
        navigationController!.pushViewController(vc, animated: true)

        result(nil)
    }

    var callUUID: UUID? = nil

    var qbDialogs = [QBChatDialog]()
    private func qbGetChatDialogs(result: @escaping FlutterResult) {
        let page = QBResponsePage(limit: 1000, skip: 0)
        QBRequest.dialogs(
            for: page,
            extendedRequest: nil,
            successBlock: { (response, dialogs, dialogUserIds, page) in
                self.qbDialogs = dialogs
                let a = self.qbDialogs.map { (item) -> [String: Any?] in
                    return item.toHashMap()
                }
                result(a)
            }, errorBlock: { (response) in
                result(FlutterError(code: "QBError", message: response.error?.error?.localizedDescription, details: nil))
            }
        )
    }

    private func qbDeleteChatDialog(result: @escaping FlutterResult, opponentId: UInt) {
        let dialog = qbDialogs.first(where: { (x) -> Bool in
            return x.recipientID == opponentId
        })

        if (dialog == nil) {
            result(nil)
            return
        }

        QBRequest.deleteDialogs(withIDs: [dialog?.id ?? ""], forAllUsers: true, successBlock: { (response, _, _, _) in
            result(nil)
        }, errorBlock: { (response) in
            result(FlutterError(code: "QBError", message: response.error?.error?.localizedDescription, details: nil))
        })
    }

    private func qbSendDialogMessage(result: @escaping FlutterResult, requestCode: String, opponentId: UInt, text: String) {
        let message = QBChatMessage()
        message.text = text
        message.recipientID = opponentId
        message.deliveredIDs = []
        message.readIDs = []
        message.markable = true

        let params: NSMutableDictionary = NSMutableDictionary()
        params["save_to_history"] = true
        message.customParameters = params

        var tmp = message.toHashMap()
        tmp["requestCode"] = requestCode
        result(tmp)

        QBRequest.send(
            message,
            successBlock: { (response, message) in
                // result(message.toHashMap())
                if(self._eventSink != nil) {
                    var tmp = message.toHashMap()
                    tmp["requestCode"] = requestCode
                    (self._eventSink!)(self.createAppEvent(type: "message_sent", data: tmp))
                }
            },
            errorBlock: { (response) in
                // result(FlutterError(code: "QBError", message: response.error?.error?.localizedDescription, details: nil))
            }
        )
    }

    private func qbMarkAsRead(result: @escaping FlutterResult, messageId: String, dialogId: String) {
        let page = QBResponsePage(limit: 1, skip: 0)
        QBRequest.messages(
            withDialogID: dialogId,
            extendedRequest: ["_id": messageId],
            for: page,
            successBlock: { (response, messages, page) in
                if(!messages.isEmpty) {
                    QBChat.instance.read(messages.first!) { (error) in
                        print(error?.localizedDescription ?? "no error")
                    }

                }
            },
            errorBlock: { (response) in
                // result(FlutterError(code: "QBError", message: response.error?.error?.localizedDescription, details: nil))
            }
        )

        result(nil)
    }

    private func qbGetChatDialogHistories(result: @escaping FlutterResult, opponentId: UInt) {
        let dialog = qbDialogs.first(where: { (x) -> Bool in
            return x.recipientID == opponentId
        })

        if (dialog == nil) {
            result([[:]])
            return
        }

        let page = QBResponsePage(limit: 1000, skip: 0)
        QBRequest.messages(
            withDialogID: dialog?.id ?? "",
            extendedRequest: [:],
            for: page,
            successBlock: { (response, messages, page) in
                let tmp = messages.map({ (message) -> [String: Any?] in
                    return message.toHashMap()
                })
                if(!messages.isEmpty) {
                    QBChat.instance.read(messages.last!) { (error) in
                    }
                }
                result(tmp)
            },
            errorBlock: { (response) in
                result(FlutterError(code: "QBError", message: response.error?.error?.localizedDescription, details: nil))
            }
        )
    }

    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String: String]? = nil) {
        if self.session != nil {
            let userInfo: [String: String] = [:]
            session.rejectCall(userInfo)
        }
        else {
            self.session = session

            let page = QBGeneralResponsePage(currentPage: 1, perPage: 100)
            page.perPage = 1000

            self.callUUID = UUID()
            QBRequest.users(
                withIDs: [session.initiatorID.stringValue],
                page: page,
                successBlock: { (response, page, users) in
                    let txt = users.count > 0 ? users.first!.fullName : "Unknown-"
                    self.reportIncomingCall(withUserIDs: [session.initiatorID], outCallerName: txt!, session: session, uuid: self.callUUID!)
                },
                errorBlock: { (response) in
                    let txt = "Unknown"
                    self.reportIncomingCall(withUserIDs: [session.initiatorID], outCallerName: txt, session: session, uuid: self.callUUID!)
                }
            )
        }
    }

    func sessionDidClose(_ session: QBRTCSession) {
        CallKitManager.instance.endCall(with: callUUID)

        self.session = nil
    }

    private func reportIncomingCall(withUserIDs userIDs: [NSNumber], outCallerName: String, session: QBRTCSession, uuid: UUID) {
        CallKitManager.instance
            .reportIncomingCall(
                withUserIDs: userIDs,
                outCallerName: outCallerName,
                session: session,
                uuid: uuid,
                onAcceptAction: { [weak self] in
                    guard let self = self else {
                        return
                    }

                    let storyboard = UIStoryboard(name: "CallFlow", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "CallFlowController") as! CallFlowController
                    vc.setup(opponentId: UInt(session.initiatorID.intValue), callType: session.conferenceType, isIncomingCall: true)
                    self.navigationController!.pushViewController(vc, animated: true)
                }, completion: { (end) in
                    debugPrint("[UsersViewController] endCall")
                })
    }

    override func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        guard let identifierForVendor = UIDevice.current.identifierForVendor else {
            return
        }

        let deviceIdentifier = identifierForVendor.uuidString
        let subscription = QBMSubscription()
        subscription.notificationChannel = .APNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        QBRequest.createSubscription(subscription, successBlock: { response, objects in
        }, errorBlock: { response in
            debugPrint("[AppDelegate] createSubscription error: \(String(describing: response.error))")
        })
    }
}

extension QBChatDialog {
    func toHashMap() -> [String: Any?] {
        var tmp: [String: Any?] = [:]
        tmp["dialogId"] = self.id
        tmp["name"] = self.name
        tmp["recipientId"] = self.recipientID
        tmp["unreadMessageCount"] = self.unreadMessagesCount
        tmp["lastMessage"] = self.lastMessageText
        tmp["lastMessageDateSent"] = self.lastMessageDate?.secondsSince1970
        tmp["lastMessageUserId"] = self.lastMessageUserID

        return tmp
    }
}

extension QBChatMessage {
    func toHashMap() -> [String: Any?] {
        var tmp: [String: Any?] = [:]
        tmp["id"] = self.id
        tmp["dialogId"] = self.dialogID
        tmp["recipientId"] = self.recipientID
        tmp["senderId"] = self.senderID
        tmp["body"] = self.text
        tmp["dateSent"] = self.dateSent?.secondsSince1970
        tmp["readIds"] = self.readIDs
        tmp["deliveredIds"] = self.deliveredIDs

        return tmp
    }
}

extension Date {
    var millisecondsSince1970: Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    var secondsSince1970: Int64 {
        return Int64(self.timeIntervalSince1970.rounded())
    }

    init(milliseconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

class AppEvent {
    var type: String = ""
    var data: Any?
}

extension AppEvent {
    func toHashMap() -> [String: Any?] {
        var tmp: [String: Any?] = [:]
        tmp["type"] = self.type
        tmp["data"] = self.data
        return tmp
    }
}
