//
//  CallFlowControllerViewController.swift
//  Runner
//
//  Created by Riad on 18/11/19.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import UIKit
import Quickblox
import QuickbloxWebRTC

struct CallStateConstant {
    static let disconnected = "Disconnected"
    static let connecting = "Connecting..."
    static let connected = "Connected"
    static let disconnecting = "Disconnecting..."
}

struct CallConstant {
    static let opponentCollectionViewCellIdentifier = "OpponentCollectionViewCellIdentifier"
    static let unknownUserLabel = "Unknown user"
    static let sharingViewControllerIdentifier = "SharingViewController"
    static let refreshTimeInterval: TimeInterval = 1.0

    static let memoryWarning = NSLocalizedString("MEMORY WARNING: leaving out of call. Please, reduce the quality of the video settings", comment: "")
    static let sessionDidClose = NSLocalizedString("Session did close due to time out", comment: "")
}

class CallFlowController: UIViewController, QBRTCClientDelegate {

    @IBOutlet weak var remoteVideoContainer: UIView!
    @IBOutlet weak var remoteVideo: QBRTCRemoteVideoView!
    @IBOutlet weak var localVideo: UIView!

    @IBOutlet weak var i1Container: UIView!
    @IBOutlet weak var i1OpponentName: UILabel!
    @IBOutlet weak var i1Timer: UILabel!

    @IBOutlet weak var i2Container: UIView!
    @IBOutlet weak var i2OpponentName: UILabel!
    @IBOutlet weak var i2Timer: UILabel!

    @IBOutlet weak var rejectButton: RoundButton!
    @IBOutlet weak var hangUpButton: RoundButton!
    @IBOutlet weak var acceptButton: RoundButton!

    private var opponentId: UInt = 0
    private var callType: QBRTCConferenceType = .video
    private var isIncomingCall: Bool = false

    private var timeDuration: TimeInterval = 0.0

    private var callTimer: Timer?
    private var beepTimer: Timer?

    private var isVideo: Bool {
        get {
            return callType == .video
        }
    }

    private var videoCapture: QBRTCCameraCapture?
    private var isWaiting: Bool = true
    private var callCounter: UInt = 0

    public func setup(opponentId: UInt, callType: QBRTCConferenceType, isIncomingCall: Bool) {
        self.opponentId = opponentId
        self.callType = callType
        self.isIncomingCall = isIncomingCall
    }

    private var session: QBRTCSession? {
        get {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                return appDelegate.session ?? nil
            }
            return nil
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        QBRTCClient.instance().add(self)
        initAudio()
        
        if(!isIncomingCall) {
            startRingtone()
            playCallingSound(nil)

            session?.startCall([:])
        }
        else {
            session?.acceptCall([:])
            isWaiting = false
        }

        fetchOpponentInfo()

        updateView()
    }

    override func viewDidDisappear(_ animated: Bool) {
        stopRingtone()
    }

    private func initAudio() {
        //Save current audio configuration before start call or accept call
        QBRTCAudioSession.instance().initialize()
        if (isVideo) {
            //Set speaker
            QBRTCAudioSession.instance().currentAudioDevice = .speaker
        }
        else {
            //or headphone or phone receiver
            QBRTCAudioSession.instance().currentAudioDevice = .receiver
        }

        self.session!.localMediaStream.audioTrack.isEnabled = true
    }

    private func updateView() {
        self.remoteVideoContainer.isHidden = true
        self.localVideo.isHidden = true
        self.i1Container.isHidden = true
        self.i2Container.isHidden = true
        self.rejectButton.isHidden = true
        self.hangUpButton.isHidden = true
        self.acceptButton.isHidden = true

        if(isWaiting) {
            self.i2Container.isHidden = false
            if(isIncomingCall) {
                self.rejectButton.isHidden = false
                self.acceptButton.isHidden = false
            } else {
                self.hangUpButton.isHidden = false
            }
        } else {
            self.hangUpButton.isHidden = false
            if(isVideo) {
                self.i1Container.isHidden = false
                self.remoteVideoContainer.isHidden = false
                self.localVideo.isHidden = false
                initLocalViedeo()
            } else {
                self.i2Container.isHidden = false
            }
        }

        self.view.layoutIfNeeded()

        // Refresh timer
        refreshCallTime(nil)
    }

    @IBAction func rejectButtonAction(_ sender: Any) {
        session?.rejectCall([:])
    }

    @IBAction func hangUpButtonAction(_ sender: Any) {
        session?.hangUp([:])
    }

    @IBAction func acceptButtonAction(_ sender: Any) {
        stopRingtone()
        session?.acceptCall([:])
    }

    private func startRingtone() {
        //Begin play calling sound
        beepTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(),
                                         target: self,
                                         selector: #selector(playCallingSound(_:)),
                                         userInfo: nil, repeats: true)
    }

    private func stopRingtone() {
        if let beepTimer = beepTimer {
            beepTimer.invalidate()
            self.beepTimer = nil
            SoundProvider.stopSound()
        }

    }

    private var localVideoInitialized = false
    private func initLocalViedeo() {
        if(localVideoInitialized) { return }

        #if targetEnvironment(simulator)
            // Simulator
        #else
            let videoFormat = QBRTCVideoFormat.init()
//            videoFormat.frameRate = 30
            videoFormat.pixelFormat = QBRTCPixelFormat.format420f
//            videoFormat.width = 640
//            videoFormat.height = 480

            // QBRTCCameraCapture class used to capture frames using AVFoundation APIs
            self.videoCapture = QBRTCCameraCapture.init(videoFormat: videoFormat, position: AVCaptureDevice.Position.front)

            // add video capture to session's local media stream
            // from version 2.3 you no longer need to wait for 'initializedLocalMediaStream:' delegate to do it
            self.session!.localMediaStream.videoTrack.videoCapture = self.videoCapture

            self.videoCapture!.previewLayer.frame = self.localVideo.bounds
            self.videoCapture!.startSession()

            self.localVideo.layer.insertSublayer(self.videoCapture!.previewLayer, at: 0)

            localVideoInitialized = true
        #endif
    }


    func session(_ session: QBRTCSession, userDidNotRespond userID: NSNumber) {
        stopRingtone()
    }

    func session(_ session: QBRTCBaseSession, connectedToUser userID: NSNumber) {
        isWaiting = false
        callCounter = 0
        updateView()

//        beepTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(),
//                                         target: self,
//                                         selector: #selector(playCallingSound(_:)),
//                                         userInfo: nil, repeats: true)
        stopRingtone()

        if callTimer == nil {
            callTimer = Timer.scheduledTimer(
                timeInterval: CallConstant.refreshTimeInterval,
                target: self,
                selector: #selector(refreshCallTime(_:)),
                userInfo: nil,
                repeats: true
            )
        }
    }

    @objc func playCallingSound(_ sender: Any?) {
        SoundProvider.playSound(type: isIncomingCall ? SoundType.ringtone : SoundType.calling)
    }

    @objc func refreshCallTime(_ sender: Timer?) {
        timeDuration += CallConstant.refreshTimeInterval
        if(isWaiting) {
            let callDirectionText = (isIncomingCall) ? "Incoming" : "Outgoing"
            let callTypeText = (isVideo) ? "Video Call" : "Audio Call"
            let txt = "\(callDirectionText) \(callTypeText)\((isIncomingCall) ? "" : "...")"
            i1Timer.text = txt
            i2Timer.text = txt

        } else {
            let txt = "Call time - \(string(withTimeDuration: timeDuration))"
            i1Timer.text = txt
            i2Timer.text = txt
        }
    }

    func string(withTimeDuration timeDuration: TimeInterval) -> String {
        let hours = Int(timeDuration / 3600)
        let minutes = Int(timeDuration / 60)
        let seconds = Int(timeDuration) % 60

        var timeStr = ""
        if hours > 0 {
            let minutes = Int((timeDuration - Double(3600 * hours)) / 60)
            timeStr = "\(hours):\(minutes):\(seconds)"
        } else {
            if (seconds < 10) {
                timeStr = "\(minutes):0\(seconds)"
            } else {
                timeStr = "\(minutes):\(seconds)"
            }
        }
        return timeStr
    }

    private func fetchOpponentInfo() {
        let txt = "..."
        self.i1OpponentName.text = txt
        self.i2OpponentName.text = txt

        let page = QBGeneralResponsePage(currentPage: 1, perPage: 100)
        page.perPage = 1000

        QBRequest.users(
            withIDs: [String(opponentId)],
            page: page,
            successBlock: { (response, page, users) in
                let txt = users.count > 0 ? users.first!.fullName : "Unknown-"
                self.i1OpponentName.text = txt
                self.i2OpponentName.text = txt
            },
            errorBlock: { (response) in
                let txt = "Unknown"
                self.i1OpponentName.text = txt
                self.i2OpponentName.text = txt
            }
        )
    }

    func session(_ session: QBRTCSession, acceptedByUser userID: NSNumber, userInfo: [String: String]? = nil) {
        // Panggilan diterima oleh opponent
        stopRingtone()
        isWaiting = false
        callCounter = 0
        updateView()
    }

    func session(_ session: QBRTCSession, rejectedByUser userID: NSNumber, userInfo: [String: String]? = nil) {
        stopRingtone()
    }

    func session(_ session: QBRTCBaseSession, receivedRemoteVideoTrack videoTrack: QBRTCVideoTrack, fromUser userID: NSNumber) {
        self.remoteVideo.setVideoTrack(videoTrack)
    }

    func session(_ session: QBRTCBaseSession, receivedRemoteAudioTrack audioTrack: QBRTCAudioTrack, fromUser userID: NSNumber) {
        audioTrack.isEnabled = true
    }

    func sessionDidClose(_ session: QBRTCSession) {
        stopRingtone()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        CallKitManager.instance.endCall(with: appDelegate.callUUID)

        //deinitialize after session close
        videoCapture?.stopSession()
        QBRTCAudioSession.instance().deinitialize()

        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

}
