package com.example.kisi_flutter.qb

import android.util.Log
import com.example.kisi_flutter.Application
import com.quickblox.videochat.webrtc.QBRTCSession
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientSessionCallbacksImpl

object RtcSessionManager : QBRTCClientSessionCallbacksImpl() {
    private val TAG = RtcSessionManager::class.java.simpleName

    private var currentSession: QBRTCSession? = null

    fun getCurrentSession(): QBRTCSession? {
        return currentSession
    }

    fun setCurrentSession(qbCurrentSession: QBRTCSession?) {
        currentSession = qbCurrentSession
    }

    override fun onReceiveNewSession(session: QBRTCSession?) {
        Log.d(TAG, "onReceiveNewSession to WebRtcSessionManager")

        if (currentSession == null) {
            setCurrentSession(session)
            CallActivity.incomingCall(Application.getInstance())
        }
    }

    override fun onSessionClosed(session: QBRTCSession?) {
        Log.d(TAG, "onSessionClosed WebRtcSessionManager")

        if (session == getCurrentSession()) setCurrentSession(null)
    }
}