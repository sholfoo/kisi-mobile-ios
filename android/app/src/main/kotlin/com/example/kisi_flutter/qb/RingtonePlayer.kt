package com.example.kisi_flutter.qb

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import java.io.IOException

class RingtonePlayer(context: Context, resource: Int) {

    private var tag = RingtonePlayer::class.java.simpleName

    private var mediaPlayer: MediaPlayer? = null

    init {
        val beepUri = Uri.parse("android.resource://" + context.packageName + "/" + resource)
        mediaPlayer = MediaPlayer()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            val audioAttributesBuilder = AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION_SIGNALLING)

            mediaPlayer?.setAudioAttributes(audioAttributesBuilder.build())
        } else {
            mediaPlayer?.setAudioStreamType(AudioManager.STREAM_VOICE_CALL)
        }
        try {
            mediaPlayer?.setDataSource(context, beepUri)
            mediaPlayer?.prepare()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun play(looping: Boolean) {
        Log.i(tag, "play")
        mediaPlayer?.let {
            it.isLooping = looping
            it.start()
        } ?: run {
            Log.i(tag, "mediaPlayer isn't created ")
        }
    }

    @Synchronized
    fun stop() {
        mediaPlayer?.let {
            try {
                it.stop()
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }
            it.release()
            mediaPlayer = null
        }
    }
}