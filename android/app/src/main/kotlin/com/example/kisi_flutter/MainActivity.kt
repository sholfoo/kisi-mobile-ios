package com.example.kisi_flutter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.example.kisi_flutter.qb.*
import com.quickblox.auth.session.QBSessionManager
import com.quickblox.auth.session.QBSettings
import com.quickblox.chat.QBChatService
import com.quickblox.chat.QBRestChatService
import com.quickblox.chat.exception.QBChatException
import com.quickblox.chat.listeners.QBChatDialogMessageListener
import com.quickblox.chat.model.QBChatDialog
import com.quickblox.chat.model.QBChatMessage
import com.quickblox.chat.model.QBDialogType
import com.quickblox.chat.request.QBMessageGetBuilder
import com.quickblox.core.QBEntityCallback
import com.quickblox.core.exception.QBResponseException
import com.quickblox.core.request.QBRequestGetBuilder
import com.quickblox.users.QBUsers
import com.quickblox.users.model.QBUser
import com.quickblox.videochat.webrtc.QBRTCClient
import com.quickblox.videochat.webrtc.QBRTCTypes
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity : FlutterActivity(), EventChannel.StreamHandler {

    private var _rtcClient: QBRTCClient? = null

    private var _eventSink: EventChannel.EventSink? = null

    override fun onListen(p0: Any?, p1: EventChannel.EventSink?) {
        _eventSink = p1
    }

    override fun onCancel(p0: Any?) {
        _eventSink = null
    }

    private lateinit var mcResult: MethodChannel.Result

    private val context: Context = this
    private lateinit var qbChannel: MethodChannel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)

        EventChannel(flutterView, "qb_event_channel").setStreamHandler(this)

        qbChannel = MethodChannel(flutterView, "qb_channel")
        qbChannel.setMethodCallHandler { call, result ->
            mcResult = result
            when (call.method) {
                "initialize" -> {
                    val appId = call.argument<String>("appId")
                    val authKey = call.argument<String>("authKey")
                    val authSecret = call.argument<String>("authSecret")
                    val accountKey = call.argument<String>("accountKey")
                    qbInitialize(result, appId!!, authKey!!, authSecret!!, accountKey!!)
                }
                "qbConnect" -> {
                    val login = call.argument<String>("login")
                    val password = call.argument<String>("password")
                    qbConnect(result, login, password)
                }
                "qbDisconnect" -> {
                    qbDisconnect(result)
                }
//                "qbSignIn" -> {
//                    val login = call.argument<String>("login")
//                    val password = call.argument<String>("password")
//                    qbSignIn(result, login, password)
//                }
//                "qbSignOut" -> {
//                    qbSignOut(result)
//                }
//                "qbConnectChatService" -> {
//                    qbConnectChat()
//                }
                "startCall" -> {
                    val opponentId = call.argument<Int>("opponentId")
                    val conferenceType = call.argument<Int>("conferenceType")
                    startCall(result, opponentId!!, QBRTCTypes.QBConferenceType.values().first { x -> x.value == conferenceType!! })
                }
                "qbGetChatDialogs" -> {
                    qbGetChatDialogs(result)
                }
                "qbDeleteChatDialog" -> {
                    val opponentId = call.argument<Int>("opponentId")
                    qbDeleteChatDialog(result, opponentId!!)
                }
                "qbGetChatDialogHistories" -> {
                    val opponentId = call.argument<Int>("opponentId")
                    qbGetChatDialogHistories(result, opponentId!!)
                }
                "qbSendDialogMessage" -> {
                    val opponentId = call.argument<Int>("opponentId")
                    val text = call.argument<String>("text")
                    qbSendDialogMessage(result, opponentId!!, text!!)
                }
            }
        }
    }

    private fun qbInitialize(result: MethodChannel.Result, appId: String, authKey: String, authSecret: String, accountKey: String) {
        QBSettings.getInstance().init(applicationContext, appId, authKey, authSecret)
        QBSettings.getInstance().accountKey = accountKey

        result.success(null)
    }

    private fun loginToChat(result: MethodChannel.Result, login: String?, password: String?) {
        val qbUser = QBUser(login, password)
        qbUser.id = QBSessionManager.getInstance().sessionParameters.userId
        QBChatService.getInstance().login(qbUser, object : QBEntityCallback<QBUser> {
            override fun onSuccess(qbUser: QBUser?, bundle: Bundle) {
                _rtcClient = QBRTCClient.getInstance(context)

                // Add signalling manager
                QBChatService.getInstance().videoChatWebRTCSignalingManager
                        .addSignalingManagerListener { qbSignaling, createdLocally ->
                            if (!createdLocally) {
                                _rtcClient?.addSignaling(qbSignaling)
                            }
                        }

                // Add service as callback to RTCClient
                _rtcClient?.addSessionCallbacksListener(RtcSessionManager)
                _rtcClient?.prepareToProcessCalls()

                result.success(true)
            }

            override fun onError(e: QBResponseException) {
                val errorMessage = if (e.message != null) e.message else "Login error"
                result.error("QBResponseException", errorMessage, null)
            }
        })
    }

    private fun qbConnect(result: MethodChannel.Result, login: String?, password: String?) {
        if (QBSessionManager.getInstance().sessionParameters != null) {
            loginToChat(result, login, password)
            return
        }

        // Login
        val qbUser = QBUser(login, password)
        QBUsers.signIn(qbUser).performAsync(object : QBEntityCallback<QBUser> {
            override fun onSuccess(user: QBUser, args: Bundle) {
                loginToChat(result, login, password)
            }

            override fun onError(error: QBResponseException) {
                result.error(error.message, error.message, error.message)
            }
        })
    }

    private fun qbDisconnect(result: MethodChannel.Result) {
        _rtcClient?.destroy()
        ChatLoginService.logout(this)
        QBUsers.signOut().performAsync(object : QBEntityCallback<Void> {
            override fun onSuccess(p0: Void?, p1: Bundle?) {
                result.success(null)
            }

            override fun onError(ex: QBResponseException) {
                result.error("QBResponseException", ex.message, null)
            }
        })
    }

//    private fun qbSignIn(result: MethodChannel.Result, login: String?, password: String?) {
//        if (QBSessionManager.getInstance().sessionParameters != null) {
//            mcResult.success(QBSessionManager.getInstance().sessionParameters.userId)
//            return
//        }
//
//        val user = QBUser(login, password)
//        // Login
//        QBUsers.signIn(user).performAsync(object : QBEntityCallback<QBUser> {
//            override fun onSuccess(user: QBUser, args: Bundle) {
//                result.success(user.id)
//            }
//
//            override fun onError(error: QBResponseException) {
//                result.error(error.message, error.message, error.message)
//            }
//        })
//    }

//    private fun qbSignOut(result: MethodChannel.Result) {
//        ChatLoginService.logout(this)
//        QBUsers.signOut().performAsync(object : QBEntityCallback<Void> {
//            override fun onSuccess(p0: Void?, p1: Bundle?) {
//                result.success(null)
//            }
//
//            override fun onError(ex: QBResponseException) {
//                result.error("QBResponseException", ex.message, null)
//            }
//        })
//    }

//    private fun qbConnectChat() {
//        val userSession = QBSessionManager.getInstance().sessionParameters
//        val qbUser = QBUser(userSession.userLogin, userSession.userPassword)
//        qbUser.id = userSession.userId
//
//        val tempIntent = Intent(this, ChatLoginService::class.java)
//        val pendingIntent = createPendingResult(EXTRA_LOGIN_RESULT_CODE, tempIntent, 0)
//        ChatLoginService.start(this, qbUser, pendingIntent)
//    }

    private fun startCall(result: MethodChannel.Result, opponentId: Int, conferenceType: QBRTCTypes.QBConferenceType = QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO) {
        val opponentsList = listOf(opponentId)

        val newQbRtcSession =
                _rtcClient?.createNewSessionWithOpponents(opponentsList, conferenceType)

        RtcSessionManager.setCurrentSession(newQbRtcSession)

        CallActivity.outgoingCall(context, opponentId)

        result.success(null)
    }

    val qbDialogs = ArrayList<QBChatDialog>()
    private fun qbGetChatDialogs(result: MethodChannel.Result) {
        qbChannel.invokeMethod("qbReceivedMessage", null)

        val requestBuilder = QBRequestGetBuilder()
        requestBuilder.limit = 1000

        QBRestChatService.getChatDialogs(QBDialogType.PRIVATE, requestBuilder)
                .performAsync(object : QBEntityCallback<ArrayList<QBChatDialog>> {
                    override fun onSuccess(dialogs: ArrayList<QBChatDialog>, args: Bundle) {
                        qbDialogs.clear()
                        qbDialogs.addAll(dialogs)
                        result.success(dialogs.map { x -> x.toHashMap() })
                    }

                    override fun onError(ex: QBResponseException) {
                        result.error("QBResponseException", ex.message, null)
                    }
                })
    }

    private fun qbDeleteChatDialog(result: MethodChannel.Result, opponentId: Int) {
        val dialog = qbDialogs.firstOrNull { x -> x.recipientId == opponentId }
        if (dialog == null) {
            result.success(null)
            return
        }

        QBRestChatService.deleteDialog(dialog.dialogId, true)
                .performAsync(object : QBEntityCallback<Void> {
                    override fun onSuccess(v: Void?, args: Bundle?) {
                        result.success(null)
                    }

                    override fun onError(ex: QBResponseException) {
                        result.error("QBResponseException", ex.message, null)
                    }
                })
    }

    private fun qbSendDialogMessage(result: MethodChannel.Result, opponentId: Int, text: String) {
        val message = QBChatMessage()
        message.setSaveToHistory(true)
        message.body = text
        message.recipientId = opponentId

        QBRestChatService.createMessage(message, true)
                .performAsync(object : QBEntityCallback<QBChatMessage> {
                    override fun onSuccess(message: QBChatMessage, args: Bundle?) {
                        result.success(message.toHashMap())
                    }

                    override fun onError(ex: QBResponseException) {
                        result.error("QBResponseException", ex.message, null)
                    }
                })
    }

    private fun qbGetChatDialogHistories(result: MethodChannel.Result, opponentId: Int) {
        val dialog = qbDialogs.firstOrNull { x -> x.recipientId == opponentId }
        if (dialog == null) {
            result.success(arrayListOf<HashMap<String, Any?>>())
            return
        }

        val requestBuilder = QBMessageGetBuilder()
        requestBuilder.limit = 1000

        QBRestChatService.getDialogMessages(dialog, requestBuilder)
                .performAsync(object : QBEntityCallback<ArrayList<QBChatMessage>> {
                    override fun onSuccess(messages: ArrayList<QBChatMessage>, args: Bundle) {
                        result.success(messages.map { x -> x.toHashMap() })
                    }

                    override fun onError(ex: QBResponseException) {
                        result.error("QBResponseException", ex.message, null)
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == EXTRA_LOGIN_RESULT_CODE) {
            var isLoginSuccess = false
            data?.let {
                isLoginSuccess = it.getBooleanExtra(EXTRA_LOGIN_RESULT, false)
            }

            var errorMessage: String? = getString(R.string.unknown_error)
            data?.let {
                errorMessage = it.getStringExtra(EXTRA_LOGIN_ERROR_MESSAGE)
            }

            if (isLoginSuccess) {
                mcResult.success(null)
            } else {
                mcResult.error("ChatServiceLoginError", errorMessage, null)
            }

            val incomingMessagesManager = QBChatService.getInstance().incomingMessagesManager
            incomingMessagesManager.addDialogMessageListener(object : QBChatDialogMessageListener {
                override fun processMessage(dialogId: String, message: QBChatMessage, senderId: Int) {
                    Log.d("QBChatMessageListener", "Process dialogId : $dialogId")
                    Log.d("QBChatMessageListener", "Process message : ${message.body}")
                    Log.d("QBChatMessageListener", "Process senderId : $senderId")

                    _eventSink?.success(message.toHashMap())

                    // runOnUiThread {
                    //     qbChannel.invokeMethod("qbReceivedMessage", message.toHashMap())
                    // }
                }

                override fun processError(dialogId: String, ex: QBChatException, message: QBChatMessage, senderId: Int) {
                    Log.d("QBChatMessageListener", "Process dialogId : $dialogId")
                    Log.d("QBChatMessageListener", "Process exception : ${ex.message}")
                    Log.d("QBChatMessageListener", "Process message : ${message.body}")
                    Log.d("QBChatMessageListener", "Process senderId : $senderId")
                }
            })
        }
    }

    fun QBChatDialog.toHashMap(): HashMap<String, Any?> {
        val tmp = HashMap<String, Any?>()
        tmp["dialogId"] = this.dialogId
        tmp["name"] = this.name
        tmp["recipientId"] = this.recipientId // this.occupants.firstOrNull { x -> x != this.userId } ?: 0
        tmp["unreadMessageCount"] = this.unreadMessageCount
        tmp["lastMessage"] = this.lastMessage
        tmp["lastMessageDateSent"] = this.lastMessageDateSent
        tmp["lastMessageUserId"] = this.lastMessageUserId

        return tmp
    }

    fun QBChatMessage.toHashMap(): HashMap<String, Any?> {
        val tmp = HashMap<String, Any?>()
        tmp["id"] = this.id
        tmp["dialogId"] = this.dialogId
        tmp["recipientId"] = this.recipientId
        tmp["senderId"] = this.senderId
        tmp["body"] = this.body
        tmp["dateSent"] = this.dateSent
        tmp["readIds"] = this.readIds

        return tmp
    }
}
