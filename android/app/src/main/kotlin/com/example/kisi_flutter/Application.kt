package com.example.kisi_flutter

import android.content.Context
import com.quickblox.auth.session.QBSettings
import com.quickblox.chat.QBChatService
import com.quickblox.chat.QBSignaling
import com.quickblox.chat.QBWebRTCSignaling
import com.quickblox.chat.listeners.QBVideoChatSignalingManagerListener
import com.quickblox.videochat.webrtc.QBRTCClient
import io.flutter.app.FlutterApplication
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugins.GeneratedPluginRegistrant
import io.flutter.plugins.firebasemessaging.FlutterFirebaseMessagingService

class Application : FlutterApplication(), PluginRegistry.PluginRegistrantCallback {
    companion object {
        private lateinit var instance: Application

        @Synchronized
        fun getInstance(): Application = instance
    }

    private val context: Context = this

    override fun onCreate() {
        super.onCreate()
        instance = this
        FlutterFirebaseMessagingService.setPluginRegistrant(this)
    }

    override fun registerWith(registry: PluginRegistry) {
        GeneratedPluginRegistrant.registerWith(registry)
    }
}