package com.example.kisi_flutter.qb

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import android.util.Log
import android.view.View
import com.example.kisi_flutter.R
import com.quickblox.auth.session.QBSession
import com.quickblox.chat.QBChatService
import com.quickblox.chat.QBRestChatService
import com.quickblox.core.QBEntityCallback
import com.quickblox.core.exception.QBResponseException
import com.quickblox.core.request.QBPagedRequestBuilder
import com.quickblox.users.QBUsers
import com.quickblox.users.model.QBUser
import com.quickblox.videochat.webrtc.*
import com.quickblox.videochat.webrtc.callbacks.QBRTCClientVideoTracksCallbacks
import com.quickblox.videochat.webrtc.callbacks.QBRTCSessionEventsCallback
import com.quickblox.videochat.webrtc.callbacks.QBRTCSessionStateCallback
import com.quickblox.videochat.webrtc.view.QBRTCSurfaceView
import com.quickblox.videochat.webrtc.view.QBRTCVideoTrack
import kotlinx.android.synthetic.main.activity_call.*
import org.webrtc.RendererCommon
import org.webrtc.SurfaceViewRenderer
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class CallActivity : Activity(), QBRTCSessionEventsCallback,
        QBRTCClientVideoTracksCallbacks<QBRTCSession>, QBRTCSessionStateCallback<QBRTCSession> {

    companion object {
        fun outgoingCall(context: Context, opponentId: Int) {
            val intent = Intent(context, CallActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("IS_INCOMING", false)
            intent.putExtra("OPPONENT_ID", opponentId)
            context.startActivity(intent)
        }

        fun incomingCall(context: Context) {
            val intent = Intent(context, CallActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("IS_INCOMING", true)
            context.startActivity(intent)
        }
    }

    private var ringtonePlayer: RingtonePlayer? = null
    private var vibrator: Vibrator? = null

    private var isIncomingCall: Boolean = true
    private var opponentId: Int = 0
    private var currentSession: QBRTCSession? = null
    private var audioManager: AppRTCAudioManager? = null

    private lateinit var rtcClient: QBRTCClient

    private var isConnected: Boolean = false

    private var currentApiVersion: Int = 0

    private val callTimer: Timer = Timer()
    private var callCounter: Int = 0

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call)
        setResult(RESULT_CANCELED)

        currentApiVersion = Build.VERSION.SDK_INT

        val flags = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        // This work only for android 4.4+
        if (currentApiVersion >= Build.VERSION_CODES.KITKAT) {

            window.decorView.systemUiVisibility = flags

            val decorView = window.decorView
            decorView.setOnSystemUiVisibilityChangeListener { visibility ->
                if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                    decorView.systemUiVisibility = flags
                }
            }
        }

        // Parse intent data
        if (!intent.hasExtra("IS_INCOMING")) {
            finish()
            return
        }
        isIncomingCall = intent.getBooleanExtra("IS_INCOMING", false)
        opponentId = intent.getIntExtra("OPPONENT_ID", 0)

        initRTCClient()
        initAudioManager()

        currentSession = RtcSessionManager.getCurrentSession()
        currentSession?.addVideoTrackCallbacksListener(this)
        currentSession?.addSessionCallbacksListener(this)

        initScreen()

        callTimer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                callCounter++
                runOnUiThread {
                    updateTimer()
                }
            }
        }, 1000, 1000)
    }

    @SuppressLint("NewApi")
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (currentApiVersion >= Build.VERSION_CODES.KITKAT && hasFocus) {
            window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
        }
    }

    private fun initScreen() {
        // Play ringtone if incoming call
        if (isIncomingCall) playRingtone()
        else {
            val userInfo = HashMap<String, String>()
            userInfo["caller_name"] = "Caller"
            userInfo["called_name"] = "Called"
            currentSession?.startCall(userInfo)
        }

        // Setup action
        btnReject.setOnClickListener { currentSession?.rejectCall(HashMap()) }
        btnAccept.setOnClickListener {
            stopRingtone()
            currentSession?.acceptCall(HashMap())

            callCounter = 0
            updateTimer()
        }
        btnHangUp.setOnClickListener { currentSession?.hangUp(HashMap()) }

        // update view for appropriate state
        updateCallView()

        initCorrectSizeForLocalView()
    }

    private fun initRTCClient() {
        rtcClient = QBRTCClient.getInstance(this)
        rtcClient.addSessionCallbacksListener(this)

        rtcClient.prepareToProcessCalls()
    }

    private fun initAudioManager() {
        audioManager = AppRTCAudioManager.create(this)
        val isVideoCall = currentSession?.conferenceType == QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO

        if (isVideoCall) {
            audioManager?.defaultAudioDevice = AppRTCAudioManager.AudioDevice.SPEAKER_PHONE
        } else {
            audioManager?.defaultAudioDevice = AppRTCAudioManager.AudioDevice.EARPIECE
        }
        audioManager?.init()
        audioManager?.start { _, _ -> }

        if (isVideoCall) {
            audioManager?.setAudioDevice(AppRTCAudioManager.AudioDevice.SPEAKER_PHONE)
        } else {
            audioManager?.setAudioDevice(AppRTCAudioManager.AudioDevice.EARPIECE)
        }

        setAudioEnabled(true)
    }

    private fun setAudioEnabled(isAudioEnabled: Boolean) {
        if (currentSession != null && currentSession!!.mediaStreamManager != null) {
            currentSession!!.mediaStreamManager.localAudioTrack.setEnabled(isAudioEnabled)
        }
    }

    private fun updateTimer() {
        val minute = callCounter / 60
        val second = callCounter % 60
        val text = "$minute:$second"
        callTimer1.text = text
        callTimer2.text = text
    }

    private fun updateCallView() {
        remoteVideoView.visibility = View.GONE
        callInfoContainer1.visibility = View.GONE
        localVideoView.visibility = View.GONE
        callInfoContainer2.visibility = View.GONE
        callType.visibility = View.GONE
        callTimer2.visibility = View.GONE

        val isVideo = currentSession?.conferenceType == QBRTCTypes.QBConferenceType.QB_CONFERENCE_TYPE_VIDEO
        val isOutgoing = !isIncomingCall

        val callDirectionText = if (isOutgoing) "Outgoing" else "Incoming"
        val callTypeText = if (isVideo) "Video Call" else "Audio Call"
        callType.text = "$callDirectionText $callTypeText${if (isOutgoing) "..." else ""}"

        if (isVideo) {
            if (isConnected) {
                remoteVideoView.visibility = View.VISIBLE
                localVideoView.visibility = View.VISIBLE
                callInfoContainer1.visibility = View.VISIBLE
                callTimer2.visibility = View.VISIBLE
            } else {
                callInfoContainer2.visibility = View.VISIBLE
                callType.visibility = View.VISIBLE
            }
        } else {
            callInfoContainer2.visibility = View.VISIBLE
            if (isConnected)
                callTimer2.visibility = View.VISIBLE
            else
                callType.visibility = View.VISIBLE
        }

        // Opponent Name
        if (isIncomingCall) opponentId = currentSession?.callerID ?: 0

        val rb = QBPagedRequestBuilder()
        rb.perPage = 100

        QBUsers.getUsersByIDs(arrayListOf(opponentId), rb)
                .performAsync(object : QBEntityCallback<ArrayList<QBUser>> {
                    override fun onSuccess(users: ArrayList<QBUser>, args: Bundle?) {
                        val name = users.firstOrNull()?.fullName ?: "Unknown"
                        opponentName1.text = name
                        opponentName2.text = name
                    }

                    override fun onError(ex: QBResponseException) {
                        Log.d("AAA", ex.message)
                        val name = "Unknown"
                        opponentName1.text = name
                        opponentName2.text = name
                    }
                })


        // Button control
        containerInCall.visibility = View.GONE
        containerWaiting.visibility = View.GONE
        if (isConnected || isOutgoing) containerInCall.visibility = View.VISIBLE
        else containerWaiting.visibility = View.VISIBLE
    }

    private fun initCorrectSizeForLocalView() {
        val params = localVideoView.layoutParams
        val displayMetrics = resources.displayMetrics

        val screenWidthPx = displayMetrics.widthPixels

        val width = (screenWidthPx * 0.3).toInt()
        val height = width / 2 * 3
        params?.width = width
        params?.height = height
        localVideoView.layoutParams = params
    }

    private fun playRingtone() {
        // Play Ringtone
        ringtonePlayer = RingtonePlayer(this, R.raw.beep)
        ringtonePlayer?.play(true)

        // Play vibrator
        vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
        val vibrationCycle = longArrayOf(0, 1000, 1000)
        vibrator?.hasVibrator()?.let {
            vibrator?.vibrate(vibrationCycle, 1)
        }
    }

    private fun stopRingtone() {
        ringtonePlayer?.stop()
        vibrator?.cancel()
    }

    private fun fillVideoView(
            videoView: QBRTCSurfaceView, videoTrack: QBRTCVideoTrack,
            remoteRenderer: Boolean
    ) {
        videoTrack.removeRenderer(videoTrack.renderer)
        videoTrack.addRenderer(videoView)
        if (!remoteRenderer) {
            updateVideoView(videoView, true)
        }
        Log.d("AAA", (if (remoteRenderer) "remote" else "local") + " Track is rendering")
    }

    private fun updateVideoView(videoView: SurfaceViewRenderer, mirror: Boolean) {
        val scalingType = RendererCommon.ScalingType.SCALE_ASPECT_FILL
        videoView.setScalingType(scalingType)
        videoView.setMirror(mirror)
        videoView.requestLayout()
    }

    override fun onUserNotAnswer(session: QBRTCSession?, opponentId: Int?) {
        stopRingtone()
    }

    override fun onReceiveHangUpFromUser(
            session: QBRTCSession?,
            opponentId: Int?,
            userInfo: MutableMap<String, String>?
    ) {
    }

    override fun onCallAcceptByUser(
            session: QBRTCSession?,
            opponentId: Int?,
            userInfo: MutableMap<String, String>?
    ) {
        stopRingtone()

        isConnected = true
        updateCallView()

        callCounter = 0
        updateTimer()
    }

    override fun onCallRejectByUser(
            session: QBRTCSession?,
            opponentId: Int?,
            userInfo: MutableMap<String, String>?
    ) {
        stopRingtone()
    }

    override fun onSessionClosed(session: QBRTCSession?) {
        stopRingtone()
        finish()
    }

    override fun onLocalVideoTrackReceive(session: QBRTCSession?, videoTrack: QBRTCVideoTrack) {
        fillVideoView(localVideoView, videoTrack, false)
        updateVideoView(localVideoView, false)
    }

    override fun onRemoteVideoTrackReceive(
            session: QBRTCSession?,
            videoTrack: QBRTCVideoTrack,
            opponentId: Int?
    ) {
        fillVideoView(remoteVideoView, videoTrack, true)
        updateVideoView(remoteVideoView, false)
    }

    override fun onDisconnectedFromUser(session: QBRTCSession?, opponentId: Int?) {
    }

    override fun onConnectedToUser(session: QBRTCSession?, opponentId: Int?) {
        isConnected = true
        updateCallView()

        currentSession?.removeVideoTrackCallbacksListener(this)
        currentSession?.addVideoTrackCallbacksListener(this)
    }

    override fun onConnectionClosedForUser(session: QBRTCSession?, opponentId: Int?) {
    }

    override fun onStateChanged(session: QBRTCSession?, state: BaseSession.QBRTCSessionState?) {
    }
}
